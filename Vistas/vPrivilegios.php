<!DOCTYPE html>
<html lang="es" dir="ltr">

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Sep 2018 05:47:59 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>BOOTCLASIFIED - Responsive Classified Theme</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>
<body>
<div id="wrapper">


    <div class="header">
        <nav class="navbar  fixed-top navbar-site navbar-light bg-light navbar-expand-md"
             role="navigation">
            <div class="container">

                <div class="navbar-identity">


                    <a href="index.html" class="navbar-brand logo logo-title">
<span class="logo-icon"><i class="icon icon-search-1 ln-shadow-logo "></i>
</span>BOOT<span>CLASSIFIED </span> </a>


                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggler pull-right"
                            type="button">

                        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 30 30" width="30" height="30"
                             focusable="false"><title>Menu</title>
                            <path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10"
                                  d="M4 7h22M4 15h22M4 23h22"/>
                        </svg>


                    </button>


                    <button
                        class="flag-menu country-flag d-block d-md-none btn btn-secondary hidden pull-right"
                        href="#select-country" data-toggle="modal"><span class="flag-icon flag-icon-us"></span> <span
                            class="caret"></span>
                    </button>

                </div>


                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="flag-menu country-flag tooltipHere hidden-xs nav-item" data-toggle="tooltip"
                            data-placement="bottom" title="Select Country"><a href="#select-country" data-toggle="modal"
                                                                              class="nav-link">

                                <span class="flag-icon flag-icon-us"></span> <span class="caret"></span>

                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav ml-auto navbar-right">
                        <li class="nav-item"><a href="category.html" class="nav-link"><i class="icon-th-thumb"></i> All
                                Ads</a>
                        </li>
                        <li class="dropdown no-arrow nav-item"><a href="#" class="dropdown-toggle nav-link"
                                                                  data-toggle="dropdown">

                                <span>Jhon Doe</span> <i class="icon-user fa"></i> <i
                                    class=" icon-down-open-big fa"></i></a>
                            <ul
                                class="dropdown-menu user-menu dropdown-menu-right">
                                <li class="active dropdown-item"><a href="account-home.html"><i class="icon-home"></i>
                                        Personal Home

                                    </a>
                                </li>
                                <li class="dropdown-item"><a href="account-myads.html"><i class="icon-th-thumb"></i> My
                                        ads </a>
                                </li>
                                <li class="dropdown-item"><a href="account-favourite-ads.html"><i
                                            class="icon-heart"></i> Favourite ads </a>
                                </li>
                                <li class="dropdown-item"><a href="account-saved-search.html"><i
                                            class="icon-star-circled"></i> Saved search

                                    </a>
                                </li>
                                <li class="dropdown-item"><a href="account-archived-ads.html"><i
                                            class="icon-folder-close"></i> Archived ads

                                    </a>
                                </li>
                                <li class="dropdown-item"><a href="account-pending-approval-ads.html"><i
                                            class="icon-hourglass"></i> Pending

                                        approval </a>
                                </li>
                                <li class="dropdown-item"><a href="statements.html"><i class=" icon-money "></i> Payment
                                        history </a>
                                </li>
                                <li class="dropdown-item"><a href="login.html"><i class=" icon-logout "></i> Log out
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                                                        href="post-ads.html">Post Free Add</a>
                        </li>
                        <li class="dropdown  lang-menu nav-item">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                                <span class="lang-title">EN</span>

                            </button>
                            <ul class="dropdown-menu dropdown-menu-right user-menu" role="menu">
                                <li class="dropdown-item"><a class="active">

                                        <span class="lang-name">English</span></a>
                                </li>
                                <li class="dropdown-item"><a><span class="lang-name">Dutch</span></a>
                                </li>
                                <li class="dropdown-item"><a><span class="lang-name">fran&#xE7;ais </span></a>
                                </li>
                                <li class="dropdown-item"><a><span class="lang-name">Deutsch</span></a>
                                </li>
                                <li class="dropdown-item"><a> <span class="lang-name">Arabic</span></a>
                                </li>
                                <li class="dropdown-item"><a><span class="lang-name">Spanish</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- /.header -->


    <?php
    $ci = $_REQUEST['ci'];
    $rol = $_REQUEST['rol'];
    ?>


    <div class="container main-container">
        <div class="container">
            <div class="row">

                <div class=" col-xl-8">
                    <div class="card card-dark card-elements" style="margin-bottom: 30px">
                        <div class="card-header">
                            <h4 class="card-title"><a aria-expanded="true" data-toggle="collapse" href="#collapse2"
                                                      class="collapseWill">
                                    Gestionar Privilegios <?php echo $ci . "| " . $rol; ?>
                                </a>

                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse show">
                            <div class="card-body">
                                <form method="POST" action="../Controlador/cPrivilegio.php">

                                    <form method="POST" action="../Controlador/cPrivilegio.php">
                                        <h2>ASIGNACION DE PRIVILEGIOS</h2>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="1"
                                                       name="gestionarCliente" checked="">
                                                Administrar Cliente&mdash;
                                            </label>
                                        </div>
                                        <br>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="2"
                                                       name="gestionarEmpleado" checked="">
                                                Gestionar Empleado&mdash;
                                            </label>
                                        </div>
                                        <br>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="3"
                                                       name="gestionarPrivilegios" checked="">
                                                Gestionar Privilegios&mdash;
                                            </label>
                                        </div>
                                        <br>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="4"
                                                       name="gestionarReporte" checked="">
                                                Administrar Reporte&mdash;
                                            </label>
                                        </div>
                                        <br>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="5"
                                                       name="gestionarSoporte" checked="">
                                                Administrar Soporte&mdash;
                                            </label>
                                        </div>
                                        <br>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="6"
                                                       name="gestionarBitacora" checked="">
                                                Administrar Bitacora&mdash;
                                            </label>
                                        </div>
                                        <br>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="7"
                                                       name="gestionarBackup" checked="">
                                                Administrar Backup&mdash;
                                            </label>
                                        </div>
                                        <div>
                                            <input type="text" name="ci" hidden="hidden" value="<?php echo $ci ?>">
                                        </div>
                                        <hr>
                                        <button type="submit" name="privilegio" class="btn btn-primary">Aceptar</button>
                                    </form>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /main container -->

                    <!-- <div class="page-bottom-info">
                        <div class="page-bottom-info-inner">

                            <div class="page-bottom-info-content text-center">
                                <h1>If you have any questions, comments or concerns, please call the Classified Advertising department
                                    at (000) 555-5555</h1>
                                <a class="btn  btn-lg btn-primary-dark" href="tel:+000000000">
                                    <i class="icon-mobile"></i> <span class="hide-xs color50">Call Now:</span> (000) 555-5555 </a>
                            </div>

                        </div>
                    </div> -->

                    <footer class="main-footer">
                        <div class="footer-content">
                            <div class="container">
                                <div class="row">

                                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                                        <div class="footer-col">
                                            <h4 class="footer-title">About us</h4>
                                            <ul class="list-unstyled footer-nav">
                                                <li><a href="#">About Company</a></li>
                                                <li><a href="#">For Business</a></li>
                                                <li><a href="#">Our Partners</a></li>
                                                <li><a href="#">Press Contact</a></li>
                                                <li><a href="#">Careers</a></li>
                                                <li><a href="event-home.html">Events</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                                        <div class="footer-col">
                                            <h4 class="footer-title">Help & Contact</h4>
                                            <ul class="list-unstyled footer-nav">
                                                <li><a href="#">
                                                        Stay Safe Online
                                                    </a></li>
                                                <li><a href="#">
                                                        How to Sell</a></li>
                                                <li><a href="#">
                                                        How to Buy
                                                    </a></li>
                                                <li><a href="#">Posting Rules
                                                    </a></li>

                                                <li><a href="#">
                                                        Promote Your Ad
                                                    </a></li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                                        <div class="footer-col">
                                            <h4 class="footer-title">More From Us</h4>
                                            <ul class="list-unstyled footer-nav">
                                                <li><a href="faq.html">FAQ
                                                    </a></li>
                                                <li><a href="blogs.html">Blog
                                                    </a></li>
                                                <li><a href="#">
                                                        Popular Searches
                                                    </a></li>
                                                <li><a href="#"> Site Map
                                                    </a></li>
                                                <li><a href="#"> Customer Reviews
                                                    </a></li>


                                            </ul>
                                        </div>
                                    </div>
                                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                                        <div class="footer-col">
                                            <h4 class="footer-title">Account</h4>
                                            <ul class="list-unstyled footer-nav">
                                                <li><a href="account-home.html"> Manage Account
                                                    </a></li>
                                                <li><a href="login.html">Login
                                                    </a></li>
                                                <li><a href="signup.html">Register
                                                    </a></li>
                                                <li><a href="account-myads.html"> My ads
                                                    </a></li>
                                                <li><a href="seller-profile.html"> Profile
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                                        <div class="footer-col row">

                                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                                <div class="mobile-app-content">
                                                    <h4 class="footer-title">Mobile Apps</h4>
                                                    <div class="row ">
                                                        <div class="col-6  ">
                                                            <a class="app-icon" target="_blank"
                                                               href="https://itunes.apple.com/">
                                                                <span class="hide-visually">iOS app</span>
                                                                <img src="images/site/app_store_badge.svg"
                                                                     alt="Available on the App Store">
                                                            </a>
                                                        </div>
                                                        <div class="col-6  ">
                                                            <a class="app-icon" target="_blank"
                                                               href="https://play.google.com/store/">
                                                                <span class="hide-visually">Android App</span>
                                                                <img src="images/site/google-play-badge.svg"
                                                                     alt="Available on the App Store">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                                <div class="hero-subscribe">
                                                    <h4 class="footer-title no-margin">Follow us on</h4>
                                                    <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                                        <li><a class="icon-color fb" title="Facebook"
                                                               data-placement="top" data-toggle="tooltip" href="#"><i
                                                                    class="fab fa-facebook-f"></i> </a></li>
                                                        <li><a class="icon-color tw" title="Twitter"
                                                               data-placement="top" data-toggle="tooltip" href="#"><i
                                                                    class="fab fa-twitter"></i> </a></li>
                                                        <li><a class="icon-color gp" title="Google+"
                                                               data-placement="top" data-toggle="tooltip" href="#"><i
                                                                    class="fab fa-google-plus-g"></i> </a></li>
                                                        <li><a class="icon-color lin" title="Linkedin"
                                                               data-placement="top" data-toggle="tooltip" href="#"><i
                                                                    class="fab fa-linkedin"></i> </a></li>
                                                        <li><a class="icon-color pin" title="Linkedin"
                                                               data-placement="top" data-toggle="tooltip" href="#"><i
                                                                    class="fab fa-pinterest-p"></i> </a></li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>

                                    <div class="col-xl-12">
                                        <div class=" text-center paymanet-method-logo">

                                            <img src="images/site/payment/master_card.png" alt="img">
                                            <img alt="img" src="images/site/payment/visa_card.png">
                                            <img alt="img" src="images/site/payment/paypal.png">
                                            <img alt="img" src="images/site/payment/american_express_card.png"> <img
                                                alt="img" src="images/site/payment/discover_network_card.png">
                                            <img alt="img" src="images/site/payment/google_wallet.png">
                                        </div>

                                        <div class="copy-info text-center">
                                            &copy; 2017 BootClassified. All Rights Reserved.
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </footer>
                    <!-- /.footer -->
                </div>
                <!-- /.wrapper -->

                <!-- Modal Change City -->

                <div class="modal fade modalHasList" id="select-country" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title uppercase font-weight-bold" id="exampleModalLabel"><i
                                        class=" icon-map"></i> Select your region </h4>

                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="row" style="padding: 0 20px">


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
                <script src="assets/js/vendors.min.js"></script>
                <script src="assets/js/main.min.js"></script>


</body>

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Sep 2018 05:47:59 GMT -->
</html>
