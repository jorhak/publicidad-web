<?php
$id_s = $_GET['id'];
require_once("../Controlador/cSoporte.php");
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
    <title>Inicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--       <link href="../starter-template.css" rel="stylesheet"> 

        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>

    <!-- Para el Modal -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body style="background-color:#D5F5E3">

<div align="center">
    <br>
    <div class="inner-box">
        <br>
        <h1 class="title-1">
            <i class="icon-docs"></i>
            <strong>Responder Solicitud de Soporte Cliente</strong>
        </h1>
        <br>
        <br>
        <form class="form-horizontal" action="../Controlador/cSoporteAdm.php" method="POST" role="form"
              style="width: 50%">

            <div class="form-group">
                <label class="col-sm-3 control-label">id</label>

                <div class="col-sm-9">
                    <input type="text" readonly name="id_sop" value="<?php echo($id_s) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Respuesta</label>

                <div class="col-sm-9">
                                                    <textarea rows="6" cols="40" name="descrip" autofocus="">
                                                    </textarea>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-success" name="btn_resp">
                        Responder
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
<br>
</body>
</html>
