<?php
require_once("../Controlador/cCliente.php");
require_once("../Modelo/MCasoUso.php");
//echo '<pre>'; print_r($_SESSION['ListaCU']); echo '</pre>';
if (session_status() == PHP_SESSION_NONE) {
    ob_start();
    session_start();
}
?>
<?php
function getPrivilegios()
{
    $objeto_modelo = MCasoUso::getCasoUso($_SESSION['ci_cliente'], $_SESSION['user']);
    foreach ($objeto_modelo as $obj) { ?>

        <a href="<?php echo $obj->carpeta; ?>"> <?php echo $obj->nombre ?>
            <span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>
        <?php
    }
}

?>

<!DOCTYPE html>
<html lang="es" dir="ltr">
<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Sep 2018 05:41:26 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Vista Administrador</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>
<body style="background-color:#D5F5E3">
<div id="wrapper">
    <div class="themeControll ">

        <h3 style=" color:#fff; font-size: 10px; line-height: 12px;" class="uppercase color-white text-center">
            <a target="_blank" href="http://templatecycle.com/demo/bootclassified-4.4/template-index.html"> Casos de
                usos </a></h3>

        <div class="linkinner linkScroll scrollbar ">
            <!-- <a  href="../Vistas/vSoporteAdm.php"> Responder Soporte <span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>
            <a  href="../Vistas/vDetalleAnuncio.php"> Gestionar Detalle Anuncio <span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>
            <a href="../Vistas/vBitacora.php"> Mostrar Bitacora <span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>

            <a href="../Reportes/index.php"> Reporte Usuarios<span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>
            <a href="../Reportes/indexAnuncio.php"> Reporte Anuncios<span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>
            <a href="../Reportes/indexCuentaBancaria.php"> Reporte Cuentas Bancarias<span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>
            <a href="../Reportes/indexSoporte.php"> Reporte Soportes<span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a>
            <a href="../Backup/backup.php"> Backup<span class="badge badge-success " style="font-size: 10px"><em>NEW</em></span></a> -->
            <?php getPrivilegios(); ?>

        </div>
        <p class="tbtn"><i class="fa fa-angle-double-left"></i></p>
    </div>
    <!--themeControll-->


    <div class="header">
        <nav class="navbar  fixed-top navbar-site navbar-light bg-light navbar-expand-md"
             role="navigation">
            <div class="container">

                <div class="navbar-identity">
                    <a href="" class="navbar-brand logo logo-title">
                <span class="logo-icon">
                    <!-- <i class="icon icon-search-1 ln-shadow-logo "></i> -->
                    <img src="img/Logo7.png" width="50" height="50">
                </span>Perfil <span>Usuario </span> </a>

                </div>
                <!-- Opciones desplegables - barra Menu -->
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav ml-auto navbar-right">

                        <li class="dropdown no-arrow nav-item"><a href="##" class="dropdown-toggle nav-link"
                                                                  data-toggle="dropdown">

                            <span> <?php echo($datos[1] . " " . $datos[2]); ?>

                            </span>
                                <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                            <ul
                                class="dropdown-menu user-menu dropdown-menu-right">

                                <li class="dropdown-item"><a href="vSoporteAdm.php"><i class="icon-docs"></i>Responder
                                        Soportes</a>
                                </li>

                                <li class="dropdown-item"><a href="../Vistas/vDetalleAnuncio.php"><i
                                            class="icon-heart"></i>Gestionar Detalle Anuncios </a>
                                </li>

                                <li class="dropdown-item">
                                    <a href="../Vistas/vBitacora.php">
                                        <i class="icon-map"></i> Mostrar Bitacora
                                    </a>

                                <li class="dropdown-item"><a href="../Controlador/cerrar_sesion.php"><i
                                            class=" icon-logout "></i> Cerrar Sesion </a>
                                </li>
                            </ul>
                        </li>
                        <!-- boton rojo cerrar sesion -->
                        <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                                                        href="../Controlador/cerrar_sesion.php">Cerrar Sesión</a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>


    <div class="intro" style="background-image: url(images/bg3.jpg);">
        <div class="dtable hw100">
            <div class="dtable-cell hw100">
                <div class="container text-center">
                    <h1 class="intro-title animated fadeInDown"> Bienvenido has ingresado como Administrador </h1>

                    <p class="sub animateme fittext3 animated fadeIn">
                        Tienes todos los permisos necesarios para administrar la pagina
                    </p>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.wrapper -->


<footer align="center">
    <img src="img/Logo6.png" align="center" style="width: 1000px;height: 300px">
</footer>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>
<script type="text/javascript" src="assets/plugins/autocomplete/jquery.mockjax.js"></script>
<script type="text/javascript" src="assets/plugins/autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="assets/plugins/autocomplete/usastates.js"></script>
<script type="text/javascript" src="assets/plugins/autocomplete/autocomplete-demo.js"></script>
</body>

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Sep 2018 05:42:27 GMT -->
</html>
