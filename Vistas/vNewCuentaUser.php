<?php
$ci = "";
$n = "";
$ape = "";
$e = "";
$t = "";
$id = "";
$pas = "";

?>

<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Registro Usuarios Nuevos</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>
<body style="background-color:#D5F5E3">
<div id="wrapper">
    <!--  Menu Principal Arriba -->
    <div class="header">
        <nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: white'>
            <div class="container">

                <div class="navbar-identity">
                    <a class="navbar-brand logo logo-title">
                <span class="logo-icon">
                    <img src="img/Logo7.png" width="50" height="50">
                </span>Nuevo <span> Usuario </span> </a>

                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav ml-auto navbar-right">
                        <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-success nav-link"
                                                        href="login.html">Iniciar Session</a>
                        </li>
                        <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                                                        href="../index.php">Cerrar</a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>

    <!-- /.Menu Principal Arriba -->

    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-8 page-content">
                    <div class="inner-box category-content">
                        <h2 class="title-2"><i class="icon-user-add"></i> Registro de tus Datos Personales</h2>
                        <div class="row">
                            <div class="col-sm-12">
                                <form method="POST" action="../Controlador/cCliente.php" onsubmit=" return marcado();">
                                    <fieldset>

                                        <!-- Text input-->

                                        <div class="form-group row required">
                                            <label class="col-md-4 control-label">Cedula Identidad
                                                <sup>*</sup>
                                            </label>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control input-md"
                                                       value="<?php echo($ci); ?>" name="ci_ncliente"
                                                       placeholder="Numero de C.I." required="" style="width: 200px">
                                            </div>
                                        </div>

                                        <div class="form-group  row required">
                                            <label class="col-md-4 control-label">Nombres
                                                <sup>*</sup>
                                            </label>

                                            <div class="col-md-6">
                                                <input placeholder="Introduce tu Nombre Completo"
                                                       class="form-control input-md" required type="text"
                                                       value="<?php echo($n); ?>" name="nnombre">
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group  row required">
                                            <label class="col-md-4 control-label">Apellidos<sup>*</sup></label>

                                            <div class="col-md-6">
                                                <input placeholder="Introduces tus Apellidos"
                                                       class="form-control input-md" required type="text"
                                                       value="<?php echo($ape); ?>" name="nape">
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="form-group  row required">
                                            <label for="inputEmail3" class="col-md-4 control-label">Email
                                                <sup>*</sup></label>

                                            <div class="col-md-6">
                                                <input type="email" class="form-control" id="inputEmail3"
                                                       required placeholder="Correo Electronico"
                                                       value="<?php echo($e); ?>" name="nemail">
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group  row required">
                                            <label class="col-md-4 control-label">Telefono/Celular <sup>*</sup></label>

                                            <div class="col-md-6">
                                                <input placeholder="Numero Telefono" required
                                                       class="form-control input-md" type="text"
                                                       value="<?php echo($t); ?>" name="ncel">
                                            </div>
                                        </div>
                                        <h2 class="title-2"><i class="icon-user-add"></i>Crear Login Usuario</h2>
                                        <div class="form-group  row required">
                                            <label for="inputPassword3" class="col-md-4 control-label">Nombre de
                                                Usuario </label>

                                            <div class="col-md-6">
                                                <input type="textinput" class="form-control" id="inputPassword3"
                                                       placeholder="Id Usuario" required=""
                                                       value="<?php echo($id); ?>" name="nid">
                                            </div>
                                        </div>

                                        <div class="form-group  row required">
                                            <label for="inputPassword3"
                                                   class="col-md-4 control-label">Contraseña </label>

                                            <div class="col-md-6">
                                                <input type="password" class="form-control" id="inputPassword3"
                                                       placeholder="Password" required=""
                                                       value="<?php echo($pas); ?>" name="npas">
                                                <small id="passwordHelpBlock" class="form-text text-muted">
                                                    Mayor a 5 caracteres.
                                                </small>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label"></label>

                                            <div class="col-md-8">
                                                <div class="termbox mb10">
                                                    <div class="col-auto my-1 no-padding">
                                                        <div class="custom-control custom-checkbox mr-sm-2">
                                                            <input type="checkbox" class="custom-control-input"
                                                                   id="customControlAutosizing" name="check1" checked>
                                                            <label class="custom-control-label"
                                                                   for="customControlAutosizing">
                                                                <span class="custom-control-description"> 
                                                                    He leído y estoy de acuerdo con 
                                                                    <a href="terminos_y_condiciones.html">
                                                                        Terminos y Condiciones</a>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input name="submit_new_login" class="btn btn-primary"
                                                           value="Registrar Nuevo Usuario" type="submit">
                                                </div>
                                            </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.page-content -->

                <div class="col-md-4 reg-sidebar">
                    <div class="reg-sidebar-inner text-center">
                        <div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>

                            <h3><strong>Compra tus Productos o Inmmuebles</strong></h3>

                            <p> Este Sistema te ofrece la posibilidad de hacer compras Online con tu tarjeta de
                                Credito </p>
                        </div>
                        <div class="promo-text-box"><i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>

                            <h3><strong>Crea y Publica tu Anuncio</strong></h3>

                            <p> Puedes ofrecer una variedad de articulos e inmuebles</p>
                        </div>
                        <div class="promo-text-box"><i class="  icon-heart-2 fa fa-4x icon-color-3"></i>
                            <h3><strong>El producto que querias</strong></h3>
                            <p> Este Sistema ofrece una variedad de productos y seguro encontraras el que necesitas</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.main-container -->

    <footer class="main-footer">
        <div class='footer-content' style='background-color: white'>
            <div class="container">
                <div class="row">
                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                        <div class="footer-col">
                            <h4 class="footer-title">Ayuda y Contacto</h4>
                            <ul class="list-unstyled footer-nav">
                                <li><a href="#">
                                        Soporte Online
                                    </a></li>
                                <li><a href="#">
                                        Como vender?</a></li>
                                <li><a href="#">
                                        Como comprar?
                                    </a></li>
                                <li>
                                    <a href="terminos_y_condiciones.html">Terminos y Condiciones</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    </div>

                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                        <div class="footer-col">
                            <h4 class="footer-title">Mi Cuenta</h4>
                            <ul class="list-unstyled footer-nav">
                                <li><a href="login.html">Login
                                    </a></li>
                                <li><a href="vNewCuentaUser.php">Registro
                                    </a></li>

                            </ul>
                        </div>
                    </div>

                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    </div>

                    <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                        <div class="footer-col row">

                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                <div class="mobile-app-content">
                                    <h4 class="footer-title">App Movil</h4>
                                    <div class="row ">
                                        <div class="col-6  ">
                                            <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                                                <span class="hide-visually">iOS app</span>
                                                <img src="images/site/app_store_badge.svg"
                                                     alt="Available on the App Store">
                                            </a>
                                        </div>
                                        <div class="col-6  ">
                                            <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                                                <span class="hide-visually">Android App</span>
                                                <img src="images/site/google-play-badge.svg"
                                                     alt="Available on the App Store">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                <div class="hero-subscribe">
                                    <h4 class="footer-title no-margin">Siguenos en:</h4>
                                    <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                        <li><a class="icon-color fb" title="Facebook" data-placement="top"
                                               data-toggle="tooltip" href="#"><i class="fab fa-facebook-f"></i> </a>
                                        </li>
                                        <li><a class="icon-color tw" title="Twitter" data-placement="top"
                                               data-toggle="tooltip" href="#"><i class="fab fa-twitter"></i> </a></li>
                                        <li><a class="icon-color gp" title="Google+" data-placement="top"
                                               data-toggle="tooltip" href="#"><i class="fab fa-google-plus-g"></i> </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div style="clear: both"></div>

                    <div class="col-xl-12">
                        <div class=" text-center paymanet-method-logo">

                            <img src="images/site/payment/master_card.png" alt="img">
                            <img alt="img" src="images/site/payment/visa_card.png">
                            <img alt="img" src="images/site/payment/paypal.png">
                            <img alt="img" src="images/site/payment/american_express_card.png"> <img alt="img"
                                                                                                     src="images/site/payment/discover_network_card.png">
                            <img alt="img" src="images/site/payment/google_wallet.png">
                        </div>

                        <div class="copy-info text-center">
                            &copy; 2017 Pablo Bustos All Rights Reserved.
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </footer>
    <!--/.footer-->

</div>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>
</body>

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/signup.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:38:57 GMT -->
</html>

<script>
    function marcado() {
        if (document.form.check1.checked) {
            document.form.submit();
        } else {
            alert("Debes aceptar los términos y condiciones");
            document.form.check1.focus();
            return false;
        }
    }
</script>
