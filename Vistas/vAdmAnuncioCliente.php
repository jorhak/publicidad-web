<?php
require_once("../Controlador/cCliente.php");
//require_once ("../Controlador/cAdmAnuncio.php");
require_once("../Css_colores/css_fuentes_temas.css");
require_once("../Controlador/conexion.php");

if (!isset($_SESSION['ci_cliente'])) {
    header('location:login.html');
}

$db = Conectar::conexion();

$id_anuncio = $_GET['id'];

$sql = $db->query(" SELECT a.titulo,a.descripcion,a.estado,cat.descripcion,pi.precio,pi.estado,u.direccion,u.zona,di.superficie,pi.precio,di.detalles, t.estado,t.permuta,t.diferencia 
from anuncio as a,categoria as cat,producto_inmueble as pi , detalle_inmueble as di, ubicacion as u, trueque as t
where  a.id_categoria=cat.id and a.id_prod_inmu=pi.id and pi.id=di.id_prod_inmu and pi.id_ubicacion = u.id and pi.id = t.id_prod_inm and
 a.id='$id_anuncio';");

if (isset($_POST['GuardarT'])) {
    $var = $_POST['txt_titulo'];
    $sql1 = $db->query("UPDATE anuncio set titulo= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}


if (isset($_POST['GuardarD'])) {
    $var = $_POST['txt_descripcion'];
    $sql2 = $db->query("UPDATE anuncio set descripcion= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarEa'])) {
    $var = $_POST['txt_categoria'];
    $sql2 = $db->query("UPDATE anuncio set estado= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarPrei'])) {
    $var = $_POST['txt_precio_inmu'];
    $sql2 = $db->query("UPDATE producto_inmueble set precio= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarSupi'])) {
    $var = $_POST['txt_super_inmu'];
    $sql2 = $db->query("UPDATE detalle_inmueble set superficie= '$var'
                         where id_prod_inmu = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarDir'])) {
    $var = $_POST['txt_direccion'];
    $sql2 = $db->query("UPDATE ubicacion set direccion= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarZona'])) {
    $var = $_POST['txt_zona'];
    $sql2 = $db->query("UPDATE ubicacion set zona= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarDeta'])) {
    $var = $_POST['txt_detalles'];
    $sql2 = $db->query("UPDATE detalle_inmueble set detalles= '$var'
                         where id_prod_inmu = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}


if (isset($_POST['GuardaET'])) {
    $var = $_POST['txt_estado_tru'];
    $sql2 = $db->query("UPDATE trueque set estado= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarPer'])) {
    $var = $_POST['txt_permuta'];
    $sql2 = $db->query("UPDATE trueque set permuta= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}

if (isset($_POST['GuardarDifT'])) {
    $var = $_POST['txt_difT'];
    $sql2 = $db->query("UPDATE trueque set diferencia= '$var'
                         where id = '$id_anuncio';");
    header('location:vAdmAnuncioCliente.php?id=' . $id_anuncio);
}
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Publicidad Web</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>

<?php

if ($_SESSION['color_tema'] == 0) {
    echo "<body style='background-color:#D5F5E3'>";
}
if ($_SESSION['color_tema'] == 1) {
    echo("<body style='background-color:#D5D5D5'>");
}
if ($_SESSION['color_tema'] == 2) {
    echo("<body style='background-color:#7493FF'>");
}
if ($_SESSION['color_tema'] == 3) {
    echo("<body style='background-color:#FAFD95'>");
}

?>

<div id="wrapper">

    <div class="header">

        <?php if ($_SESSION['color_tema'] == 0) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: white'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 1) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: yellow'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 2) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #E0F4FA'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 3) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #4CD7FF'>";
        }
        ?>

        <div class="container">

            <div class="navbar-identity">
                <a href="" class="navbar-brand logo logo-title">
                <span class="logo-icon">
                    <!-- <i class="icon icon-search-1 ln-shadow-logo "></i> -->
                    <img src="img/Logo7.png" width="70" height="70">
                </span>Publicidad Web </a>

            </div>
            <!-- Opciones desplegables - barra Menu -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav ml-auto navbar-right">

                    <li class="dropdown no-arrow nav-item"><a href="##" class="dropdown-toggle nav-link"
                                                              data-toggle="dropdown">

                            <no> <?php echo($datos[1] . " " . $datos[2]); ?>

                            </no>
                            <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                        <ul style="border-color: black;"
                            class="dropdown-menu user-menu dropdown-menu-right">
                            <li class="disable dropdown-item">
                                <no> Modulo Usuario</no>
                            </li>
                            <li class="active dropdown-item">
                                <a href="vCliente.php">
                                    <i class="icon-home"></i>Datos Personales
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="vSoporte.php">
                                    <i class="icon-folder-close"></i> Soporte Tecnico
                                </a>
                            </li>

                            <li class="dropdown-item"><a href="vCambiarPass.php"><i class="icon-down-open-big"></i>
                                    Cambiar Contraseña </a>
                            </li>

                            <li class="disable dropdown-item">
                                <no> Modulo Publicidad</no>
                            </li>

                            <li class="dropdown-item"><a href="#"><i class="icon-heart"></i> Administrar Anuncios </a>
                            </li>

                            <li class="dropdown-item"><a href="vMensajeria.php"><i class="icon-mail"></i> Bandeja de
                                    Entrada </a>
                            </li>

                            <li class="disable dropdown-item">
                                <no> Modulo Pagos</no>
                            </li>


                            <li class="dropdown-item"><a href="#"><i class=" icon-money "></i> Transacciones </a>
                            </li>

                            <li class="dropdown-item"><a href="vCuentasbancarias.php"><i class="icon-docs"></i>Cuentas
                                    Bancarias</a>
                            </li>

                        </ul>
                    </li>
                    <!-- Seguir viendo anuncios -->
                    <li class="postadd nav-item">
                        <a class="btn btn-block  btn-border btn-post btn-primary nav-link"
                           href="Anuncios.php" style="background: blue; color: white">
                            Ver Anuncios
                        </a>
                    </li>
                    <!-- Crear un Anuncio -->
                    <li class="postadd nav-item">
                        <a class="btn btn-block  btn-border btn-post btn-primary nav-link"
                           href="../Vistas/vAnadirUbicacion.php" style="background-color: black">
                            Crear un Anuncio
                        </a>
                    </li>
                    <!-- boton rojo cerrar sesion -->
                    <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                                                    href="../Controlador/cerrar_sesion.php">Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- /.header -->
    <div class="main-container">
        <div class="container">
            <div class="row">

                <!--  Menu Lateral   -->
                <div class="col-md-3 page-sidebar">
                    <aside>
                        <div class="inner-box">
                            <div class="user-panel-sidebar">
                                <div class="collapse-box">
                                    <h5 class="collapse-title no-border"> Modulo Usuario <a href="#MyClassified"
                                                                                            aria-expanded="true"
                                                                                            data-toggle="collapse"
                                                                                            class="pull-right"><i
                                                class="fa fa-angle-down"></i></a>
                                    </h5>

                                    <div class="panel-collapse collapse show" id="MyClassified">
                                        <ul class="acc-list">
                                            <li>
                                                <a href="vCliente.php">
                                                    <i class="icon-home"></i>
                                                    Datos Personales </a>
                                            </li>

                                            <li><a href="vSoporte.php">
                                                    <i class="icon-folder-close"></i>Soporte Tecnico
                                                </a>
                                            </li>

                                            <li><a href="#">
                                                    <i class="icon-down-open-big"></i>Cambiar Contraseña </a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <!-- /.collapse-box  -->
                                <div class="collapse-box">
                                    <h5 class="collapse-title"> Modulo Publicidad<a href="#MyAds" aria-expanded="true"
                                                                                    data-toggle="collapse"
                                                                                    class="pull-right">
                                            <i class="fa fa-angle-down"></i></a></h5>

                                    <div class="panel-collapse collapse show" id="MyAds">
                                        <ul class="acc-list">


                                            <li><a href="vAdmAnuncios.php" class="active">
                                                    <i class="icon-heart"></i>Administrar Anuncios
                                                </a>
                                            </li>

                                            <li><a href="vMensajeria.php">
                                                    <i class="icon-mail"></i> Bandeja de Entrada
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.collapse-box  -->

                                <div class="collapse-box">
                                    <h5 class="collapse-title"> Modulo Pagos <a href="#TerminateAccount"
                                                                                aria-expanded="true"
                                                                                data-toggle="collapse"
                                                                                class="fa fa-angle-down"></i></a>
                                    </h5>
                                    <div class="panel-collapse collapse show" id="TerminateAccount">
                                        <ul class="acc-list">
                                            <li><a href="vCuentasbancarias.php">
                                                    <i class="icon-docs"></i>Cuentas Bancarias
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="panel-collapse collapse show" id="TerminateAccount">
                                        <ul class="acc-list">
                                            <li><a href="#">
                                                    <i class="icon-money"></i>Transacciones </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.collapse-box  -->
                            </div>
                        </div>
                        <!-- /.inner-box  -->
                    </aside>
                </div>


                <!--/.page-sidebar-->
                <!-- Este es eL Cuerpo de la vista y esto muestra el campo de roles y privilegios -->
                <div class="col-md-9 page-content">
                    <div class="inner-box">
                        <div class="row">
                            <div class="col-md-5 col-xs-4 col-xxs-12">
                                <h3 class="no-padding text-center-480 useradmin">
                                    <a>
                                        <img class="userImg"
                                             src="img/perfil.png" alt="user">
                                        <strong>
                                            <?php echo($datos[1] . " " . $datos[2]); ?>
                                        </strong>
                                    </a></h3>
                            </div>

                        </div>
                    </div>

                    <div class="inner-box">
                        <div class="welcome-msg">

                        </div>
                        <div id="accordion" class="panel-group">
                            <div class="card card-default">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <a href="#collapseB1" aria-expanded="true" data-toggle="collapse">
                                            <strong>Todos tus Anuncios</strong>
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse show" id="collapseB1">
                                    <div class="card-body">


                                        <table style="width: 100%">
                                            <thead style="background-color: #DCF5FF">
                                            <!--  <tr>
                                                 <td><strong>Titulo del Anuncio</strong></td>
                                                 <td><strong>Nuevo</strong></td>
                                                 <td><strong>Accion</strong></td>

                                             </tr> -->
                                            </thead>
                                            <?php
                                            $row = mysqli_fetch_array($sql) ?>
                                            <tr>
                                                <td>
                                                    Titulo del Anuncio
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" text"
                                                        name="txt_titulo" style="width: 90%"
                                                        <?php if (!isset($_POST['editarT'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[0]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarT'])) { ?>

                                                            <input type="submit" name="GuardarT" value="Guardar">

                                                        <?php } ?>
                                                </form>


                                                <?php if (!isset($_POST['editarT']) || isset($_POST['GuardarT'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarT" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Descripcion del Anuncio
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" text"
                                                        name="txt_descripcion" style="width: 90%"
                                                        <?php if (!isset($_POST['editarD'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[1]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarD'])) { ?>

                                                        <input type="submit" name="GuardarD" value="Guardar">
                                                </form>
                                                <?php } ?>

                                                <?php if (!isset($_POST['editarD']) || isset($_POST['GuardarD'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarD" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Estado del Anuncio
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <?php if (!isset($_POST['editarEa']) && !isset($_POST['GuardarCat'])) { ?>
                                                            <input class="form-control type=" text"
                                                            name="txt_estadoanuncio" style="width: 90%" readonly
                                                            value="<?php echo $row[2]; ?>">
                                                        <?php } else { ?>

                                                            <select id="txt_categoria" name="txt_categoria">
                                                                <option>activo</option>
                                                                <option>inactivo</option>
                                                            </select>
                                                        <?php } ?>


                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarEa'])) { ?>

                                                        <input type="submit" name="GuardarEa" value="Guardar">
                                                </form>
                                                <?php } ?>
                                                <?php if (!isset($_POST['editarEa']) || isset($_POST['GuardarEa'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarEa" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Precio del Inmueble
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" number"
                                                        name="txt_precio_inmu" style="width: 90%"
                                                        <?php if (!isset($_POST['editarPrei'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[9]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarPrei'])) { ?>

                                                        <input type="submit" name="GuardarPrei" value="Guardar">
                                                </form>
                                                <?php } ?>

                                                <?php if (!isset($_POST['editarPrei']) || isset($_POST['GuardarPrei'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarPrei" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Superficie del Inmueble m
                                                    <sup>2</sup>
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" number"
                                                        name="txt_super_inmu" style="width: 90%"
                                                        <?php if (!isset($_POST['editarSupi'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[8]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarSupi'])) { ?>

                                                        <input type="submit" name="GuardarSupi" value="Guardar">
                                                </form>
                                                <?php } ?>

                                                <?php if (!isset($_POST['editarSupi']) || isset($_POST['GuardarSupi'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarSupi" value="Editar">
                                                    </form>
                                                <?php } ?>

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Direccion del Inmueble
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" text"
                                                        name="txt_direccion" style="width: 90%"
                                                        <?php if (!isset($_POST['editarDir'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[6]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarDir'])) { ?>

                                                        <input type="submit" name="GuardarDir" value="Guardar">
                                                </form>
                                                <?php } ?>
                                                <?php if (!isset($_POST['editarDir']) || isset($_POST['GuardarDir'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarDir" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Zona del Inmueble
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" text"
                                                        name="txt_zona" style="width: 90%"
                                                        <?php if (!isset($_POST['editarZona'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[7]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarZona'])) { ?>

                                                        <input type="submit" name="GuardarZona" value="Guardar">
                                                </form>
                                                <?php } ?>

                                                <?php if (!isset($_POST['editarZona']) || isset($_POST['GuardarZona'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarZona" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Detalles del Inmueble
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" text"
                                                        name="txt_detalles" style="width: 90%"
                                                        <?php if (!isset($_POST['editarDeta'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[10]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarDeta'])) { ?>

                                                        <input type="submit" name="GuardarDeta" value="Guardar">
                                                </form>
                                                <?php } ?>
                                                <?php if (!isset($_POST['editarDeta']) || isset($_POST['GuardarDeta'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarDeta" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Estado del Trueque
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <?php if (!isset($_POST['editarET']) && !isset($_POST['GuardarET'])) { ?>
                                                            <input class="form-control type=" text"
                                                            name="txt_estadoTrueque" style="width: 90%" readonly
                                                            value="<?php echo $row[11]; ?>">
                                                        <?php } else { ?>

                                                            <select id="txt_estado_tru" name="txt_estado_tru">
                                                                <option>ACEPTA</option>
                                                                <option>NO ACEPTA</option>
                                                            </select>
                                                        <?php } ?>


                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarET'])) { ?>

                                                        <input type="submit" name="GuardaET" value="Guardar">
                                                </form>
                                                <?php } ?>
                                                <?php if (!isset($_POST['editarET']) || isset($_POST['GuardaET'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarET" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Permuta del Trueque
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" text"
                                                        name="txt_permuta" style="width: 90%"
                                                        <?php if (!isset($_POST['editarPermuta'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[12]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarPermuta'])) { ?>

                                                        <input type="submit" name="GuardarPer" value="Guardar">
                                                </form>
                                                <?php } ?>
                                                <?php if (!isset($_POST['editarPermuta']) || isset($_POST['GuardarPer'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarPermuta" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                            <tr>
                                                <td>
                                                    Diferencia que pide por el Trueque
                                                </td>
                                                <form method="POST">
                                                    <td>
                                                        <input class="form-control type=" number"
                                                        name="txt_difT" style="width: 90%"
                                                        <?php if (!isset($_POST['editarDifT'])) {
                                                            echo "readonly";
                                                        } ?>
                                                        value="<?php echo $row[13]; ?>">
                                                    </td>

                                                    <td>
                                                        <?php if (isset($_POST['editarDifT'])) { ?>

                                                        <input type="submit" name="GuardarDifT" value="Guardar">
                                                </form>
                                                <?php } ?>
                                                <?php if (!isset($_POST['editarDifT']) || isset($_POST['GuardarDifT'])) { ?>
                                                    <form method="POST">
                                                        <input type="submit" name="editarDifT" value="Editar">
                                                    </form>
                                                <?php } ?>


                                                <!-- <a href='vAdmAnuncioCliente.php' style='color': red>Administrar</a> -->

                                                </td>

                                            </tr>


                                        </table>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <!--/.row-box End-->
                    </div>
                </div>
                <!--/.page-content-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- /.main-container -->


    <footer class="main-footer">

        <?php if ($_SESSION['color_tema'] == 0) {
            echo "<div class='footer-content' style='background-color: white'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 1) {
            echo "<div class='footer-content' style='background-color: yellow'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 2) {
            echo "<div class='footer-content' style='background-color: #E0F4FA'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 3) {
            echo "<div class='footer-content' style='background-color: #4CD7FF'>";
        } ?>

        <div class="container">
            <div class="row">
                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    <div class="footer-col">
                        <h4 class="footer-title">Ayuda y Contacto</h4>
                        <ul class="list-unstyled footer-nav">
                            <li><a href="#">
                                    Soporte Online
                                </a></li>
                            <li><a href="#">
                                    Como vender?</a></li>
                            <li><a href="#">
                                    Como comprar?
                                </a></li>
                            <li><a href="terminos_y_condiciones.html">Terminos y Condiciones
                                </a></li>
                        </ul>
                    </div>
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    <div class="footer-col">
                        <h4 class="footer-title">Mi Cuenta</h4>
                        <ul class="list-unstyled footer-nav">
                            <li><a href="login.html">Login
                                </a></li>
                            <li><a href="vNewCuentaUser.php">Registro
                                </a></li>
                        </ul>
                    </div>
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                </div>

                <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                    <div class="footer-col row">

                        <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                            <div class="mobile-app-content">
                                <h4 class="footer-title">App Movil</h4>
                                <div class="row ">
                                    <div class="col-6  ">
                                        <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                                            <span class="hide-visually">iOS app</span>
                                            <img src="images/site/app_store_badge.svg" alt="Available on the App Store">
                                        </a>
                                    </div>
                                    <div class="col-6  ">
                                        <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                                            <span class="hide-visually">Android App</span>
                                            <img src="images/site/google-play-badge.svg"
                                                 alt="Available on the App Store">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                            <div class="hero-subscribe">
                                <h4 class="footer-title no-margin">Siguenos en:</h4>
                                <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                    <li><a class="icon-color fb" title="Facebook" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-facebook-f"></i> </a></li>
                                    <li><a class="icon-color tw" title="Twitter" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-twitter"></i> </a></li>
                                    <li><a class="icon-color gp" title="Google+" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-google-plus-g"></i> </a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>

                <div class="col-xl-12">
                    <div class=" text-center paymanet-method-logo">

                        <img src="images/site/payment/master_card.png" alt="img">
                        <img alt="img" src="images/site/payment/visa_card.png">
                        <img alt="img" src="images/site/payment/paypal.png">
                        <img alt="img" src="images/site/payment/american_express_card.png"> <img alt="img"
                                                                                                 src="images/site/payment/discover_network_card.png">
                        <img alt="img" src="images/site/payment/google_wallet.png">
                    </div>

                    <div class="copy-info text-center">
                        &copy; 2017 Pablo Bustos All Rights Reserved.
                    </div>

                </div>

            </div>
        </div>
</div>
</footer>
</div>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>
</body>

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/account-home.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:38:54 GMT -->
</html>
