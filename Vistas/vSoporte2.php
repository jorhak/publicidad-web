<!DOCTYPE html>
<html lang="es" dir="ltr">

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/blank-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Sep 2018 05:47:53 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Vista de Clientes Registrados</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
 

    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>
<body style="background-color:#D5F5E3">
<div id="wrapper">
    <div class="intro-inner">
        <div class="about-intro" style="
    background:url(images/bg2.jpg) no-repeat center;
	background-size:cover;">
            <div class="dtable hw100">
                <div class="dtable-cell hw100">
                    <div class="container text-center">
                        <h1 class="intro-title animated fadeInDown"> Vista de Clientes</h1>
                    </div>
                </div>
            </div>
        </div>
        <!--/.about-intro -->

    </div>
    <!-- /.intro-inner -->

    <?php require_once("../Modelo/mCliente1.php");
    $objeto_modelo = MCliente::getCliente();
    ?>
    <div class="container main-container">
        <div class="container">
            <div class="row">
                <div class=" col-xl-8">
                    <div class="card card-dark card-elements">
                        <div class="card-body">
                            <h3>Tabla de Clientes</h3>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>CI</th>
                                        <th>NOMBRE</th>
                                        <th>APELLIDO</th>
                                        <th>TELEFONO</th>
                                        <th>OPERACIONES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($objeto_modelo as $obj) { ?>
                                        <tr class="gradeX">
                                            <td>
                                                <?php echo $obj->ci; ?>
                                            </td>
                                            <td>
                                                <?php echo $obj->nombre; ?>
                                            </td>
                                            <td>
                                                <?php echo $obj->apellido; ?>
                                            </td>
                                            <td>
                                                <?php echo $obj->telefono; ?>
                                            </td>
                                            <td>
                                                <?php $ci = $obj->ci;
                                                $nombre = $obj->nombre;
                                                $apellido = $obj->apellido;
                                                $email = $obj->email;
                                                $telefono = $obj->telefono; ?>
                                                <a href="vCliente1.php?ci=<?php echo $ci ?>&nombre=<?php echo $nombre ?>&apellido=<?php echo $apellido ?>&email=<?php echo $email ?>&telefono=<?php echo $telefono ?>">Modificar</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>

</body>
<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/blank-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Sep 2018 05:47:53 GMT -->
</html>
