<?php

require_once("../Controlador/cCliente.php");
require_once("../Css_colores/css_fuentes_temas.css");

if (!isset($_SESSION['ci_cliente'])) {
    header('location:login.html');
}
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Crear Anuncio</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>

<?php

if ($_SESSION['color_tema'] == 0) {
    echo "<body style='background-color:#D5F5E3'>";
}
if ($_SESSION['color_tema'] == 1) {
    echo("<body style='background-color:#D5D5D5'>");
}
if ($_SESSION['color_tema'] == 2) {
    echo("<body style='background-color:#7493FF'>");
}
if ($_SESSION['color_tema'] == 3) {
    echo("<body style='background-color:#FAFD95'>");
}

?>

<div id="wrapper">

    <div class="header">
        <?php if ($_SESSION['color_tema'] == 0) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: white'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 1) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: yellow'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 2) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #E0F4FA'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 3) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #4CD7FF'>";
        }
        ?>
        <div class="container">

            <div class="navbar-identity">
                <a href="" class="navbar-brand logo logo-title">
    			<span class="logo-icon">
                    <!-- <i class="icon icon-search-1 ln-shadow-logo "></i> -->
                    <img src="img/Logo7.png" width="70" height="70">
    			</span>Ubicacion del Inmueble </a>

            </div>
            <!-- Opciones desplegables - barra Menu -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav ml-auto navbar-right">

                    <li class="dropdown no-arrow nav-item"><a href="##" class="dropdown-toggle nav-link"
                                                              data-toggle="dropdown">

                            <?php echo($datos[1] . " " . $datos[2]); ?>


                            <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                        <!-- <ul style="border-color: black"
                            class="dropdown-menu user-menu dropdown-menu-right">
                            <li class="active dropdown-item">
                                <a>
                                    <i class="icon-home"></i>Datos Personales
                                </a>
                            </li>

                            <li class="dropdown-item"><a href="vCuentasbancarias.php"><i class="icon-docs"></i>Cuentas Bancarias</a>
                            </li>

                            <li class="dropdown-item"><a href="account-favourite-ads.html"><i class="icon-heart"></i> Administrar Anuncios </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="vSoporte.php">
                                    <i class="icon-folder-close"></i> Soporte
                                </a>
                            </li>
                            <li class="dropdown-item"><a href="#"><i class=" icon-money "></i> Transacciones </a>
                            </li>
                            <li class="dropdown-item"><a href="vMensajeria.php"><i class="icon-mail"></i> Mensajeria </a>
                            </li>
                            <li class="dropdown-item"><a href="#"><i class="icon-down-open-big"></i> Cambiar Contraseña </a>
                            </li>
                            <li class="dropdown-item"><a href="../Controlador/cerrar_sesion.php"><i class=" icon-logout "></i> Cerrar Sesion </a>
                            </li>
                        </ul> -->
                    </li>
                    <!-- Seguir viendo anuncios -->
                    <li class="postadd nav-item">
                        <a class="btn btn-block btn-border btn-post nav-link" style="background-color: black"
                           href="Anuncios.php">Volver a Anuncios</a>
                    </li>
                    <!-- boton rojo cerrar sesion -->
                    <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                                                    href="../Controlador/cerrar_sesion.php">Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- /.header -->
    <div class="main-container">
        <div class="container">
            <div class="row">

                <!--  Menu Lateral   -->
                <div class="col-md-4 page-content">


                    <div class="col-md-3reg-sidebar">
                        <div class="reg-sidebar-inner text-center">
                            <div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>

                                <h3>Compra tus Productos o Inmmuebles</h3>

                                <p> Este Sistema te ofrece la posibilidad de hacer compras Online con tu tarjeta de
                                    Credito </p>
                            </div>
                            <div class="promo-text-box"><i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>

                                <h3>Crea y Publica tu Anuncio</h3>

                                <p> Puedes ofrecer una variedad de articulos e inmuebles</p>
                            </div>
                            <div class="promo-text-box"><i class="  icon-heart-2 fa fa-4x icon-color-3"></i>

                                <h3>El producto que querias</h3>

                                <p> Este Sistema ofrece una variedad de productos y seguro encontraras el que
                                    necesitas</p>
                            </div>
                        </div>
                    </div>


                </div>
                <!--/.page-sidebar-->
                <!-- Este es eL Cuerpo de la vista y esto muestra el campo de roles y privilegios -->
                <div class="col-md-8 page-content">

                    <div class="inner-box">
                        <div class="welcome-msg">
                        </div>
                        <div id="accordion" class="panel-group">
                            <div class="card card-default">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <a href="#collapseB1" aria-expanded="true" data-toggle="collapse">
                                            <strong>Agregar ubicacion</strong>
                                        </a>

                                    </h4>
                                    <br>

                                    <form method="post" action="../Controlador/cAddUbicacion.php" name="form">
                                        <label>
                                            <strong>Escriba direccion del inmueble a vender</strong>
                                        </label>
                                        <br>
                                        <textarea name="direccion" cols="30" rows="7" required></textarea>
                                        <br>
                                        <label> <strong>Zona</strong></label>
                                        <br>
                                        <select name="zona" required class="form-control">

                                            <option>Norte</option>
                                            <option>Sur</option>
                                            <option>Este</option>
                                            <option>Oeste</option>
                                        </select>
                                        <br>
                                        <label><strong>Selecciona un departamento</strong></label>
                                        <br>
                                        <select name="departamentos" required class="form-control">

                                            <option value="Santa Cruz">Santa Cruz</option>
                                            <option value="La Paz">La Paz</option>
                                            <option value="Cochabamba">Cochabamba</option>
                                            <option value="Chuquisaca">Chuquisaca</option>
                                            <option value="Tarija">Tarija</option>
                                            <option value="Beni">Beni</option>
                                            <option value="Pando">Pando</option>
                                            <option value="Oruro">Oruro</option>
                                            <option value="Potosi">Potosi</option>

                                        </select>
                                        <BR>
                                        <br>
                                        <input type="submit" name="btn_addUbi" class="btn btn-primary"
                                               value="Siguiente" style="background-color: blue">
                                    </form>

                                </div>

                            </div>

                        </div>

                    </div>
                    <!--/.row-box End-->
                </div>

            </div>
            <!--/.page-content-->

        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->


<footer class="main-footer">


    <?php if ($_SESSION['color_tema'] == 0) {
        echo "<div class='footer-content' style='background-color: white'>";
    } ?>

    <?php if ($_SESSION['color_tema'] == 1) {
        echo "<div class='footer-content' style='background-color: yellow'>";
    } ?>

    <?php if ($_SESSION['color_tema'] == 2) {
        echo "<div class='footer-content' style='background-color: #E0F4FA'>";
    } ?>

    <?php if ($_SESSION['color_tema'] == 3) {
        echo "<div class='footer-content' style='background-color: #4CD7FF'>";
    } ?>

    <div class="container">
        <div class="row">
            <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                <div class="footer-col">
                    <h4 class="footer-title">Ayuda y Contacto</h4>
                    <ul class="list-unstyled footer-nav">
                        <li><a href="#">
                                Soporte Online
                            </a></li>
                        <li><a href="#">
                                Como vender?</a></li>
                        <li><a href="#">
                                Como comprar?
                            </a></li>
                        <li><a href="terminos_y_condiciones.html">Terminos y Condiciones
                            </a></li>
                    </ul>
                </div>
            </div>

            <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
            </div>

            <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                <div class="footer-col">
                    <h4 class="footer-title">Mi Cuenta</h4>
                    <ul class="list-unstyled footer-nav">
                        <li><a href="login.html">Login
                            </a></li>
                        <li><a href="vNewCuentaUser.php">Registro
                            </a></li>
                    </ul>
                </div>
            </div>

            <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
            </div>

            <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                <div class="footer-col row">

                    <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                        <div class="mobile-app-content">
                            <h4 class="footer-title">App Movil</h4>
                            <div class="row ">
                                <div class="col-6  ">
                                    <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                                        <span class="hide-visually">iOS app</span>
                                        <img src="images/site/app_store_badge.svg" alt="Available on the App Store">
                                    </a>
                                </div>
                                <div class="col-6  ">
                                    <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                                        <span class="hide-visually">Android App</span>
                                        <img src="images/site/google-play-badge.svg" alt="Available on the App Store">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                        <div class="hero-subscribe">
                            <h4 class="footer-title no-margin">Siguenos en:</h4>
                            <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                <li><a class="icon-color fb" title="Facebook" data-placement="top" data-toggle="tooltip"
                                       href="#"><i class="fab fa-facebook-f"></i> </a></li>
                                <li><a class="icon-color tw" title="Twitter" data-placement="top" data-toggle="tooltip"
                                       href="#"><i class="fab fa-twitter"></i> </a></li>
                                <li><a class="icon-color gp" title="Google+" data-placement="top" data-toggle="tooltip"
                                       href="#"><i class="fab fa-google-plus-g"></i> </a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <div style="clear: both"></div>

            <div class="col-xl-12">
                <div class=" text-center paymanet-method-logo">

                    <img src="images/site/payment/master_card.png" alt="img">
                    <img alt="img" src="images/site/payment/visa_card.png">
                    <img alt="img" src="images/site/payment/paypal.png">
                    <img alt="img" src="images/site/payment/american_express_card.png"> <img alt="img"
                                                                                             src="images/site/payment/discover_network_card.png">
                    <img alt="img" src="images/site/payment/google_wallet.png">
                </div>

                <div class="copy-info text-center">
                    &copy; 2017 Pablo Bustos All Rights Reserved.
                </div>

            </div>

        </div>
    </div>
    </div>
</footer>
</div>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>
</body>
<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/account-home.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:38:54 GMT -->
</html>
