<?php

require_once("../Controlador/cCliente.php");
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Perfil de Usuario</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>
<body style="background-color:#D5F5E3">
<div id="wrapper">

    <div class="header">
        <nav class="navbar  fixed-top navbar-site navbar-light bg-light navbar-expand-md"
             role="navigation">
            <div class="container">

                <div class="navbar-identity">
                    <a href="" class="navbar-brand logo logo-title">
                        <span class="logo-icon">
                    <!-- <i class="icon icon-search-1 ln-shadow-logo "></i> -->
                            <img src="img/Logo7.png" width="70" height="70">
                        </span>Administrar <span>Usuario </span> </a>
                </div>
                <!-- Opciones desplegables - barra Menu -->

                <!--/.nav-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- /.header -->
    <div class="main-container">
        <div class="container">
            <div class="row">

                <!--/.page-sidebar-->
                <!-- Este es eL Cuerpo de la vista y esto muestra el campo de roles y privilegios -->
                <div class="col-md-9 page-content">
                    <?php
                    $ci = $_REQUEST['ci'];
                    $nombre = $_REQUEST['nombre'];
                    $apellido = $_REQUEST['apellido'];
                    $email = $_REQUEST['email'];
                    $telefono = $_REQUEST['telefono'];
                    ?>

                    <?php if (!isset($_POST['btn-editar'])) { ?>
                        <div class="inner-box">
                            <div class="welcome-msg">
                                <h3 class="page-sub-header2 clearfix no-padding">Bienvenido Cliente </h3>
                                <span class="page-sub-header-sub small">Estos son sus datos registrados</span>
                            </div>
                            <div id="accordion" class="panel-group">
                                <div class="card card-default">
                                    <div class="card-header">
                                        <h4 class="card-title">
                                            <a href="#collapseB1" aria-expanded="true" data-toggle="collapse">
                                                Datos Personales
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse show" id="collapseB1">
                                        <div class="card-body">

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Cedula Identidad</label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" readonly name="ci_cli"
                                                           style="width: 200px" value="<?php echo $_REQUEST['ci']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Nombres</label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" readonly
                                                           value="<?php echo $_REQUEST['nombre']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Apellidos</label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" readonly
                                                           value="<?php echo $_REQUEST['apellido']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Email</label>

                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" readonly
                                                           value="<?php echo $_REQUEST['email']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Phone" class="col-sm-3 control-label">Telefono /
                                                    Celular</label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="Phone" readonly
                                                           style="width: 250px"
                                                           value="<?php echo $_REQUEST['telefono']; ?>">
                                                </div>
                                            </div>
                                            <form method="POST">
                                                <div class="form-group" align="center">
                                                    <div class="col-sm-offset-3 col-sm-9">
                                                        <button type="submit" name="btn-editar" class="btn btn-primary">
                                                            Editar Datos
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.row-box End-->
                        </div>
                    <?php } else { ?>
                        <div class="inner-box">
                            <div class="welcome-msg">
                                <h3 class="page-sub-header2 clearfix no-padding">Editar Datos Personales: </h3>
                                <span class="page-sub-header-sub small">Estos son sus datos registrados</span>
                            </div>
                            <div id="accordion" class="panel-group">
                                <div class="card card-default">
                                    <div class="card-header">
                                        <h4 class="card-title">
                                            <a href="#collapseB1" aria-expanded="true" data-toggle="collapse">
                                                Datos Personales
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse show" id="collapseB1">
                                        <div class="card-body">
                                            <form action="../Controlador/cCliente1.php" method="POST">

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">CI</label>
                                                    <div class="col-sm-9">
                                                        <input type="ci" readonly name="ci_old"
                                                               class="form-control"
                                                               style="width: 300px"
                                                               value="<?php echo $ci; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <!--  <label class="col-sm-3 control-label">CI Nuevo</label> -->
                                                    <div class="col-sm-9">
                                                        <input type="ci" class="form-control" name="ci" hidden="hidden"
                                                               style="width: 300px">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Nombres</label>
                                                    <div class="col-sm-9">
                                                        <input type="nombre" readonly name="nombre_old"
                                                               class="form-control"
                                                               value="<?php echo $nombre; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Nombres Nuevos</label>
                                                    <div class="col-sm-9">
                                                        <input type="nombre" class="form-control" name="nombre">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Apellidos</label>
                                                    <div class="col-sm-9">
                                                        <input type="apellidos" readonly name="apellidos_old"
                                                               class="form-control"
                                                               value="<?php echo $apellido; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Apellidos Nuevos</label>
                                                    <div class="col-sm-9">
                                                        <input type="apellidos" class="form-control" name="apellidos">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Email Actual</label>
                                                    <div class="col-sm-9">
                                                        <input type="email" readonly name="email_old"
                                                               class="form-control"
                                                               value="<?php echo $email; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Email Nuevo</label>
                                                    <div class="col-sm-9">
                                                        <input type="email" class="form-control" name="email">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="Phone" class="col-sm-3 control-label">Telefono
                                                        Actual</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" readonly name="cel_old"
                                                               style="width: 300px" value="<?php echo $telefono; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Phone" class="col-sm-3 control-label">
                                                        Nuevo Telefono
                                                    </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="telefono"
                                                               style="width: 300px">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Contraseña Nueva</label>
                                                    <div class="col-sm-9">
                                                        <input type="password" class="form-control" name="password"
                                                               style="width: 300px">
                                                    </div>
                                                </div>

                                                <div class="form-group" align="center">
                                                    <div class="col-sm-offset-3 col-sm-9">
                                                        <button type="submit" class="btn btn-primary"
                                                                name="btn-Guardar">Guardar Cambios
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.row-box End-->
                        </div>

                    <?php }; ?>

                </div>
                <!--/.page-content-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- /.main-container -->


    <footer class="main-footer">
        <div class="footer-content">
            <div class="container">
                <div class="row">
                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                        <div class="footer-col">
                            <h4 class="footer-title">Ayuda y Contacto</h4>
                            <ul class="list-unstyled footer-nav">
                                <li><a href="#">
                                        Soporte Online
                                    </a></li>
                                <li><a href="#">
                                        Como vender?</a></li>
                                <li><a href="#">
                                        Como comprar?
                                    </a></li>
                                <li><a href="terminos_y_condiciones.html">Terminos y Condiciones
                                    </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    </div>

                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                        <div class="footer-col">
                            <h4 class="footer-title">Mi Cuenta</h4>
                            <ul class="list-unstyled footer-nav">
                                <li><a href="login.html">Login
                                    </a></li>
                                <li><a href="vNewCuentaUser.php">Registro
                                    </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    </div>

                    <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                        <div class="footer-col row">

                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                <div class="mobile-app-content">
                                    <h4 class="footer-title">App Movil</h4>
                                    <div class="row ">
                                        <div class="col-6  ">
                                            <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                                                <span class="hide-visually">iOS app</span>
                                                <img src="images/site/app_store_badge.svg"
                                                     alt="Available on the App Store">
                                            </a>
                                        </div>
                                        <div class="col-6  ">
                                            <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                                                <span class="hide-visually">Android App</span>
                                                <img src="images/site/google-play-badge.svg"
                                                     alt="Available on the App Store">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                                <div class="hero-subscribe">
                                    <h4 class="footer-title no-margin">Siguenos en:</h4>
                                    <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                        <li><a class="icon-color fb" title="Facebook" data-placement="top"
                                               data-toggle="tooltip" href="#"><i class="fab fa-facebook-f"></i> </a>
                                        </li>
                                        <li><a class="icon-color tw" title="Twitter" data-placement="top"
                                               data-toggle="tooltip" href="#"><i class="fab fa-twitter"></i> </a></li>
                                        <li><a class="icon-color gp" title="Google+" data-placement="top"
                                               data-toggle="tooltip" href="#"><i class="fab fa-google-plus-g"></i> </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div style="clear: both"></div>

                    <div class="col-xl-12">
                        <div class=" text-center paymanet-method-logo">

                            <img src="images/site/payment/master_card.png" alt="img">
                            <img alt="img" src="images/site/payment/visa_card.png">
                            <img alt="img" src="images/site/payment/paypal.png">
                            <img alt="img" src="images/site/payment/american_express_card.png"> <img alt="img"
                                                                                                     src="images/site/payment/discover_network_card.png">
                            <img alt="img" src="images/site/payment/google_wallet.png">
                        </div>

                        <div class="copy-info text-center">
                            &copy; 2017 Pablo Bustos All Rights Reserved.
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </footer>


</div>
<!-- /.wrapper -->

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>
</body>

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/account-home.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:38:54 GMT -->
</html>
