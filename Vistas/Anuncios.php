<?php

require_once("../Controlador/cCliente.php");
require_once("../Controlador/conexion.php");
require_once("../Css_colores/css_fuentes_temas.css");

if (!isset($_SESSION['ci_cliente'])) {
    header('location:login.html');
}

$db = Conectar::conexion();

// Mostrar Datos para el Anuncio

$sql = $db->query("SELECT a.id,a.titulo,pi.precio,di.superficie,di.cant_habitaciones,u.dpto,u.direccion,u.zona,g.imagen 
    from anuncio as a, detalle_inmueble as di, ubicacion as u, galeria as g, producto_inmueble as pi, categoria as c
    where a.id_categoria=c.id and a.id_prod_inmu = pi.id and pi.id=di.id_prod_inmu and pi.id_ubicacion=u.id and pi.id=g.id_prod_inmu and g.url='foto1' and a.id_categoria=5;");


$sql2 = $db->query("SELECT a.id,a.titulo,pi.precio,dp.marca,c.descripcion,u.direccion,pi.estado,g.imagen 
    from anuncio as a, producto_inmueble as pi, detalle_producto as dp ,categoria as c , galeria as g , ubicacion as u
    where a.id_categoria = c.id and a.id_prod_inmu=pi.id and pi.id_ubicacion = u.id and pi.id=g.id_prod_inmu 
    and pi.id=dp.id_prod_inmu and g.url='foto1' and a.id_categoria <>5;");


?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/property-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:26:42 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <link rel="stylesheet" href="../alertifyJS/css/alertify.min.css"/>
    <link rel="stylesheet" href="../alertifyJS/css/themes/semantic.min.css"/>


    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Publicidad Web</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>
</head>
<?php

if ($_SESSION['color_tema'] == 0) {
    echo "<body style='background-color:#D5F5E3'>";
}
if ($_SESSION['color_tema'] == 1) {
    echo("<body style='background-color:#D5D5D5'>");
}
if ($_SESSION['color_tema'] == 2) {
    echo("<body style='background-color:#7493FF'>");
}
if ($_SESSION['color_tema'] == 3) {
    echo("<body style='background-color:#FAFD95'>");
}

?>

<div id="wrapper">

    <div class="header">

        <?php if ($_SESSION['color_tema'] == 0) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: white'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 1) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: yellow'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 2) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #E0F4FA'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 3) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #4CD7FF'>";
        }
        ?>


        <div class="container">

            <div class="navbar-identity">
                <a href="" class="navbar-brand logo logo-title">
                <span class="logo-icon">
                    <!-- <i class="icon icon-search-1 ln-shadow-logo "></i> -->
                    <img src="img/Logo7.png" width="70" height="70">
                </span>Publicidad Web </a>

            </div>


            <!-- Opciones desplegables - barra Menu -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav ml-auto navbar-right">

                    <li class="dropdown no-arrow nav-item">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                            Temas
                            <i class="icon-home fa"></i> <i class=" icon-down-open-big fa"></i></a>
                        <ul style="border-color: black;"
                            class="dropdown-menu user-menu dropdown-menu-right">
                            <li class="active dropdown-item">
                                <a href="../Controlador/cColor.php?id_tema=0" style="background-color: #D5F5E3">
                                    <i class="icon-home"></i>
                                    <no>Tema 1</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_tema=1" style="background-color: #FAFD95">
                                    <i class="icon-home"></i>
                                    <no>Tema 2</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_tema=2" style="background-color: blue">
                                    <i class="icon-home"></i>
                                    <no>Tema 3</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_tema=3" style="background-color: #4CD7FF">
                                    <i class=" icon-home "></i>
                                    <no>Tema 4</no>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="dropdown no-arrow nav-item">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                            Fuente
                            <i class="icon-heart fa"></i> <i class=" icon-down-open-big fa"></i></a>
                        <ul style="border-color: black;"
                            class="dropdown-menu user-menu dropdown-menu-right">

                            <li class="disable dropdown-item">
                                <no> Color</no>
                            </li>


                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_color_letra=0" style="background-color:white">
                                    <i class="icon-heart"></i>
                                    <no>Negro</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_color_letra=1"
                                   style="color: red ;background: white;">
                                    <i class="icon-heart"></i>
                                    <no>Rojo</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_color_letra=2"
                                   style="color: blue ;background: white;">
                                    <i class="icon-heart"></i>
                                    <no>Azul</no>
                                </a>
                            </li>


                            <li class="disable dropdown-item">
                                <no> Tamaño</no>
                            </li>


                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_size_letra=0"
                                   style="color:black;background: white;">
                                    <i class="icon-money"></i>
                                    <no>Normal</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_size_letra=1"
                                   style="color: black ;background: white;">
                                    <i class="icon-money"></i>
                                    <no>Grande</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_size_letra=2"
                                   style="color: black ;background: white;">
                                    <i class="icon-money"></i>
                                    <no>Mas Grande</no>
                                </a>
                            </li>

                            <li class="disable dropdown-item">
                                <no> Estilo</no>
                            </li>


                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_fuente_letra=0"
                                   style="color:black;background: white;">
                                    <i class="icon-docs"></i>
                                    <no>Arial</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_fuente_letra=1"
                                   style="color: black ;background: white;">
                                    <i class="icon-docs"></i>
                                    <no>Arial Black</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_fuente_letra=2"
                                   style="color: black ;background: white;">
                                    <i class="icon-docs"></i>
                                    <no>Magneto</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_fuente_letra=3"
                                   style="color: black ;background: white;">
                                    <i class="icon-docs"></i>
                                    <no>Arial Rounded MT</no>
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="../Controlador/cColor.php?id_fuente_letra=4"
                                   style="color: black ;background: white;">
                                    <i class="icon-docs"></i>
                                    <no>Harlow Solid</no>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="dropdown no-arrow nav-item"><a href="##" class="dropdown-toggle nav-link"
                                                              data-toggle="dropdown">

                            <no> <?php echo($datos[1] . " " . $datos[2]); ?>

                            </no>
                            <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                        <ul style="border-color: black;"
                            class="dropdown-menu user-menu dropdown-menu-right">
                            <li class="disable dropdown-item">
                                <no> Modulo Usuario</no>
                            </li>
                            <li class="active dropdown-item">
                                <a href="vCliente.php">
                                    <i class="icon-home"></i>Datos Personales
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="vSoporte.php">
                                    <i class="icon-folder-close"></i> Soporte Tecnico
                                </a>
                            </li>

                            <li class="dropdown-item"><a href="vCambiarPass.php"><i class="icon-down-open-big"></i>
                                    Cambiar Contraseña </a>
                            </li>

                            <li class="disable dropdown-item">
                                <no> Modulo Publicidad</no>
                            </li>

                            <li class="dropdown-item"><a href="vAdmAnuncios.php"><i class="icon-heart"></i>
                                    Administrar Anuncios </a>
                            </li>

                            <li class="dropdown-item"><a href="vMensajeria.php"><i class="icon-mail"></i> Bandeja de
                                    Entrada </a>
                            </li>

                            <li class="disable dropdown-item">
                                <no> Modulo Pagos</no>
                            </li>


                            <li class="dropdown-item"><a href="#"><i class=" icon-money "></i> Transacciones </a>
                            </li>

                            <li class="dropdown-item"><a href="vCuentasbancarias.php"><i class="icon-docs"></i>Cuentas
                                    Bancarias</a>
                            </li>

                        </ul>
                    </li>


                    <!-- boton rojo cerrar sesion -->

                    <li class="postadd nav-item">


                        <!-- <a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                            href="../Controlador/cerrar_sesion.php">Cerrar Sesión
                        </a> -->

                        <?php if ($_SESSION['color_tema'] == 0) {
                            echo "<a class='btn btn-lg  btn-primary-lg ' 
                                href='../Controlador/cerrar_sesion.php' style='background: red';>
                                Cerrar Sesión
                              </a>";
                        } ?>

                        <?php if ($_SESSION['color_tema'] == 1) {
                            echo "<a class='btn btn-lg  btn-primary-lg ' 
                                href='../Controlador/cerrar_sesion.php' style='background: #00FF24; color: black;font-size: 15px;';>
                                Cerrar Sesión
                              </a>";
                        } ?>

                        <?php if ($_SESSION['color_tema'] == 2) {
                            echo "<a class='btn btn-lg  btn-primary-lg ' 
                                href='../Controlador/cerrar_sesion.php' style='background: black'color: white;font-size: 15px;';>
                                Cerrar Sesión
                              </a>";
                        } ?>

                        <?php if ($_SESSION['color_tema'] == 3) {
                            echo "<a class='btn btn-lg  btn-primary-lg ' 
                                href='../Controlador/cerrar_sesion.php' style='background: #FFFF00; color: black;font-size: 12px';>
                                Cerrar Sesión
                              </a>";
                        } ?>


                    </li>


                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- /.header -->
    <div class="search-row-wrapper" style="background-image: url(images/bg.jpg)">
        <div class="inner">
            <div class="container ">
                <form method="POST" action="Anuncios.php">
                    <div class="row">


                        <div class="col-md-3">
                            <input class="form-control keyword" type="text" name="zona"
                                   placeholder="Ingrese su Busqueda">
                        </div>
                        <div class="col-md-3">
                            <select class="form-control selecter" name="dpto" id="search-category">
                                <option selected="selected" value="">Buscar por Departamento</option>
                                <option value="Santa Cruz">Santa Cruz</option>
                                <option value="La Paz">La Paz</option>
                                <option value="Cochabamba">Cochabamba</option>
                                <option value="Pando">Pando</option>
                                <option value="Beni">Beni</option>
                                <option value="Oruro">Oruro</option>
                                <option value="Potosi">Potosi</option>
                                <option value="Tarija">Tarija</option>
                                <option value="Chuquisaca">Chuquisaca</option>
                            </select>
                        </div>

                        <div class="col-md-3">

                            <?php if ($_SESSION['color_tema'] == 0) {
                                echo "<button  class='btn btn-block btn-lg btn-gradient' name='enviar' style='background-color: Red; color: white;'>
                                     Buscar 
                                <i class='fa fa-search'></i>
                              </button>";
                            } ?>
                            <?php if ($_SESSION['color_tema'] == 1) {
                                echo "<button class='btn btn-block btn-lg btn-gradient' name='enviar' style='background: #00FF24; color: black;font-size: 15px;'>
                                     Buscar 
                                <i class='fa fa-search'></i>
                              </button>";
                            } ?>
                            <?php if ($_SESSION['color_tema'] == 2) {
                                echo "<button class='btn btn-block btn-lg btn-gradient' name='enviar' style='background: white; color: black;font-size: 15px;'>
                                     Buscar 
                                <i class='fa fa-search'></i>
                              </button>";
                            } ?>

                            <?php if ($_SESSION['color_tema'] == 3) {
                                echo "<button class='btn btn-block btn-lg btn-gradient' name='enviar' style='background-color: #4CD7FF'>
                                     Buscar 
                                <i class='fa fa-search'></i>
                              </button>";
                            } ?>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.search-row -->
    <div class="main-container">
        <div class="container">
            <div class="row">
                <!-- this (.mobile-filter-sidebar) part will be position fixed in mobile version -->
                <div class="col-sm-3 page-sidebar mobile-filter-sidebar">
                    <aside>
                        <div class="sidebar-modern-inner">

                            <div class="block-content categories-list  list-filter ">
                                <li>
                                    <button class="btn btn-primary" type="submit"
                                            onclick="location.href='verif_prod_inmu.php'"
                                            style="background-color: blue">
                                        Crear Nuevo Anuncio
                                    </button>
                                </li>
                            </div>  <!--/.categories-list-->

                            <!--/.locations-list-->

                            <div class="block-title has-arrow">
                                <h5><a href="#"><strong>Rango de Precio</strong></a></h5>
                            </div>
                            <div class="block-content categories-list  list-filter ">

                                <form method="POST" action="Anuncios.php" role="form" class="form-inline">
                                    <div class="form-group col-lg-4 col-md-12 no-padding">
                                        <input type="text" placeholder="inicial " id="min" name="min"
                                               class="form-control">
                                    </div>
                                    <div class="form-group col-lg-1 col-md-12 no-padding text-center hidden-md"> -
                                    </div>
                                    <div class="form-group col-lg-4 col-md-12 no-padding">
                                        <input type="text" placeholder="final " id="max" name="max"
                                               class="form-control">
                                    </div>
                                    <div class="form-group col-lg-3 col-md-12 no-padding">
                                        <button class="btn btn-default pull-right btn-block-md" name='entre'
                                                type="submit">GO
                                        </button>
                                    </div>
                                </form>
                                <div style="clear:both"></div>
                            </div>
                            <!--/.list-filter-->
                            <!-- <div class="block-title has-arrow">
                                <h5><a href="#"><strong>¿Que Producto Buscas? </strong></a></h5>
                            </div>
                            <div class="block-content categories-list  list-filter ">
                                <ul class="browse-list list-unstyled ">
                                    <li><a href="#"><strong>Inmuebles</strong></a></li>
                                    <li><a href="#"><strong>Productos</strong></a></li>

                                </ul>
                            </div> -->
                            <!--/.list-filter-->

                            <div style="clear:both"></div>
                        </div>


                        <!--/.categories-list-->
                    </aside>
                </div>
                <!--/.page-side-bar-->
                <div class="col-md-9 page-content col-thin-left">

                    <div class="category-list">
                        <div class="tab-box ">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs add-tabs tablist" role="tablist">
                                <li class="active nav-item">
                                    <a href="#alladslist" role="tab" data-toggle="tab" class="nav-link">Inmuebles
                                    </a>
                                </li>
                                <li class=" nav-item">
                                    <a href="#businessads" role="tab" data-toggle="tab" class="nav-link"> Productos
                                    </a>
                                </li>

                            </ul>
                            <div class="tab-filter">
                                <select name="precio" id="precio" onchange="enviar_valores(this.value)"
                                        class="selectpicker select-sort-by"
                                        data-style="btn-select" data-width="auto">
                                    <option value="">Ordenar por Precio</option>
                                    <option value="1">Menor a Mayor</option>
                                    <option value="2">Mayor a Menor</option>
                                </select>
                            </div>

                        </div>
                        <!--/.tab-box-->


                        <!-- // Clase Pestañas -->
                        <div class="tab-content">
                            <!-- // Pestaña Inmuebles -->
                            <div class="tab-pane  active " id="alladslist">


                                <div class="adds-wrapper row no-margin property-list">


                                    <?php if (isset($_POST['enviar'])) { ?>
                                        <?php
                                        if (isset($_POST['zona']) && isset($_POST['dpto'])) {
                                            $zona = $_POST['zona'];
                                            $dpto = $_POST['dpto'];
                                        } else {
                                            header("Location: Vistas/Anuncio.php");
                                        }
                                        ?>
                                        <?php
                                        $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_inmueble.superficie,
                                            detalle_inmueble.cant_habitaciones,ubicacion.dpto,ubicacion.direccion,ubicacion.zona,galeria.imagen
                                            FROM anuncio 
                                            Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                            Inner Join detalle_inmueble ON producto_inmueble.id = detalle_inmueble.id_prod_inmu
                                            Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                            Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                            WHERE ubicacion.dpto = '$dpto' and ubicacion.zona='$zona' and galeria.url='foto1';";
                                        $sql = $db->query($query);
                                        ?>
                                        <?php while ($row = mysqli_fetch_array($sql)) { ?>
                                            <!-- fila anuncio inmueble -->
                                            <div class="item-list">
                                                <div class="row">
                                                    <div class="col-md-3 no-padding photobox">
                                                        <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>
                                                            <img
                                                                src="data:image/jpg;base64,<?php echo base64_encode($row[8]); ?>"
                                                                alt="img"/>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!--/.photobox-->
                                                    <div class="col-md-6 add-desc-box">
                                                        <div class="ads-details">
                                                            <h5 class="add-title">
                                                                <?php
                                                                echo "<a href='vAnuncio_Inmueble.php?id=" . $row[0] . "' >";
                                                                ?>
                                                                <titulo>
                                                                    <?php echo($row[1]); ?>
                                                                </titulo>
                                                                </a>
                                                            </h5>
                                                            <span class="info-row">
                                                        <span class="item-location">
                                                            <?php echo($row[6]); ?>
                                                        </span>
                                                    </span>
                                                            <div class="prop-info-box">
                                                                <div class="prop-info">
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title ">
                                                                    <dAnuncio>
                                                                    <?php echo($row[5]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Departamento</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-area">
                                                                    <dAnuncio>
                                                                        <?php echo($row[3]); ?> m²
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Superficie</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">
                                                                    <dAnuncio>
                                                                        <?php echo($row[4]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Habitaciones </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                    <div class="col-md-3 text-right  price-box">
                                                        <h3 class="item-price ">
                                                            Bs. <?php echo($row[2]); ?>
                                                        </h3>
                                                        <div style="clear: both"></div>
                                                        <?php
                                                        echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                        href='vAnuncio_Inmueble.php?id=" . $row[0] . "'>";
                                                        ?>
                                                        Ver Anuncio
                                                        </a>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                </div>
                                            </div>
                                            <!--/.item-list-->
                                        <?php }; ?>
                                    <?php }; ?>


                                    <?php if (isset($_POST['entre'])) { ?>
                                        <?php
                                        if (isset($_POST['min']) && isset($_POST['max'])) {
                                            $min = $_POST['min'];
                                            $max = $_POST['max'];
                                        } else {
                                            header("Location: Anuncios.php");
                                        }
                                        ?>
                                        <?php
                                        $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_inmueble.superficie,
                                            detalle_inmueble.cant_habitaciones,ubicacion.dpto,ubicacion.direccion,ubicacion.zona,galeria.imagen
                                            FROM anuncio 
                                            Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                            Inner Join detalle_inmueble ON producto_inmueble.id = detalle_inmueble.id_prod_inmu
                                            Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                            Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                            WHERE producto_inmueble.precio BETWEEN '$min' and '$max'  and galeria.url='foto1';";
                                        $sql = $db->query($query);
                                        ?>
                                        <?php while ($row = mysqli_fetch_array($sql)) { ?>
                                            <!-- fila anuncio inmueble -->
                                            <div class="item-list">
                                                <div class="row">
                                                    <div class="col-md-3 no-padding photobox">
                                                        <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>
                                                            <img
                                                                src="data:image/jpg;base64,<?php echo base64_encode($row[8]); ?>"
                                                                alt="img"/>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!--/.photobox-->
                                                    <div class="col-md-6 add-desc-box">
                                                        <div class="ads-details">
                                                            <h5 class="add-title">
                                                                <?php
                                                                echo "<a href='vAnuncio_Inmueble.php?id=" . $row[0] . "' >";
                                                                ?>
                                                                <titulo>
                                                                    <?php echo($row[1]); ?>
                                                                </titulo>
                                                                </a>
                                                            </h5>
                                                            <span class="info-row">
                                                        <span class="item-location">
                                                            <?php echo($row[6]); ?>
                                                        </span>
                                                    </span>
                                                            <div class="prop-info-box">
                                                                <div class="prop-info">
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title ">
                                                                    <dAnuncio>
                                                                    <?php echo($row[5]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Departamento</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-area">
                                                                    <dAnuncio>
                                                                        <?php echo($row[3]); ?> m²
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Superficie</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">
                                                                    <dAnuncio>
                                                                        <?php echo($row[4]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Habitaciones </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                    <div class="col-md-3 text-right  price-box">
                                                        <h3 class="item-price ">
                                                            Bs. <?php echo($row[2]); ?>
                                                        </h3>
                                                        <div style="clear: both"></div>
                                                        <?php
                                                        echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                        href='vAnuncio_Inmueble.php?id=" . $row[0] . "' >";
                                                        ?>
                                                        Ver Anuncio
                                                        </a>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                </div>
                                            </div>
                                            <!--/.item-list-->
                                        <?php }; ?>
                                    <?php }; ?>

                                    <?php if (isset($_REQUEST['valor'])) { ?>
                                        <?php
                                        $valor = $_REQUEST['valor'];
                                        if ($valor == 1) {
                                            $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_inmueble.superficie,
                                                detalle_inmueble.cant_habitaciones,ubicacion.dpto,ubicacion.direccion,ubicacion.zona,galeria.imagen,galeria.url
                                                FROM anuncio 
                                                Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                                Inner Join detalle_inmueble ON producto_inmueble.id=detalle_inmueble.id_prod_inmu
                                                Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                                Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                                WHERE producto_inmueble.precio>0 and galeria.url='foto1' order by precio asc;";
                                        } else {
                                            $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_inmueble.superficie,
                                                detalle_inmueble.cant_habitaciones,ubicacion.dpto,ubicacion.direccion,ubicacion.zona,galeria.imagen,galeria.url
                                                FROM anuncio 
                                                Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                                Inner Join detalle_inmueble ON producto_inmueble.id=detalle_inmueble.id_prod_inmu
                                                Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                                Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                                WHERE producto_inmueble.precio>0 and galeria.url='foto1' order by precio desc;";
                                        }
                                        ?>
                                        <?php

                                        $sql = $db->query($query);
                                        ?>
                                        <?php while ($row = mysqli_fetch_array($sql)) { ?>
                                            <!-- fila anuncio inmueble -->
                                            <div class="item-list">
                                                <div class="row">
                                                    <div class="col-md-3 no-padding photobox">
                                                        <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>
                                                            <img
                                                                src="data:image/jpg;base64,<?php echo base64_encode($row[8]); ?>"
                                                                alt="img"/>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!--/.photobox-->
                                                    <div class="col-md-6 add-desc-box">
                                                        <div class="ads-details">
                                                            <h5 class="add-title">
                                                                <?php
                                                                echo "<a href='vAnuncio_Inmueble.php?id=" . $row[0] . "' >";
                                                                ?>
                                                                <titulo>
                                                                    <?php echo($row[1]); ?>
                                                                </titulo>
                                                                </a>
                                                            </h5>
                                                            <span class="info-row">
                                                        <span class="item-location">
                                                            <?php echo($row[6]); ?>
                                                        </span>
                                                    </span>
                                                            <div class="prop-info-box">
                                                                <div class="prop-info">
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title ">
                                                                    <dAnuncio>
                                                                    <?php echo($row[5]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Departamento</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-area">
                                                                    <dAnuncio>
                                                                        <?php echo($row[3]); ?> m²
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Superficie</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">
                                                                    <dAnuncio>
                                                                        <?php echo($row[4]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Habitaciones </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                    <div class="col-md-3 text-right  price-box">
                                                        <h3 class="item-price ">
                                                            Bs. <?php echo($row[2]); ?>
                                                        </h3>
                                                        <div style="clear: both"></div>
                                                        <?php
                                                        echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                        href='vAnuncio_Inmueble.php?id=" . $row[0] . "' >";
                                                        ?>
                                                        Ver Anuncio
                                                        </a>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                </div>
                                            </div>
                                            <!--/.item-list-->
                                        <?php }; ?>
                                    <?php }; ?>


                                    <?php if (!isset($_POST['enviar'])) { ?>
                                        <?php while ($row = mysqli_fetch_array($sql)) { ?>
                                            <!-- fila anuncio inmueble -->
                                            <div class="item-list">
                                                <div class="row">
                                                    <div class="col-md-3 no-padding photobox">
                                                        <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>
                                                            <img
                                                                src="data:image/jpg;base64,<?php echo base64_encode($row[8]); ?>"
                                                                alt="img"/>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!--/.photobox-->
                                                    <div class="col-md-6 add-desc-box">
                                                        <div class="ads-details">
                                                            <h5 class="add-title">
                                                                <?php
                                                                echo "<a href='vAnuncio_Inmueble.php?id=" . $row[0] . "' >";
                                                                ?>
                                                                <titulo>
                                                                    <?php echo($row[1]); ?>
                                                                </titulo>
                                                                </a>
                                                            </h5>
                                                            <span class="info-row">
                                                        <span class="item-location">
                                                            <?php echo($row[6]); ?>
                                                        </span>
                                                    </span>
                                                            <div class="prop-info-box">
                                                                <div class="prop-info">
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title ">
                                                                    <dAnuncio>
                                                                    <?php echo($row[5]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Departamento</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-area">
                                                                    <dAnuncio>
                                                                        <?php echo($row[3]); ?> m²
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Superficie</span>
                                                                    </div>
                                                                    <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">
                                                                    <dAnuncio>
                                                                        <?php echo($row[4]); ?>
                                                                    </dAnuncio>
                                                                </span>
                                                                        <span class="text">Habitaciones </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                    <div class="col-md-3 text-right  price-box">
                                                        <h3 class="item-price ">
                                                            Bs. <?php echo($row[2]); ?>
                                                        </h3>
                                                        <div style="clear: both"></div>
                                                        <?php
                                                        echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                        href='vAnuncio_Inmueble.php?id=" . $row[0] . "' >";
                                                        ?>
                                                        Ver Anuncio
                                                        </a>
                                                    </div>
                                                    <!--/.add-desc-box-->
                                                </div>
                                            </div>
                                            <!--/.item-list-->
                                        <?php }; ?>
                                    <?php }; ?>


                                </div>
                                <!--/.adds-wrapper-->
                            </div>
                            <!-- fin pestaña Inmuebles -->


                            <!--  Pestaña Productos -->
                            <div class="tab-pane" id="businessads">
                                <div class="tab-pane  active " id="alladslist">


                                    <div class="adds-wrapper row no-margin property-list">


                                        <?php if (isset($_POST['enviar'])) { ?>
                                            <?php
                                            if (isset($_POST['zona']) && isset($_POST['dpto'])) {
                                                $zona = $_POST['zona'];
                                                $dpto = $_POST['dpto'];
                                            } else {
                                                header("Location: Anuncios.php");
                                            }
                                            ?>
                                            <?php
                                            $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_producto.marca,
                                            categoria.descripcion,ubicacion.direccion,producto_inmueble.estado,galeria.imagen,galeria.url
                                            FROM anuncio
                                            Inner Join categoria ON anuncio.id_categoria = categoria.id
                                            Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                            Inner Join detalle_producto ON producto_inmueble.id = detalle_producto.id_prod_inmu
                                            Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                            Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                            WHERE ubicacion.dpto = '$dpto' and ubicacion.zona='$zona' and galeria.url='foto1';";
                                            $sql2 = $db->query($query);
                                            ?>
                                            <?php while ($row = mysqli_fetch_array($sql2)) { ?>
                                                <!-- fila anuncio inmueble -->
                                                <div class="item-list">

                                                    <div class="row">

                                                        <div class="col-md-3 no-padding photobox">
                                                            <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>

                                                                <img
                                                                    src="data:image/jpg;base64,<?php echo base64_encode($row[7]); ?>"
                                                                    alt="img"/>

                                                            </div>
                                                        </div>
                                                        <!--/.photobox-->
                                                        <div class="col-md-6 add-desc-box">
                                                            <div class="ads-details">
                                                                <h5 class="add-title">
                                                                    <?php
                                                                    echo "<a href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                                    ?>
                                                                    <titulo>
                                                                        <?php echo($row[1]); ?>
                                                                    </titulo>
                                                                    </a>
                                                                </h5>
                                                                <span class="info-row">
                                                            <span class="item-location">
                                                            <?php echo($row[5]); ?>
                                                            </span><i class="fa fa-map-marker-alt"></i>
                                                        </span>
                                                                <div class="prop-info-box">

                                                                    <div class="prop-info">
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title ">
                                                                        <?php echo($row[3]); ?>
                                                                    </span>
                                                                            <span class="text">Marca</span>
                                                                        </div>

                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-area">
                                                                        <?php echo($row[4]); ?></span>
                                                                            <span class="text">Categoria</span>
                                                                        </div>
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-room">
                                                                        <?php echo($row[6]); ?>
                                                                    </span>
                                                                            <span class="text">Estado </span>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                        <!--/.add-desc-box-->
                                                        <div class="col-md-3 text-right  price-box">

                                                            <h3 class="item-price ">
                                                                <no> Bs. <?php echo($row[2]); ?></no>
                                                            </h3>

                                                            <div style="clear: both"></div>
                                                            <?php
                                                            echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                                    href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                            ?>
                                                            Ver Anuncio
                                                            </a>
                                                        </div>
                                                        <!--/.add-desc-box-->
                                                    </div>
                                                </div>
                                                <!--/.item-list-->
                                            <?php }; ?>
                                        <?php }; ?>

                                        <?php if (isset($_POST['entre'])) { ?>
                                            <?php
                                            if (isset($_POST['min']) && isset($_POST['max'])) {
                                                $min = $_POST['min'];
                                                $max = $_POST['max'];
                                            } else {
                                                header("Location: Vistas/vParametroZonaDptoPrecioOrden.php");
                                            }
                                            ?>
                                            <?php
                                            $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_producto.marca,
                                            categoria.descripcion,ubicacion.direccion,producto_inmueble.estado,galeria.imagen,galeria.url
                                        FROM anuncio
                                        Inner Join categoria ON anuncio.id_categoria=categoria.id
                                        Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                        Inner Join detalle_producto ON producto_inmueble.id=detalle_producto.id_prod_inmu
                                        Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                        Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                        WHERE producto_inmueble.precio BETWEEN '$min' and '$max'  and galeria.url='foto1' order by producto_inmueble.precio;";
                                            $sql2 = $db->query($query);
                                            ?>
                                            <?php while ($row = mysqli_fetch_array($sql2)) { ?>
                                                <!-- fila anuncio inmueble -->
                                                <div class="item-list">

                                                    <div class="row">

                                                        <div class="col-md-3 no-padding photobox">
                                                            <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>

                                                                <img
                                                                    src="data:image/jpg;base64,<?php echo base64_encode($row[7]); ?>"
                                                                    alt="img"/>

                                                            </div>
                                                        </div>
                                                        <!--/.photobox-->
                                                        <div class="col-md-6 add-desc-box">
                                                            <div class="ads-details">
                                                                <h5 class="add-title">
                                                                    <?php
                                                                    echo "<a href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                                    ?>
                                                                    <titulo>
                                                                        <?php echo($row[1]); ?>
                                                                    </titulo>
                                                                    </a>
                                                                </h5>
                                                                <span class="info-row">
                                                            <span class="item-location">
                                                            <?php echo($row[5]); ?>
                                                            </span><i class="fa fa-map-marker-alt"></i>
                                                        </span>
                                                                <div class="prop-info-box">

                                                                    <div class="prop-info">
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title ">
                                                                        <?php echo($row[3]); ?>
                                                                    </span>
                                                                            <span class="text">Marca</span>
                                                                        </div>

                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-area">
                                                                        <?php echo($row[4]); ?></span>
                                                                            <span class="text">Categoria</span>
                                                                        </div>
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-room">
                                                                        <?php echo($row[6]); ?>
                                                                    </span>
                                                                            <span class="text">Estado </span>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                        <!--/.add-desc-box-->
                                                        <div class="col-md-3 text-right  price-box">

                                                            <h3 class="item-price ">
                                                                <no> Bs. <?php echo($row[2]); ?></no>
                                                            </h3>

                                                            <div style="clear: both"></div>
                                                            <?php
                                                            echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                                    href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                            ?>
                                                            Ver Anuncio
                                                            </a>
                                                        </div>
                                                        <!--/.add-desc-box-->
                                                    </div>
                                                </div>
                                                <!--/.item-list-->
                                            <?php }; ?>
                                        <?php }; ?>


                                        <?php if (isset($_REQUEST['valor'])) { ?>
                                            <?php
                                            $valor = $_REQUEST['valor'];
                                            if ($valor == 1) {
                                                $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_producto.marca,
                                                categoria.descripcion,ubicacion.direccion,producto_inmueble.estado,galeria.imagen,galeria.url
                                            FROM anuncio
                                            Inner Join categoria ON anuncio.id_categoria=categoria.id
                                            Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                            Inner Join detalle_producto ON producto_inmueble.id=detalle_producto.id_prod_inmu
                                            Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                            Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                            WHERE producto_inmueble.precio>0 and galeria.url='foto1' order by precio asc;";
                                            } else {
                                                $query = "SELECT anuncio.id,anuncio.titulo,producto_inmueble.precio,detalle_producto.marca,
                                            categoria.descripcion,ubicacion.direccion,producto_inmueble.estado,galeria.imagen,galeria.url
                                            FROM anuncio
                                            Inner Join categoria ON anuncio.id_categoria=categoria.id
                                            Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
                                            Inner Join detalle_producto ON producto_inmueble.id=detalle_producto.id_prod_inmu
                                            Inner Join ubicacion ON producto_inmueble.id_ubicacion = ubicacion.id
                                            Inner Join galeria ON producto_inmueble.id = galeria.id_prod_inmu
                                            WHERE producto_inmueble.precio>0 and galeria.url='foto1' order by precio desc;";
                                            }
                                            ?>
                                            <?php

                                            $sql2 = $db->query($query);
                                            ?>
                                            <?php while ($row = mysqli_fetch_array($sql2)) { ?>
                                                <!-- fila anuncio inmueble -->
                                                <div class="item-list">

                                                    <div class="row">

                                                        <div class="col-md-3 no-padding photobox">
                                                            <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>

                                                                <img
                                                                    src="data:image/jpg;base64,<?php echo base64_encode($row[7]); ?>"
                                                                    alt="img"/>

                                                            </div>
                                                        </div>
                                                        <!--/.photobox-->
                                                        <div class="col-md-6 add-desc-box">
                                                            <div class="ads-details">
                                                                <h5 class="add-title">
                                                                    <?php
                                                                    echo "<a href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                                    ?>
                                                                    <titulo>
                                                                        <?php echo($row[1]); ?>
                                                                    </titulo>
                                                                    </a>
                                                                </h5>
                                                                <span class="info-row">
                                                            <span class="item-location">
                                                            <?php echo($row[5]); ?>
                                                            </span><i class="fa fa-map-marker-alt"></i>
                                                        </span>
                                                                <div class="prop-info-box">

                                                                    <div class="prop-info">
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title ">
                                                                        <?php echo($row[3]); ?>
                                                                    </span>
                                                                            <span class="text">Marca</span>
                                                                        </div>

                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-area">
                                                                        <?php echo($row[4]); ?></span>
                                                                            <span class="text">Categoria</span>
                                                                        </div>
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-room">
                                                                        <?php echo($row[6]); ?>
                                                                    </span>
                                                                            <span class="text">Estado </span>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                        <!--/.add-desc-box-->
                                                        <div class="col-md-3 text-right  price-box">

                                                            <h3 class="item-price ">
                                                                <no> Bs. <?php echo($row[2]); ?></no>
                                                            </h3>

                                                            <div style="clear: both"></div>
                                                            <?php
                                                            echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                                    href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                            ?>
                                                            Ver Anuncio
                                                            </a>
                                                        </div>
                                                        <!--/.add-desc-box-->
                                                    </div>
                                                </div>
                                                <!--/.item-list-->
                                            <?php }; ?>
                                        <?php }; ?>


                                        <?php if (!isset($_POST['enviar'])) { ?>
                                            <?php while ($row = mysqli_fetch_array($sql2)) { ?>
                                                <!-- fila anuncio inmueble -->
                                                <div class="item-list">

                                                    <div class="row">

                                                        <div class="col-md-3 no-padding photobox">
                                                            <div class="add-image">
                                                        <span class="photo-count">
                                                            <i class="fa fa-camera"></i> 3 
                                                        </span>

                                                                <img
                                                                    src="data:image/jpg;base64,<?php echo base64_encode($row[7]); ?>"
                                                                    alt="img"/>

                                                            </div>
                                                        </div>
                                                        <!--/.photobox-->
                                                        <div class="col-md-6 add-desc-box">
                                                            <div class="ads-details">
                                                                <h5 class="add-title">
                                                                    <?php
                                                                    echo "<a href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                                    ?>
                                                                    <titulo>
                                                                        <?php echo($row[1]); ?>
                                                                    </titulo>
                                                                    </a>
                                                                </h5>
                                                                <span class="info-row">
                                                            <span class="item-location">
                                                            <?php echo($row[5]); ?>
                                                            </span><i class="fa fa-map-marker-alt"></i>
                                                        </span>
                                                                <div class="prop-info-box">

                                                                    <div class="prop-info">
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title ">
                                                                        <?php echo($row[3]); ?>
                                                                    </span>
                                                                            <span class="text">Marca</span>
                                                                        </div>

                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-area">
                                                                        <?php echo($row[4]); ?></span>
                                                                            <span class="text">Categoria</span>
                                                                        </div>
                                                                        <div class="clearfix prop-info-block">
                                                                    <span class="title prop-room">
                                                                        <?php echo($row[6]); ?>
                                                                    </span>
                                                                            <span class="text">Estado </span>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                        <!--/.add-desc-box-->
                                                        <div class="col-md-3 text-right  price-box">

                                                            <h3 class="item-price ">
                                                                <no> Bs. <?php echo($row[2]); ?></no>
                                                            </h3>

                                                            <div style="clear: both"></div>
                                                            <?php
                                                            echo "<a class='btn btn-success btn-lg bold' style='background: #0039FF' 
                                                                    href='vAnuncio_Producto.php?id=" . $row[0] . "' >";
                                                            ?>
                                                            Ver Anuncio
                                                            </a>
                                                        </div>
                                                        <!--/.add-desc-box-->
                                                    </div>
                                                </div>
                                                <!--/.item-list-->
                                            <?php }; ?>
                                        <?php }; ?>


                                    </div>
                                    <!--/.adds-wrapper-->
                                </div>
                                <!--/.adds-wrapper-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.page-content-->

            </div>
        </div>
    </div>
    <!-- /.main-container -->


    <footer class="main-footer">

        <?php if ($_SESSION['color_tema'] == 0) {
            echo "<div class='footer-content' style='background-color: white'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 1) {
            echo "<div class='footer-content' style='background-color: yellow'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 2) {
            echo "<div class='footer-content' style='background-color: #E0F4FA'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 3) {
            echo "<div class='footer-content' style='background-color: #4CD7FF'>";
        } ?>

        <div class="container">
            <div class="row">
                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    <div class="footer-col">
                        <h4 class="footer-title">Ayuda y Contacto</h4>
                        <ul class="list-unstyled footer-nav">
                            <li><a href="#">
                                    Soporte Online
                                </a></li>
                            <li><a href="#">
                                    Como vender?</a></li>
                            <li><a href="#">
                                    Como comprar?
                                </a></li>
                            <li><a href="terminos_y_condiciones.html">Terminos y Condiciones
                                </a></li>
                        </ul>
                    </div>
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    <div class="footer-col">
                        <h4 class="footer-title">Mi Cuenta</h4>
                        <ul class="list-unstyled footer-nav">
                            <li><a href="login.html">Login
                                </a></li>
                            <li><a href="vNewCuentaUser.php">Registro
                                </a></li>
                        </ul>
                    </div>
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                </div>

                <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                    <div class="footer-col row">

                        <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                            <div class="mobile-app-content">
                                <h4 class="footer-title">App Movil</h4>
                                <div class="row ">
                                    <div class="col-6  ">
                                        <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                                            <span class="hide-visually">iOS app</span>
                                            <img src="images/site/app_store_badge.svg"
                                                 alt="Available on the App Store">
                                        </a>
                                    </div>
                                    <div class="col-6  ">
                                        <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                                            <span class="hide-visually">Android App</span>
                                            <img src="images/site/google-play-badge.svg"
                                                 alt="Available on the App Store">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                            <div class="hero-subscribe">
                                <h4 class="footer-title no-margin">Siguenos en:</h4>
                                <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                    <li><a class="icon-color fb" title="Facebook" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-facebook-f"></i> </a>
                                    </li>
                                    <li><a class="icon-color tw" title="Twitter" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-twitter"></i> </a></li>
                                    <li><a class="icon-color gp" title="Google+" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-google-plus-g"></i> </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>

                <div class="col-xl-12">
                    <div class=" text-center paymanet-method-logo">

                        <img src="images/site/payment/master_card.png" alt="img">
                        <img alt="img" src="images/site/payment/visa_card.png">
                        <img alt="img" src="images/site/payment/paypal.png">
                        <img alt="img" src="images/site/payment/american_express_card.png"> <img alt="img"
                                                                                                 src="images/site/payment/discover_network_card.png">
                        <img alt="img" src="images/site/payment/google_wallet.png">
                    </div>

                    <div class="copy-info text-center">
                        &copy; 2017 Pablo Bustos All Rights Reserved.
                    </div>

                </div>

            </div>
        </div>
</div>
</footer>
<!--/.footer-->

</div>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>

<!-- Modal Change City -->

<div class="modal fade modalHasList" id="select-country" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title uppercase font-weight-bold" id="exampleModalLabel"><i class=" icon-map"></i>
                    Select your region </h4>

                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
            </div>
        </div>
    </div>
</div>

<!-- /.modal -->
<!-- Modal Change City -->

<div class="modal fade modalHasList" id="selectRegion" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class=" icon-map"></i> Select your region </h4>

                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">

                        <p>Popular cities in <strong>New York</strong>
                        </p>
                        <hr class="hr-thin">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function enviar_valores(valor) {
        location.href = 'Anuncios.php?valor=' + valor;
    }
</script>


<script src="../alertifyJS/alertify.min.js"></script>
<script>
    function Solo_Numerico(variable) {
        numer = parseInt(variable);
        if (isNaN(numer)) {
            return "";
        }
        return numer;
    }

    function mensaje() {
        alertify.alert("Titulo", "Bienvenido Usuario ").set('label', 'Aceptar');
    }

</script>
</body>
</html>

<?php

$mensaje = $_GET['id'];
if ($mensaje == 3) {
    echo "<script>";
    echo "mensaje();";
    echo "</script>";
}
?>
