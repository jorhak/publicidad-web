<?php

require_once("../Controlador/cCliente.php");
require_once("../Css_colores/css_fuentes_temas.css");

if (!isset($_SESSION['ci_cliente'])) {
    header('location:login.html');
}

$id_pru_anuncio = $_GET['id'];
/*    $id_pru_anuncio=9;*/

require_once("../Controlador/conexion.php");
$db = Conectar::conexion();

// Mostrar Informacion del Usuario

$sql = $db->query("SELECT c.ci,c.nombre,c.apellido,c.email,c.telefono from anuncio as a, cliente as c 
                    where c.ci = a.ci_cliente and a.id='$id_pru_anuncio';");
$row1 = mysqli_fetch_array($sql);

// Informacion del Anuncio

$sql2 = $db->query("SELECT a.titulo,a.descripcion,pi.precio,pi.estado,dp.descripcion,dp.marca
                        from anuncio as a, producto_inmueble as pi, detalle_producto as dp
                        where a.id_prod_inmu=pi.id and pi.id=dp.id_prod_inmu and a.id= '$id_pru_anuncio';");
$row2 = mysqli_fetch_array($sql2);

// Fecha del anuncio

$sql_fecha = $db->query("SELECT fecha_ini from anuncio where id='$id_pru_anuncio';");
$row_fecha = mysqli_fetch_array($sql_fecha);

// fotos

$sql3 = $db->query("SELECT g.imagen from anuncio as a , producto_inmueble as pi, galeria as g
                        where a.id_prod_inmu=pi.id and g.id_prod_inmu=pi.id and a.id= '$id_pru_anuncio';");

$sql4 = $db->query("SELECT g.imagen from anuncio as a , producto_inmueble as pi, galeria as g
                        where a.id_prod_inmu=pi.id and g.id_prod_inmu=pi.id and a.id= '$id_pru_anuncio';");

// Ubicacion

$sql5 = $db->query("SELECT u.direccion,u.zona,u.dpto from ubicacion as u, producto_inmueble as pi, anuncio as a
                     where a.id_prod_inmu = pi.id and pi.id_ubicacion=u.id and a.id= '$id_pru_anuncio';");
$row5 = mysqli_fetch_array($sql5);

// Trueque

$sql6 = $db->query("SELECT t.permuta,t.diferencia from trueque as t, anuncio as a, producto_inmueble as pi
                        where a.id_prod_inmu=pi.id and pi.id=t.id_prod_inm and a.id = '$id_pru_anuncio';");
$row6 = mysqli_fetch_array($sql6);

?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/property-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:26:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Informacion del Anuncio</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">
    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>

<?php

if ($_SESSION['color_tema'] == 0) {
    echo "<body style='background-color:#D5F5E3'>";
}
if ($_SESSION['color_tema'] == 1) {
    echo("<body style='background-color:#D5D5D5'>");
}
if ($_SESSION['color_tema'] == 2) {
    echo("<body style='background-color:#7493FF'>");
}
if ($_SESSION['color_tema'] == 3) {
    echo("<body style='background-color:#FAFD95'>");
}

?>

<div id="wrapper">

    <div class="header">

        <?php if ($_SESSION['color_tema'] == 0) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: white'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 1) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: yellow'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 2) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #E0F4FA'>";
        }
        ?>

        <?php if ($_SESSION['color_tema'] == 3) {
            echo "<nav class='navbar  fixed-top navbar-site navbar-light  navbar-expand-md'
             role='navigation' style='background: #4CD7FF'>";
        }
        ?>

        <div class="container">

            <div class="navbar-identity">
                <a href="" class="navbar-brand logo logo-title">
                <span class="logo-icon">
                    <!-- <i class="icon icon-search-1 ln-shadow-logo "></i> -->
                    <img src="img/Logo7.png" width="50" height="50">
                </span>Publicidad inmobiliaria </a>

            </div>


            <!-- Opciones desplegables - barra Menu -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav ml-auto navbar-right">

                    <li class="dropdown no-arrow nav-item"><a href="##" class="dropdown-toggle nav-link"
                                                              data-toggle="dropdown">

                            <no> <?php echo($datos[1] . " " . $datos[2]); ?>

                            </no>
                            <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                        <ul style="border-color: black;"
                            class="dropdown-menu user-menu dropdown-menu-right">
                            <li class="disable dropdown-item">
                                <no> Modulo Usuario</no>
                            </li>
                            <li class=" dropdown-item">
                                <a href="vCliente.php">
                                    <i class="icon-home"></i>Datos Personales
                                </a>
                            </li>

                            <li class="dropdown-item">
                                <a href="vSoporte.php">
                                    <i class="icon-folder-close"></i> Soporte Tecnico
                                </a>
                            </li>

                            <li class="dropdown-item"><a href="vCambiarPass.php"><i class="icon-down-open-big"></i>
                                    Cambiar Contraseña </a>
                            </li>

                            <li class="disable dropdown-item">
                                <no> Modulo Publicidad</no>
                            </li>

                            <li class="dropdown-item"><a href="#"><i class="icon-heart"></i> Administrar Anuncios </a>
                            </li>

                            <li class="dropdown-item"><a href="vMensajeria.php"><i class="icon-mail"></i> Bandeja de
                                    Entrada </a>
                            </li>

                            <li class="disable dropdown-item">
                                <no> Modulo Pagos</no>
                            </li>


                            <li class="dropdown-item"><a href="#"><i class=" icon-money "></i> Transacciones </a>
                            </li>

                            <li class="dropdown-item"><a href="vCuentasbancarias.php"><i class="icon-docs"></i>Cuentas
                                    Bancarias</a>
                            </li>

                        </ul>
                    </li>
                    <!-- boton rojo cerrar sesion -->
                    <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                                                    href="../Controlador/cerrar_sesion.php">Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- /.header -->

    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <nav aria-label="breadcrumb" role="navigation" class="pull-left">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="Anuncios.php"><i class="icon-home fa"></i></a></li>
                            <li class="breadcrumb-item">Productos</a></li>
                        </ol>
                    </nav>


                    <div class="pull-right backtolist"><a href="Anuncios.php"> <i
                                class="fa fa-angle-double-left"></i>
                            <titulo2>Seguir Viendo mas Anuncios</titulo2>
                        </a></div>

                </div>
            </div>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <h2 class="auto-heading"><span
                                class="auto-title left"><?php echo($row2[0]); ?></span>
                            <span
                                class="auto-price pull-right"><strong> <?php echo($row2[3]); ?></strong>
                            </span>
                        </h2>

                        <div style="clear:both;"></div>
                        <span class="info-row"> 
                            <span class="date">
                                <i class=" icon-clock"> 
                                    </i> Publicado el <?php echo $row_fecha[0]; ?> 
                            </span>
                        </span>


                        <div class="row ">
                            <div class="col-sm-12 automobile-left-col">


                                <div class="ads-image">
                                    <h1 class="pricetag"> Bs. <?php echo($row2[2]); ?></h1>
                                    <ul class="bxslider">


                                        <?php while ($row3 = mysqli_fetch_array($sql3)) { ?>
                                            <li>
                                                <img height="400px"
                                                     src="data:image/jpg;base64,<?php echo base64_encode($row3[0]); ?>"
                                                     alt="img"/>
                                            </li>
                                        <?php }; ?>
                                    </ul>

                                    <div class="product-view-thumb-wrapper" align="center">


                                        <ul id="bx-pager" class="product-view-thumb">

                                            <?php
                                            $c = 0;
                                            while ($row4 = mysqli_fetch_array($sql4)) { ?>
                                                <li>

                                                    <a data-slide-index="<?php echo $c; ?>" class="thumb-item-link"
                                                       href="#">
                                                        <img
                                                            src="data:image/jpg;base64,<?php echo base64_encode($row4[0]); ?>"
                                                            alt="img"/>
                                                    </a>
                                                </li>
                                                <?php $c = $c + 1 ?>
                                            <?php }; ?>
                                        </ul>


                                    </div>

                                </div>
                                <!--ads-image-->
                            </div>
                            <!-- /.automobile-left-col-->


                        </div>
                        <!--/.row-->


                        <div class="Ads-Details">
                            <h5 class="list-title"><strong>Descripcion</strong></h5>

                            <div class="row">
                                <div class="ads-details-info col-md-8">
                                    <p>
                                        <?php echo($row2[1]); ?>
                                    </p>
                                    <h4 class="text-uppercase"><strong>Informacion del Producto</strong></h4>


                                    <ul class="list-circle">

                                        <li><strong>Estado del Producto: </strong><?php echo($row2[3]); ?></li>
                                        <li><strong>Marca: </strong><?php echo($row2[5]); ?></li>

                                    </ul>

                                    <h4 class="text-uppercase"><strong>Informacion Detallada:</strong></h4>
                                    <p> <?php echo($row2[4]); ?> </p>

                                    <h4 class="text-uppercase"><strong>Direccion</strong></h4>
                                    <address>
                                        <p>
                                            <?php echo($row5[0]); ?>
                                        </p>
                                        <br>
                                        <strong>Tel/Cel de Referencia: </strong><?php echo($row1[4]); ?><br>
                                    </address>


                                </div>
                                <div class="col-md-4">
                                    <aside class="panel panel-body panel-details">
                                        <ul>
                                            <li>
                                                <p class=" no-margin ">
                                                    <strong>Precio:</strong> Bs. <?php echo($row2[2]); ?>
                                                </p>
                                            </li>

                                            <li>
                                                <p class="no-margin">
                                                    <strong>Zona:</strong><?php echo $row5[1]; ?>
                                                </p>
                                            </li>
                                            <li>
                                                <p class=" no-margin ">
                                                    <strong>Departamento:</strong><?php echo $row5[2]; ?>
                                                </p>
                                            </li>
                                        </ul>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.ads-details-wrapper-->

                </div>
                <!--/.page-content-->

                <div class="col-md-3  page-sidebar-right">
                    <aside>

                        <div class="card sidebar-card card-contact-seller" style="width: 120%">
                            <div class="card-header" style="background-color:#CD5353">>
                                Detalle Oferta de Trueque
                            </div>
                            <div class="card-content user-info">
                                <div class="card-body text-center" style="background-color:#FCFB99">
                                    <div class="seller-info">
                                        <p align="left">
                                            Permuta que se pide a Cambio : <br><strong><?php echo($row6[0]); ?></strong>
                                        </p>

                                        <p align="left">
                                            Diferencia de dinero que se Pide: <br><strong>
                                                Bs.<?php echo($row6[1]); ?></strong>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card sidebar-card card-contact-seller" style="width: 120%">
                            <div class="card-header">Informacion Vendedor</div>
                            <div class="card-content user-info">
                                <div class="card-body text-center">
                                    <div class="seller-info">
                                        <div class="company-logo-thumb">
                                            <a><img src="images/usuario.jpg" class=" " alt="img"> </a>
                                        </div>

                                        <p align="left">C.I. : <strong><?php echo($row1[0]); ?></strong></p>

                                        <p align="left">Nombres: <strong><?php echo($row1[1]); ?></strong></p>

                                        <p align="left">Apellidos: <strong><?php echo($row1[2]); ?></strong></p>

                                        <p align="left"> Telefono: <strong><?php echo($row1[3]); ?></strong></p>

                                        <p align="left"> email: <strong><?php echo($row1[4]); ?></strong></p>
                                    </div>
                                    <div class="user-ads-action">

                                        <a href="#contactAdvertiser" data-toggle="modal"
                                           class="btn   btn-info btn-block"><i class=" icon-mail-2"></i> Enviar Mensaje
                                        </a></div>
                                </div>
                            </div>
                        </div>

                        <div class="card sidebar-card">
                            <div class="card-header">Tips de Seguridad para Compradores</div>
                            <div class="card-content">
                                <div class="card-body text-left">
                                    <ul class="list-check">
                                        <li> Realiza la Compra en lugar Publico.</li>
                                        <li> Revisa antes de realizar la Compra.</li>
                                        <li> Realiza el Pago despues de la Firma del contrato.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--/.categories-list-->
                    </aside>
                </div>
                <!--/.page-side-bar-->
            </div>
        </div>
    </div>
    <!-- /.main-container -->


    <footer class="main-footer">
        <?php if ($_SESSION['color_tema'] == 0) {
            echo "<div class='footer-content' style='background-color: white'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 1) {
            echo "<div class='footer-content' style='background-color: yellow'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 2) {
            echo "<div class='footer-content' style='background-color: #E0F4FA'>";
        } ?>

        <?php if ($_SESSION['color_tema'] == 3) {
            echo "<div class='footer-content' style='background-color: #4CD7FF'>";
        } ?>
        <div class="container">
            <div class="row">
                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    <div class="footer-col">
                        <h4 class="footer-title">Ayuda y Contacto</h4>
                        <ul class="list-unstyled footer-nav">
                            <li><a href="#">
                                    Soporte Online
                                </a></li>
                            <li><a href="#">
                                    Como vender?</a></li>
                            <li><a href="#">
                                    Como comprar?
                                </a></li>
                            <li><a href="terminos_y_condiciones.html">Terminos y Condiciones
                                </a></li>
                        </ul>
                    </div>
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                    <div class="footer-col">
                        <h4 class="footer-title">Mi Cuenta</h4>
                        <ul class="list-unstyled footer-nav">
                            <li><a href="login.html">Login
                                </a></li>
                            <li><a href="vNewCuentaUser.php">Registro
                                </a></li>
                        </ul>
                    </div>
                </div>

                <div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
                </div>

                <div class=" col-xl-4 col-xl-4 col-md-4 col-12">
                    <div class="footer-col row">

                        <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                            <div class="mobile-app-content">
                                <h4 class="footer-title">App Movil</h4>
                                <div class="row ">
                                    <div class="col-6  ">
                                        <a class="app-icon" target="_blank" href="https://itunes.apple.com/">
                                            <span class="hide-visually">iOS app</span>
                                            <img src="images/site/app_store_badge.svg" alt="Available on the App Store">
                                        </a>
                                    </div>
                                    <div class="col-6  ">
                                        <a class="app-icon" target="_blank" href="https://play.google.com/store/">
                                            <span class="hide-visually">Android App</span>
                                            <img src="images/site/google-play-badge.svg"
                                                 alt="Available on the App Store">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
                            <div class="hero-subscribe">
                                <h4 class="footer-title no-margin">Siguenos en:</h4>
                                <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
                                    <li><a class="icon-color fb" title="Facebook" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-facebook-f"></i> </a></li>
                                    <li><a class="icon-color tw" title="Twitter" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-twitter"></i> </a></li>
                                    <li><a class="icon-color gp" title="Google+" data-placement="top"
                                           data-toggle="tooltip" href="#"><i class="fab fa-google-plus-g"></i> </a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>

                <div class="col-xl-12">
                    <div class=" text-center paymanet-method-logo">

                        <img src="images/site/payment/master_card.png" alt="img">
                        <img alt="img" src="images/site/payment/visa_card.png">
                        <img alt="img" src="images/site/payment/paypal.png">
                        <img alt="img" src="images/site/payment/american_express_card.png"> <img alt="img"
                                                                                                 src="images/site/payment/discover_network_card.png">
                        <img alt="img" src="images/site/payment/google_wallet.png">
                    </div>

                    <div class="copy-info text-center">
                        &copy; 2017 Pablo Bustos All Rights Reserved.
                    </div>

                </div>

            </div>
        </div>
</div>
</footer>
<!-- /.footer -->
</div>
<!-- /.wrapper -->

<!-- Modal Change City -->

<!-- Modal contactAdvertiser -->

<div class="modal fade" id="contactAdvertiser" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class=" icon-mail-2"></i> Contact advertiser </h4>

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name:</label>
                        <input class="form-control required" id="recipient-name" placeholder="Your name"
                               data-placement="top" data-trigger="manual"
                               data-content="Must be at least 3 characters long, and must only contain letters."
                               type="text">
                    </div>
                    <div class="form-group">
                        <label for="sender-email" class="control-label">E-mail:</label>
                        <input id="sender-email" type="text"
                               data-content="Must be a valid e-mail address (user@gmail.com)" data-trigger="manual"
                               data-placement="top" placeholder="email@you.com" class="form-control email">
                    </div>
                    <div class="form-group">
                        <label for="recipient-Phone-Number" class="control-label">Phone Number:</label>
                        <input type="text" maxlength="60" class="form-control" id="recipient-Phone-Number">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Message <span class="text-count">(300) </span>:</label>
                        <textarea class="form-control" id="message-text" placeholder="Your message here.."
                                  data-placement="top" data-trigger="manual"></textarea>
                    </div>
                    <div class="form-group">
                        <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not
                            valid. </p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success pull-right">Send message!</button>
            </div>
        </div>
    </div>
</div>

<!-- /.modal -->

<!-- Modal contactAdvertiser -->

<div class="modal fade" id="reportAdvertiser" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa icon-info-circled-alt"></i> There's something wrong with this ads?
                </h4>

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="report-reason" class="control-label">Reason:</label>
                        <select name="report-reason" id="report-reason" class="form-control">
                            <option value="">Select a reason</option>
                            <option value="soldUnavailable">Item unavailable</option>
                            <option value="fraud">Fraud</option>
                            <option value="duplicate">Duplicate</option>
                            <option value="spam">Spam</option>
                            <option value="wrongCategory">Wrong category</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-email" class="control-label">Your E-mail:</label>
                        <input type="text" name="email" maxlength="60" class="form-control" id="recipient-email">
                    </div>
                    <div class="form-group">
                        <label for="message-text2" class="control-label">Message <span class="text-count">(300) </span>:</label>
                        <textarea class="form-control" id="message-text2"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Send Report</button>
            </div>
        </div>
    </div>
</div>

<!-- /.modal -->
<!-- Modal Change City -->

<div class="modal fade modalHasList" id="select-country" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title uppercase font-weight-bold" id="exampleModalLabel"><i class=" icon-map"></i>
                    Select your region </h4>

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
            </div>

        </div>
    </div>
</div>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>
<script>
    $(document).ready(function () {
        // Slider
        var $mainImgSlider = $('.bxslider').bxSlider({
            speed: 1000,
            pagerCustom: '#bx-pager',
            controls: false,
            adaptiveHeight: true
        });

        // initiates responsive slide
        var settings = function () {
            var mobileSettings = {
                slideWidth: 80,
                minSlides: 2,
                maxSlides: 5,
                slideMargin: 5,
                adaptiveHeight: true,
                pager: false,

            };
            var pcSettings = {
                slideWidth: 100,
                minSlides: 3,
                maxSlides: 5,
                pager: false,
                slideMargin: 10,
                adaptiveHeight: true
            };
            return ($(window).width() < 768) ? mobileSettings : pcSettings;
        }

        var thumbSlider;

        function tourLandingScript() {
            thumbSlider.reloadSlider(settings());
        }

        thumbSlider = $('.product-view-thumb').bxSlider(settings());
        $(window).resize(tourLandingScript);

    });
</script>
</body>

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/property-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:27:42 GMT -->
</html>
