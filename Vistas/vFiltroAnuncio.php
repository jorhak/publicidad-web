<?php
//require_once ("../Controlador/cDetalleAnuncio.php");
require_once("../Controlador/conexion.php");
$estado = 'no';
$fecha = 'si';
$tipo = 'si';

$db = Conectar::conexion();

$sql = $db->query("SELECT a.id,a.estado,da.tipo,a.fecha_ini from anuncio as a, detalle_anuncio as da 
                      where a.id_deta_auncio=da.id;");

?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
    <title>Inicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--       <link href="../starter-template.css" rel="stylesheet">

        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>

    <!-- Para el Modal -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body style="background-color:#D5F5E3">

<div align="center">
    <br>
    <div class="inner-box">
        <h1 class="title-1">
            <i class="icon-docs"></i>
            <strong>Mis Anuncios</strong>
        </h1>
        <br>
        <div class="table-responsive">
            <table id="addManageTable" style="width: 50%"
                   class="table table-striped table-bordered add-manage-table table demo"
                   data-filter="#filter" data-filter-text-only="true" align="center">
                <thead>
                <tr>

                    <th style="width:5%">Id</th>
                    <?php if ($estado == 'si') { ?>
                        <th style="width:5%">Estado</th>
                    <?php } ?>

                    <?php if ($tipo == 'si') { ?>
                        <th style="width:5%">Tipo Anuncio</th>
                    <?php } ?>

                    <?php if ($fecha == 'si') { ?>
                        <th style="width:5%">Fecha del Anuncio</th>
                    <?php } ?>

                </tr>
                </thead>
                <tbody>
                <tr>

                    <?php while ($row = mysqli_fetch_array($sql)){ ?>
                <tr>

                    <td> <?php echo $row[0]; ?> </td>

                    <?php if ($estado == 'si') { ?>
                        <td> <?php echo $row[1]; ?> </td>
                    <?php } ?>

                    <?php if ($tipo == 'si') { ?>
                        <td> <?php echo $row[2]; ?> </td>
                    <?php } ?>

                    <?php if ($fecha == 'si') { ?>
                        <td> <?php echo $row[2]; ?> </td>
                    <?php } ?>

                </tr>
                <?php } ?>
                </tr>

                </tbody>
            </table>
        </div>
        <!--/.row-box End-->

    </div>
</div>

<!-- MODAL -->
<div class="container">

    <br>

    <br>
    <div class="container">
        <a class="btn btn-danger btn-primary btn-lg" href="../index.php">
            Salir
        </a>
    </div>
</div>
</body>
</html>
