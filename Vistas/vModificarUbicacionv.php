<?php

require_once("../Controlador/cCliente.php");
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>Perfil de Usuario</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>
<body>
<div id="wrapper">

    <div class="header">
        <nav class="navbar  fixed-top navbar-site navbar-light bg-light navbar-expand-md"
             role="navigation">
            <div class="container">

                <div class="navbar-identity">
                    <a href="" class="navbar-brand logo logo-title">
<span class="logo-icon">
                    <!-- <i class="icon icon-search-1 ln-shadow-logo "></i> -->
                    <img src="img/Logo.png" width="70" height="70">
</span>Perfil <span>Usuario </span> </a>

                </div>
                <!-- Opciones desplegables - barra Menu -->
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav ml-auto navbar-right">

                        <li class="dropdown no-arrow nav-item"><a href="##" class="dropdown-toggle nav-link"
                                                                  data-toggle="dropdown">
                                <span> <?php echo($datos[1] . " " . $datos[2]); ?>

                            </span>
                                <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
                            <ul
                                class="dropdown-menu user-menu dropdown-menu-right">
                                <li class="active dropdown-item">
                                    <a>
                                        <i class="icon-home"></i>Datos Personales
                                    </a>
                                </li>

                                <li class="dropdown-item"><a href="vCuentasbancarias.php"><i class="icon-docs"></i>Cuentas
                                        Bancarias</a>
                                </li>

                                <li class="dropdown-item"><a href="account-favourite-ads.html"><i
                                            class="icon-heart"></i> Administrar Anuncios </a>
                                </li>

                                <li class="dropdown-item">
                                    <a href="vSoporte.php">
                                        <i class="icon-folder-close"></i> Soporte
                                    </a>
                                </li>
                                <li class="dropdown-item"><a href="#"><i class=" icon-money "></i> Transacciones </a>
                                </li>
                                <li class="dropdown-item"><a href="vMensajeria.php"><i class="icon-mail"></i> Mensajeria
                                    </a>
                                </li>
                                <li class="dropdown-item"><a href="#"><i class="icon-down-open-big"></i> Cambiar
                                        Contraseña </a>
                                </li>
                                <li class="dropdown-item"><a href="../Controlador/cerrar_sesion.php"><i
                                            class=" icon-logout "></i> Cerrar Sesion </a>
                                </li>
                            </ul>
                        </li>
                        <!-- boton rojo cerrar sesion -->
                        <li class="postadd nav-item"><a class="btn btn-block   btn-border btn-post btn-danger nav-link"
                                                        href="../Controlador/cerrar_sesion.php">Cerrar Sesión</a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- /.header -->
    <div class="main-container">
        <div class="container">
            <div class="row">

                <!--  Menu Lateral   -->
                <div class="col-md-3 page-sidebar">
                    <aside>
                        <div class="inner-box">
                            <div class="user-panel-sidebar">
                                <div class="collapse-box">
                                    <h5 class="collapse-title no-border"> Mi Perfil <a href="#MyClassified"
                                                                                       aria-expanded="true"
                                                                                       data-toggle="collapse"
                                                                                       class="pull-right"><i
                                                class="fa fa-angle-down"></i></a>
                                    </h5>

                                    <div class="panel-collapse collapse show" id="MyClassified">
                                        <ul class="acc-list">
                                            <li><a class="active"><i class="icon-home"></i>
                                                    Datos Personales </a></li>
                                        </ul>
                                    </div>

                                </div>
                                <!-- /.collapse-box  -->
                                <div class="collapse-box">
                                    <h5 class="collapse-title"> Mis Actividades <a href="#MyAds" aria-expanded="true"
                                                                                   data-toggle="collapse"
                                                                                   class="pull-right">
                                            <i class="fa fa-angle-down"></i></a></h5>

                                    <div class="panel-collapse collapse show" id="MyAds">
                                        <ul class="acc-list">
                                            <li><a href="vCuentasbancarias.php">
                                                    <i class="icon-docs"></i>Cuentas Bancarias
                                                </a>
                                            </li>

                                            <li><a href="VistaDeUbicaciones.php">
                                                    <i class="icon-heart"></i>Administrar Anuncios
                                                </a>
                                            </li>

                                            <li><a href="vSoporte.php">
                                                    <i class="icon-folder-close"></i>Soporte Tecnico
                                                </a>
                                            </li>

                                            <li><a href="#">
                                                    <i class="icon-money"></i> Transacciones
                                                </a>
                                            </li>

                                            <li><a href="vMensajeria.php">
                                                    <i class="icon-mail"></i> Mensajeria
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.collapse-box  -->

                                <div class="collapse-box">
                                    <h5 class="collapse-title"> Administrar Sesion <a href="#TerminateAccount"
                                                                                      aria-expanded="true"
                                                                                      data-toggle="collapse"
                                                                                      class="fa fa-angle-down"></i></a>
                                    </h5>
                                    <div class="panel-collapse collapse show" id="TerminateAccount">
                                        <ul class="acc-list">
                                            <li><a href="#">
                                                    <i class="icon-down-open-big"></i>Cambiar Contraseña </a></li>
                                        </ul>
                                    </div>
                                    <div class="panel-collapse collapse show" id="TerminateAccount">
                                        <ul class="acc-list">
                                            <li><a href="../Controlador/cerrar_sesion.php">
                                                    <i class="icon-logout"></i> Cerrar Sesion </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.collapse-box  -->
                            </div>
                        </div>
                        <!-- /.inner-box  -->


                    </aside>
                </div>
                <!--/.page-sidebar-->
                <!-- Este es eL Cuerpo de la vista y esto muestra el campo de roles y privilegios -->
                <div class="col-md-9 page-content">
                    <div class="inner-box">
                        <div class="row">
                            <div class="col-md-5 col-xs-4 col-xxs-12">
                                <h3 class="no-padding text-center-480 useradmin">
                                    <a>
                                        <img class="userImg"
                                             src="img/perfil.png" alt="user">
                                        <?php echo($datos[1] . " " . $datos[2]); ?>
                                    </a></h3>
                            </div>
                            <div class="col-md-7 col-xs-8 col-xxs-12">
                                <div class="header-data text-center-xs">
                                    <div class="hdata">
                                        <div class="mcol-left">
                                            <!-- Icon with red background -->
                                            <i class="fas fa-envelope ln-shadow"></i></div>
                                        <div class="mcol-right">
                                            <!-- Number of visitors -->
                                            <p><a href="#">ROL</a> <em>Adm</em></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <!-- Traffic data -->
                                    <div class="hdata">
                                        <div class="mcol-left">
                                            <!-- Icon with red background -->
                                            <i class="fa fa-eye ln-shadow"></i></div>
                                        <div class="mcol-right">
                                            <!-- Number of visitors -->
                                            <p><a href="#">Lectura</a> <em>si</em></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <!-- revenue data -->
                                    <div class="hdata">
                                        <div class="mcol-left">
                                            <!-- Icon with green background -->
                                            <i class="icon-th-thumb ln-shadow"></i></div>
                                        <div class="mcol-right">
                                            <!-- Number of visitors -->
                                            <p><a href="#">Ejecucion </a><em>si</em></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <!-- revenue data -->
                                    <div class="hdata">
                                        <div class="mcol-left">
                                            <!-- Icon with blue background -->
                                            <i class="fa fa-user ln-shadow"></i></div>
                                        <div class="mcol-right">
                                            <!-- Number of visitors -->
                                            <p><a href="#">Escritura</a> <em>si </em></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (!isset($_POST['btn-editar'])){ ?>
                    <div class="inner-box">
                        <div class="welcome-msg">
                            <h3 class="page-sub-header2 clearfix no-padding">Ubicacion del producto</h3>
                        </div>
                        <div id="accordion" class="panel-group">
                            <div class="card card-default">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <a href="#collapseB1" aria-expanded="true" data-toggle="collapse">
                                            Modificar ubicacion
                                        </a>

                                    </h4>
                                    <br>

                                    <form method="post" action="../Controlador/cModificarUbicacion.php">
                                        <label> Porfavor especificar la nueva direccion</label>
                                        <br>
                                        <textarea name="direccion" cols="30" rows="7"></textarea>
                                        <br>
                                        <label>Nueva zona</label>
                                        <br>
                                        <input type="text" name="zona" size="29">
                                        <br>
                                        <label>Selecciona el nuevo departamento</label>
                                        <br>
                                        <select name="departamentos">
                                            <option value="">VACIO</option>
                                            <option value="Santa Cruz">Santa Cruz</option>
                                            <option value="La Paz">La Paz</option>
                                            <option value="Cochabamba">Cochabamba</option>
                                            <option value="Chuquisaca">Chuquisaca</option>
                                            <option value="Tarija">Tarija</option>
                                            <option value="Beni">Beni</option>
                                            <option value="Pando">Pando</option>
                                            <option value="Oruro">Oruro</option>
                                            <option value="Potosi">Potosi</option>

                                        </select>
                                        <BR>

                                        <input type="submit" value="ENVIAR">
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                    <!--/.row-box End-->
                </div>
                <?php } else { ?>
                    <div class="inner-box">
                        <div class="welcome-msg">
                            <h3 class="page-sub-header2 clearfix no-padding">Editar Datos Personales: </h3>
                            <span class="page-sub-header-sub small">Estos son sus datos registrados</span>
                        </div>
                        <div id="accordion" class="panel-group">
                            <div class="card card-default">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <a href="#collapseB1" aria-expanded="true" data-toggle="collapse">
                                            Ubicacion del producto
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse show" id="collapseB1">
                                    <div class="card-body">
                                        <form action="../Controlador/cCliente.php" method="POST">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Email Actual</label>
                                                <div class="col-sm-9">
                                                    <input type="email" readonly name="email_old"
                                                           class="form-control"
                                                           value="<?php echo($datos[3]); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Email Nuevo</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="edit_email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Phone" class="col-sm-3 control-label">Telefono
                                                    Actual</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" readonly name="cel_old"
                                                           style="width: 300px" value="<?php echo($datos[4]); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Phone" class="col-sm-3 control-label">
                                                    Nuevo Telefono
                                                </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="edit_cel"
                                                           style="width: 300px">
                                                </div>
                                            </div>
                                            <div class="form-group" align="center">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="submit" class="btn btn-primary"
                                                            name="btn-Guardar">Guardar Cambios
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/.row-box End-->
                    </div>

                <?php }; ?>

            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->


</div>

<script src="assets/js/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/js/vendors.min.js"></script>
<script src="assets/js/main.min.js"></script>
</body>

<!-- Mirrored from templatecycle.com/demo/bootclassified-4.4/dist/account-home.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Sep 2018 22:38:54 GMT -->
</html>
