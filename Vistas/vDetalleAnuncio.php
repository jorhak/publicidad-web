<?php

require_once("../Controlador/cDetalleAnuncio.php");
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
    <title>Inicio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--       <link href="../starter-template.css" rel="stylesheet">

        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>

    <!-- Para el Modal -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body style="background-color:#D5F5E3">

<div align="center">
    <br>
    <div class="inner-box">
        <h1 class="title-1">
            <i class="icon-docs"></i>
            <strong>Mis Planes de Anuncios</strong>
        </h1>
        <br>
        <div class="table-responsive">
            <table id="addManageTable" style="width: 50%"
                   class="table table-striped table-bordered add-manage-table table demo"
                   data-filter="#filter" data-filter-text-only="true" align="center">
                <thead>
                <tr>
                    <th style="width:5%">Id</th>
                    <th data-sort-ignore="true">Tipo de Plan de Anuncio</th>
                    <th>Duracion</th>
                    <th data-type="numeric"> Precio</th>
                </tr>
                </thead>
                <tbody>
                <tr>

                    <?php while ($row = mysqli_fetch_array($datos)){ ?>
                <tr>
                    <td> <?php echo $row[0]; ?> </td>
                    <td> <?php echo $row[1]; ?> </td>
                    <td> <?php echo $row[2]; ?> </td>
                    <td> <?php echo $row[3]; ?> </td>

                </tr>
                <?php } ?>
                </tr>

                </tbody>
            </table>
        </div>
        <!--/.row-box End-->

    </div>
</div>

<!-- MODAL -->
<div class="container">
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
            data-target="#contactAdvertiser">Agregar Nuevo Plan
    </button>
    <br>

    <br>
    <div class="container">
        <a class="btn btn-danger btn-primary btn-lg" href="../index.php">
            Salir
        </a>
    </div>
</div>

<!-- Modal contactAdvertiser -->

<div class="modal fade" id="contactAdvertiser" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class=" icon-mail-2"></i>Agregar Nuevo Plan</h4>

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Cerrar</span></button>
            </div>
            <div class="modal-body">
                <form role="form" action="../Controlador/cDetalleAnuncio.php" method="POST">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nombre del Plan:</label>
                        <input class="form-control required" placeholder="Nombre del Nuevo Plan"
                               data-placement="top" data-trigger="manual" required="" name="nplan"
                               data-content="Must be at least 3 characters long, and must only contain letters."
                               type="text">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Duracion del Plan:</label>
                        <input class="form-control required" placeholder="Tiempo del Plan"
                               data-placement="top" data-trigger="manual" required="" name="dplan"
                               data-content="Must be at least 3 characters long, and must only contain letters."
                               type="text">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Precio del Plan:</label>
                        <input class="form-control required" placeholder="Precio del Nuevo Plan"
                               data-placement="top" data-trigger="manual" required="" name="pplan"
                               data-content="Must be at least 3 characters long, and must only contain letters."
                               type="number">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" name="submit_newPlan" class="btn btn-success pull-right">Registrar Nuevo Plan
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
