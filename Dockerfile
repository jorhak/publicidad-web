# Utilizamos la imagen base de Ubuntu
FROM ubuntu:20.04

# Actualizamos el sistema e instalamos Apache, PHP 7.4 y otras dependencias
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    apache2 \
    php7.4 \
    libapache2-mod-php7.4 \
    php7.4-mysql \
    php7.4-cli \
    php7.4-json \
    php7.4-curl \
    php7.4-gd \
    php7.4-mbstring \
    php7.4-intl \
    php7.4-xml \
    php7.4-zip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Configuramos Apache para que se ejecute en primer plano
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN a2enmod rewrite

# Creamos un directorio para el código de la aplicación
RUN mkdir -p /var/www/html

# Copiamos tu código PHP en el directorio de la aplicación
COPY . /var/www/html

RUN rm /var/www/html/index.html
# Exponemos el puerto 80
EXPOSE 80

# Iniciamos Apache en primer plano
CMD ["apachectl", "-D", "FOREGROUND"]
