<?php
require_once("../Modelo/MProducto.php");
require_once("../Modelo/MBitacora.php");

if (isset($_POST['submit_new_producto'])) {

    session_start();
    $cli = $_SESSION['ci_cliente'];

    $prod = new MProducto();

    $v1 = $_POST['precio'];
    $v2 = $_POST['estado'];
    $v3 = $_POST['descrip'];
    $v4 = $_POST['marca'];

    $res = $prod->addProducto("$v1", "$v2", "$v3", "$v4");

    if ($res == 1) {
        $req = MBitacora::insertBitacora('Se inserto informacion del Producto', "$cli");
        header('location:../Vistas/vGaleria2.php');
    } else {
        header('location:../Vistas/vProducto.php');
    }

}
