<?php
session_start();

// Temas de Colores
if (isset($_GET['id_tema'])) {

    $tema = $_GET['id_tema'];

    if ($tema == 0) {
        $_SESSION['color_tema'] = 0;
        header('location:../Vistas/Anuncios.php');
    }

    if ($tema == 1) {
        $_SESSION['color_tema'] = 1;
        header('location:../Vistas/Anuncios.php');
    }

    if ($tema == 2) {
        $_SESSION['color_tema'] = 2;
        header('location:../Vistas/Anuncios.php');
    }

    if ($tema == 3) {
        $_SESSION['color_tema'] = 3;
        header('location:../Vistas/Anuncios.php');
    }
}


// Color de Letra

if (isset($_GET['id_color_letra'])) {

    $color_letra = $_GET['id_color_letra'];

    if ($color_letra == 0) {
        $_SESSION['color_letra'] = 0;
        header('location:../Vistas/Anuncios.php');
    }

    if ($color_letra == 1) {
        $_SESSION['color_letra'] = 1;
        header('location:../Vistas/Anuncios.php');
    }

    if ($color_letra == 2) {
        $_SESSION['color_letra'] = 2;
        header('location:../Vistas/Anuncios.php');
    }
}

// Tamaño de Letra

if (isset($_GET['id_size_letra'])) {

    $size = $_GET['id_size_letra'];

    if ($size == 0) {
        $_SESSION['size_letra'] = 0;
        header('location:../Vistas/Anuncios.php');
    }

    if ($size == 1) {
        $_SESSION['size_letra'] = 1;
        header('location:../Vistas/Anuncios.php');
    }

    if ($size == 2) {
        $_SESSION['size_letra'] = 2;
        header('location:../Vistas/Anuncios.php');
    }
}

// Tipo de Fuente Letra

if (isset($_GET['id_fuente_letra'])) {

    $fuente = $_GET['id_fuente_letra'];

    if ($fuente == 0) {
        $_SESSION['tipo_letra'] = 0;
        header('location:../Vistas/Anuncios.php');
    }

    if ($fuente == 1) {
        $_SESSION['tipo_letra'] = 1;
        header('location:../Vistas/Anuncios.php');
    }

    if ($fuente == 2) {
        $_SESSION['tipo_letra'] = 2;
        header('location:../Vistas/Anuncios.php');
    }

    if ($fuente == 3) {
        $_SESSION['tipo_letra'] = 3;
        header('location:../Vistas/Anuncios.php');
    }

    if ($fuente == 4) {
        $_SESSION['tipo_letra'] = 4;
        header('location:../Vistas/Anuncios.php');
    }
}
