<?php

namespace si2;
require_once "../Modelo/MCuentasBancarias.php";

$nro_cuenta = (int)$_POST['nro_cuenta'];

if (empty($nro_cuenta)) {

    echo "<h3 align='center'>Porfavor ingresar su nro de cuenta</h3>";
    echo "<br>";
    echo "<h3 align='center'><a href='../Vistas/vista_elim_cuenta.html'>Volver a intentar</a></h3>";
    return;
}


$cuenta = new \MCuentasBancarias();
$conexion = $cuenta->conect_database();

$cuenta->delete_data($nro_cuenta, $conexion);

if ($conexion->affected_rows == 0) {
    echo "<h3 align='center'>Hubo un problema al eliminar tu cuenta porfavor verifica tu nro de cuenta</h3>";
    echo "<br>";
    echo "<h3 align='center'><a  href='../Vistas/index.php' >Volver</a></h3>";

    return;
}

echo "<h3 align='center'> Borrado con exito</h3>";
