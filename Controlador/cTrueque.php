<?php

require_once("../Modelo/MTrueque.php");
require_once("../Modelo/MBitacora.php");

if (isset($_POST['btn_addTrueque'])) {
    session_start();
    $cli = $_SESSION['ci_cliente'];


    $d_trueque = $_POST['d_trueq'];
    $dif_trueque = $_POST['dif_t'];
 
    $tru = new MTrueque();
    $res = $tru->addTrueque("$d_trueque", "$dif_trueque");
    if ($res == 1) {
        $req = MBitacora::insertBitacora('Se registro informacion de Trueque', "$cli");
        header('location:../Vistas/vAnuncio.php');
    } else {
        header('location:../Vistas/vTrueque.php');
    }
}


if (isset($_POST['btn_addTrueque2'])) {
    session_start();
    $cli = $_SESSION['ci_cliente'];

    $d_trueque = $_POST['d_trueq'];
    $dif_trueque = $_POST['dif_t'];

    $tru = new MTrueque();
    $res = $tru->addTrueque("$d_trueque", "$dif_trueque");
    if ($res == 1) {
        $req = MBitacora::insertBitacora('Se registro informacion de Trueque', "$cli");
        header('location:../Vistas/vAnuncio2.php');
    } else {
        header('location:../Vistas/vTrueque2.php');
    }
}
