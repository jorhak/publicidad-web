<?php

require_once "../Modelo/MAddCuentaBancaria.php";
require_once("../Modelo/MBitacora.php");


if (isset($_POST["btn_add_cb"])) {
    session_start();
    $ci = $_SESSION['ci_cliente'];

    $Cuenta = $_POST["nCuentaBancaria"]; /// la cuenta se recibe desde la vista

    $CuentaBacaria = new MAddCuentaBancaria(); /// creamos un objeto cuenta bancaria

    $monto = $CuentaBacaria->montoCuentaBancaria("$Cuenta");

    if (is_null($monto)) {
        header('location:../Vistas/vCliente.php');
    } else {
        $res = $CuentaBacaria->addCuentaBancaria("$Cuenta", "$monto", "$ci");

        if ($res == 1) {
            $req = MBitacora::insertBitacora('El cliente registro una Cuenta Bancaria ', "$ci");
            header('location:../Vistas/vCuentasbancarias.php');
        } else {
            $res = $CuentaBacaria->activarCuentasBancarias("$Cuenta");
            $req = MBitacora::insertBitacora('El cliente registro una Cuenta Bancaria ', "$ci");
            header('location:../Vistas/vCuentasbancarias.php');
        }
    }
}
