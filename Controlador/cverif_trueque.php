<?php

require_once("../Modelo/MTrueque.php");

$tru = new MTrueque();

if (isset($_POST['smt_si'])) {
    header("Location: ../Vistas/vTrueque.php");
}

if (isset($_POST['smt_no'])) {
    $tru->addTruequeNO();
    header("Location: ../Vistas/vAnuncio.php");
}

if (isset($_POST['smt_si2'])) {
    header("Location: ../Vistas/vTrueque2.php");
}

if (isset($_POST['smt_no2'])) {
    $tru->addTruequeNO();
    header("Location: ../Vistas/vAnuncio2.php");
}

?>
