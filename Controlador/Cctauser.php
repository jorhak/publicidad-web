<?php

require_once "../Modelo/MCuentaBancarias.php";

$cuenta_nro = (int)$_POST['numero_cuenta'];
$monto = (int)$_POST['monto'];

if (empty($cuenta_nro) or empty($monto)) {

    header('location:../Vistas/index.php');

} else {
    $cuenta = new cuentaBancaria($cuenta_nro, $monto);

    $conection = $cuenta->conect_database();

    if ($conection == "No") {
        echo "No se ha podido conectar con la base de datos porfavor vuelva a intentarlo luego o mas tarde";
    } else {
        $result = $cuenta->insert_data($cuenta_nro, $monto, $conection);

        if ($result == false) {

            echo "<h3 align='center'>Hubo un problema al agregar la cuenta</h3>";
            echo "<h3 align='center'><a href='../Vistas/index.php'>Volver a la pag principal</a></h3>";
            return;
        }

        echo "<h3 align='center' >Agregado exitosamente</h3>";

        echo "<br>";

        echo "<h3 align='center'><a href='../Vistas/index.php'>Volver a la pag principal</a></h3>";
    }
}
