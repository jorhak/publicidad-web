<?php
require_once('../Modelo/MAnuncio.php');
require_once("../Modelo/MBitacora.php");
require_once("../Modelo/MTrueque.php");
require_once("../Modelo/MInmueble.php");
require_once("../Modelo/MUbicacion.php");
require_once("../Modelo/MGaleria.php");
require_once("../Modelo/MProducto.php");

if (isset($_POST['submit_new_anuncio'])) {
    session_start();
    $ci = $_SESSION['ci_cliente'];

    $a = new MAnuncio();
    $titulo = $_POST['title_a'];
    $descrip = $_POST['descrip'];

    $tipo = $_POST['tipo_anuncio'];

    $id_tipo = $a->getIdDetaAnuncio("$tipo");

    $resp = $a->addAnuncio("$titulo", "$descrip", "$id_tipo", "$ci");
    if ($resp == 1) {
        $id_anun = $a->getIdDi();
        $req = MBitacora::insertBitacora('Se inserto informacion del anuncio', "$ci");
        header('location:../Vistas/vAnuncio_Inmueble.php?id=' . $id_anun);
    } else {
        header('location:../Vistas/vAnuncio.php');
    }
}


if (isset($_POST['submit_new_anuncio2'])) {
    session_start();
    $ci = $_SESSION['ci_cliente'];

    $a = new MAnuncio();
    $titulo = $_POST['title_a'];
    $descrip = $_POST['descrip'];

    $tipo = $_POST['tipo_anuncio'];

    $cat_A = $_POST['cat_anuncio'];

    $id_tipo = $a->getIdDetaAnuncio("$tipo");

    $resp = $a->addAnuncio2("$titulo", "$descrip", "$id_tipo", "$ci", "$cat_A");
    if ($resp == 1) {
        $id_anun = $a->getIdDi();
        $req = MBitacora::insertBitacora('Se inserto informacion del anuncio', "$ci");
        header('location:../Vistas/vAnuncio_Producto.php?id=' . $id_anun);
    } else {
        header('location:../Vistas/vAnuncio2.php');
    }
}

if (isset($_POST['ver_anuncio'])) {
    MUbicacion::obtenerUbicacion("$id_anuncio");
    MTrueque::obtenerTrueque("$id_anuncio");
    MGaleria::obtenerGaleria("$id_anuncio");
    MInmueble::obtenerDatosInmueble("$id_anuncio");
    MProducto::obtenerProducto("$id_anuncio");
    MAnuncio::obtenerDatosAnuncios("$ci_cliente");
}
