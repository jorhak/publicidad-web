<?php
require_once("../Modelo/MPrivilegio.php");

if (isset($_POST['privilegio'])) {
    $ci = $_POST['ci'];
    //GESTIONAR CLIENTE
    if (isset($_POST['gestionarCliente'])) {
        $cu = 1;
        $estado = 1;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    } else {
        $cu = 1;
        $estado = 0;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    }

    //GESTIONAR EMPLEADO
    if (isset($_POST['gestionarEmpleado'])) {
        $cu = 2;
        $estado = 1;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req1 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    } else {
        $cu = 2;
        $estado = 0;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req1 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    }

    //GESTIONAR PRIVILEGIO
    if (isset($_POST['gestionarPrivilegios'])) {
        $cu = 3;
        $estado = 1;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req2 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    } else {
        $cu = 3;
        $estado = 0;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req2 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    }

    //GESTIONAR REPORTE
    if (isset($_POST['gestionarReporte'])) {
        $cu = 4;
        $estado = 1;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req3 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    } else {
        $cu = 4;
        $estado = 0;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req3 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    }

    //GESTIONAR SOPORTE
    if (isset($_POST['gestionarSoporte'])) {
        $cu = 5;
        $estado = 1;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req4 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    } else {
        $cu = 5;
        $estado = 0;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req5 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    }

    //GESTIONAR BITACORA
    if (isset($_POST['gestionarBitacora'])) {
        $cu = 6;
        $estado = 1;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req6 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    } else {
        $cu = 6;
        $estado = 0;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req6 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    }

    //GESTIONAR BACKUP
    if (isset($_POST['gestionarBackup'])) {
        $cu = 7;
        $estado = 1;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req7 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    } else {
        $cu = 7;
        $estado = 0;
        echo $cu . "|  " . $estado . "|  " . $ci;
        $req7 = MPrivilegio::updatePrivilegio($cu, $estado, $ci);
    }


    if ($req || $req1 || $req2 || $req3 || $req4 || $req5 || $req6 || $req7) {
        header('location:../Vistas/vAdmin.php');
        exit();
    } else {
        header('location:../Vistas/error500.php');
        exit();
    }
}
