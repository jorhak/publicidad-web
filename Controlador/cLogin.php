<?php

require_once("../Modelo/MLogin.php");
require_once("../Modelo/MBitacora.php");

$per = new MUsuario();

if (isset($_POST['submit_login'])) {
    $user = $_POST['uname'];
    $pass = $_POST['upass'];
    $datos = $per->getUser("$user", "$pass");
    $id_rol = $per->getRol("$user", "$pass");
 
    if ($datos > 0 && $id_rol == 2) {
        session_start();
        $_SESSION['ci_cliente'] = $datos;
        $req = MBitacora::insertBitacora('Inicio Sesion el usuario: ' . $user, $datos);
        //header('location:../Vistas/Anuncios.php?id=3');
        header('location:../Vistas/Anuncios.php');
    } else {
        if ($datos > 0 && $id_rol == 1) {
            session_start();
            $_SESSION['ci_cliente'] = $datos;
            $_SESSION['user'] = $user;
            $_SESSION['rol'] = $id_rol;
            $req = MBitacora::insertBitacora('Se logeo el administrador: ' . $user, $datos);
            header("Location:../Vistas/vAdmin.php");
        } else {
            header('location:../Vistas/login.html');
        }
    }
}
