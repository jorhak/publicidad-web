<?php

require_once("../Modelo/MCliente.php");
require_once("../Modelo/MBitacora.php");

session_start();

if (!isset($_POST['btn-Guardar']) && !isset($_POST['submit_new_login'])) {
    $var = $_SESSION['ci_cliente'];
    $cli = new MCliente();
    $datos = $cli->getCliente("$var");
    $vv = $_SESSION['ci_cliente'];
}

if (isset($_POST['submit_new_login'])) {
    $newCliente = new MCliente();
    $v1 = $_POST['ci_ncliente'];
    $v2 = $_POST['nnombre'];
    $v3 = $_POST['nape'];
    $v4 = $_POST['nemail'];
    $v5 = $_POST['ncel'];

    $v6 = $_POST['nid'];
    $v7 = $_POST['npas'];

    $res = $newCliente->addCliente("$v1", "$v2", "$v3", "$v4", "$v5");
    if ($res == 1) {
        $req = MBitacora::insertBitacora('Se inserto un nuevo cliente : ' . $v2 . ' ' . $v3, "$v1");
        /*$bita->insertBitacora('Se inserto un nuevo cliente'.$v2.' '.$v3,"$v1");*/
        $res2 = $newCliente->addUser("$v6", "$v7", "$v1");
        if ($res2 == 1) {
            $r3 = MBitacora::insertBitacora('Se inserto un nuevo Usuario : ' . $v6, "$v1");
            header('location:../Vistas/login.html');
        } else {
            header('location:../Vistas/vNewCuentaUser.php');
        }
    } else {
        header('location:../Vistas/vNewCuentaUser.php');
    }
}

if (isset($_POST['btn-Guardar']) && !isset($_POST['submit_new_login'])) {

    $cli_up = new MCliente();
    $v = $_SESSION['ci_cliente'];
    $v33 = $_POST['edit_email'];
    $v44 = $_POST['edit_cel'];
    $v55 = $_POST['email_old'];
    $v66 = $_POST['cel_old'];

    if ($v33 == '') {
        $v33 = $v55;
    }
    if ($v44 == '') {
        $v44 = $v66;
    }
    $c = $cli_up->editUser("$v", "$v33", "$v44");
    //echo($v);
    if ($c == 1) {
        $req3 = MBitacora::insertBitacora('Se modificaron datos del cliente', "$v");
        header('location:../Vistas/vCliente.php');
    } else {
        header('location:../Vistas/login.html');
    }
}
