<?php

require_once("../Modelo/MInmueble.php");
require_once("../Modelo/MBitacora.php");

if (isset($_POST['submit_new_inmueble'])) {

    session_start();
    $cli = $_SESSION['ci_cliente'];

    $inmu = new MInmueble();
    $v1 = $_POST['nro_casa'];
    $v2 = $_POST['precio'];
    $v3 = $_POST['estado'];
    $v4 = $_POST['categoria'];
    $v5 = $_POST['tipo'];
    $v6 = $_POST['superficie'];
    $v7 = $_POST['pisos'];
    $v8 = $_POST['habitaciones'];
    $v9 = $_POST['baños'];
    $v10 = $_POST['garaje'];
    $v11 = $_POST['alcantarillado'];
    $v12 = $_POST['detalles'];
    $res = $inmu->addInmueble("$v1", "$v2", "$v3", "$v4", "$v5", "$v6", "$v7", "$v8", "$v9", "$v10", "$v11", "$v12");

    if ($res == 1) {
        $req = MBitacora::insertBitacora('Se registraron Datos de un Inmueble', "$cli");
        header('location:../Vistas/vGaleria.php');
    } else {
        header('location:../Vistas/vInmueble.php');
    }

}
