<?php

require_once "../Modelo/MUbicacion.php";
require_once("../Modelo/MBitacora.php");

if (isset($_POST["btn_addUbi"])) {
    session_start();
    $cli = $_SESSION['ci_cliente'];
    $direccion = $_POST["direccion"];
    $zona = $_POST["zona"];
    $departamento = $_POST["departamentos"];

    $ubicacion = new MUbicacion();
    $ubicacion->setDireccion($direccion);
    $ubicacion->setZona($zona);
    $ubicacion->setDepartamento($departamento);
    $resultado = $ubicacion->addAddress();
    if ($resultado == 1) {
        $req = MBitacora::insertBitacora('Se registro la ubicacion de un Inmueble', "$cli");
        header('location:../Vistas/vInmueble.php');
    } else { 
        header('location:../Vistas/vAnadirUbicacion.php');
    }
}

if (isset($_POST["btn_addUbi2"])) {

    session_start();
    $cli = $_SESSION['ci_cliente'];

    $direccion = $_POST["direccion"];
    $zona = $_POST["zona"];
    $departamento = $_POST["departamentos"];

    $ubicacion = new MUbicacion();
    $ubicacion->setDireccion($direccion);
    $ubicacion->setZona($zona);
    $ubicacion->setDepartamento($departamento);
    $resultado = $ubicacion->addAddress();
    if ($resultado == 1) {
        $req = MBitacora::insertBitacora('Se registro la ubicacion de un Producto', "$cli");
        header('location:../Vistas/vProducto.php');
    } else {
        header('location:../Vistas/vAnadirUbicacion.php');
    }

}
