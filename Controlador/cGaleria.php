<?php

require_once("../Modelo/MGaleria.php");
require_once("../Modelo/MBitacora.php");

session_start();
$cli = $_SESSION['ci_cliente'];

if (isset($_POST['submit_new_galeria'])) {
    $url = 'foto1';
    $url2 = 'foto2';
    $url3 = 'foto3';
    $imagen = addslashes(file_get_contents($_FILES['imagen']['tmp_name']));
    $imagen1 = addslashes(file_get_contents($_FILES['imagen1']['tmp_name']));
    $imagen2 = addslashes(file_get_contents($_FILES['imagen2']['tmp_name']));


    $foto = $_FILES['imagen']['name'];
    $foto1 = $_FILES['imagen1']['name'];
    $foto2 = $_FILES['imagen2']['name'];


    $ruta = $_FILES['imagen']['tmp_name'];
    $ruta1 = $_FILES['imagen1']['tmp_name'];
    $ruta2 = $_FILES['imagen2']['tmp_name'];


    $destino = "../fotos/" . $foto;
    copy($ruta, $destino);
    $okay = MGaleria::insertGaleria($url, $imagen, $destino);

    $destino = "../fotos/" . $foto1;
    copy($ruta1, $destino);
    $okay1 = MGaleria::insertGaleria($url2, $imagen1, $destino);

    $destino = "../fotos/" . $foto2;
    copy($ruta2, $destino);
    $okay2 = MGaleria::insertGaleria($url3, $imagen2, $destino);


    //$okay = $galeria->insertGaleria("$url", "$id_deta_inmu", "$imagen");
    if ($okay && $okay1 && $okay2) {

        //echo "si se inserto la Galeria";
        //$objeto_modelo = ubicacion::getTabla();
        $req = MBitacora::insertBitacora('Se registraron Fotos del Inmueble', "$cli");
        header("Location: ../Vistas/verif_trueque.php");
        exit();
    } else {
        header('Location: ../Vistas/vGaleria.php');
    }

}


if (isset($_POST['submit_new_galeria2'])) {
    $url = 'foto1';
    $url2 = 'foto2';
    $url3 = 'foto3';
    $imagen = addslashes(file_get_contents($_FILES['imagen']['tmp_name']));
    $imagen1 = addslashes(file_get_contents($_FILES['imagen1']['tmp_name']));
    $imagen2 = addslashes(file_get_contents($_FILES['imagen2']['tmp_name']));


    $foto = $_FILES['imagen']['name'];
    $foto1 = $_FILES['imagen1']['name'];
    $foto2 = $_FILES['imagen2']['name'];


    $ruta = $_FILES['imagen']['tmp_name'];
    $ruta1 = $_FILES['imagen1']['tmp_name'];
    $ruta2 = $_FILES['imagen2']['tmp_name'];


    $destino = "../fotos/" . $foto;
    copy($ruta, $destino);
    $okay = MGaleria::insertGaleria($url, $imagen, $destino);

    $destino = "../fotos/" . $foto1;
    copy($ruta1, $destino);
    $okay1 = MGaleria::insertGaleria($url2, $imagen1, $destino);

    $destino = "../fotos/" . $foto2;
    copy($ruta2, $destino);
    $okay2 = MGaleria::insertGaleria($url3, $imagen2, $destino);


    //$okay = $galeria->insertGaleria("$url", "$id_deta_inmu", "$imagen");
    if ($okay && $okay1 && $okay2) {

        //echo "si se inserto la Galeria";
        //$objeto_modelo = ubicacion::getTabla();
        $req = MBitacora::insertBitacora('Se registraron Fotos del Producto', "$cli");
        header("Location: ../Vistas/verif_trueque2.php");
        exit();
    } else {
        header('Location: ../Vistas/vGaleria2.php');
    }

}


// UPDATE

if (isset($_POST['submit_modificar'])) {
    $id = $_REQUEST['id'];
    $nombre = $_POST['nombre'];
    $imagen = addslashes(file_get_contents($_FILES['imagen']['tmp_name']));

    $okay = MGaleria::updateGaleria($nombre, $imagen, $id);
    if ($okay) {
        echo "se actualizo la galeria";
        header("Location: ../Vistas/vGaleria.php");
        exit();
    } else {
        echo "no se actulizo la galeria";
        header("Location: ../Vistas/error500.php");
    }
}
