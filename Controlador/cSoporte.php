<?php

require_once("../Modelo/MSoporte.php");
require_once("../Modelo/MBitacora.php");

if (isset($_POST['btn_addSoporte'])) {
    session_start();
    $ci_c = $_SESSION['ci_cliente'];
    $email = $_POST['email_c'];
    $descripcion = $_POST['descrip'];
    $soport = new MSoporte();
    $res = $soport->addSoporte("$email", "$descripcion", "$ci_c");
    if ($res == 1) {
        $req = MBitacora::insertBitacora('El Cliente solicito un Soporte Tecnico', "$ci_c");
        header('location:../Vistas/vCliente.php');
    } else {
        header('location:../Vistas/vSoporte.php');
    } 
}

if (!isset($_POST['a'])) {
    $soport = new MSoporte();
    $datos = $soport->getSoporte();
}
