<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Anuncio</title>

    <link rel="stylesheet" href="style.css"/>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="../Vistas/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="../Vistas/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="../Vistas/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../Vistas/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../Vistas/assets/ico/favicon.png">
    <title>Publicidad Web</title>
    <!-- Bootstrap core CSS -->
    <link href="../Vistas/assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="../Vistas/assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="../Vistas/assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="../Vistas/assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="../Vistas/assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="../Vistas/assets/js/pace.min.js"></script>
    <script src="../Vistas/assets/plugins/modernizr/modernizr-custom.js"></script>


</head>

<body>
<div id="content">
    <div class="navbar-identity">
        <a href="" class="navbar-brand logo logo-title">
<span class="logo-icon">
  <CENTER><img src="../Vistas/img/Logo6.png" width="400" height="70"></CENTER>  
</span>
        </a>
    </div>
    <br>
    <h1>
        <CENTER> REPORTES DE INMUEBLES POR PRECIO</CENTER>
    </h1>
    <form action="reporteT3.php" method="POST">
        <div class="search-row-wrapper" style="background-image: url(images/bg.jpg)">
            <div class="inner">
                <div class="container ">
                    <form action="vParametroZonaDpto.php" method="POST">
                        <div class="row">


                            <div class="col-md-3">
                                <input class="form-control keyword" type="text" name="inicial"
                                       placeholder="Precio Inicial">
                            </div>
                            -

                            <div class="col-md-3">
                                <input class="form-control keyword" type="text" name="final" placeholder="Precio Final">
                            </div>


                            <div class="col-md-3">
                                <button class="btn btn-block btn-primary btn-gradient" name="enviar"> Buscar <i
                                        class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </form>


    </form>

    <hr/>
