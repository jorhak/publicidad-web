<?php

require('fpdf/fpdf.php');
require('conexion.php');

class PDF extends FPDF
{
    var $widths;
    var $aligns;

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border

            $this->Rect($x, $y, $w, $h);

            $this->MultiCell($w, 5, $data[$i], 0, $a, 'true');
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    function Header()
    {

        $this->SetFont('Arial', '', 20);
        $this->Text(20, 14, '                REPORTE DE CLIENTES QUE PUBLICARON ANUNCIOS', 0, 0, 'C', 0);
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(100, 10, 'Anuncios', 0, 0, 'L');

    }

}


$pdf = new PDF('L', 'mm', 'Letter');
$pdf->Open();
$pdf->AddPage();
$pdf->SetMargins(20, 20, 20);
$pdf->Ln(10);
//										izquierda
$pdf->Image('../Vistas/img/Logo6.png', 15, 20, 100, 35, 'PNG');
$pdf->SetFont('Arial', '', 12);
$fecha = date('Y-m-d');

$pdf->Ln(10);
// Cabecera    NO SE TOCA

/*$pdf->Cell(Largo,Alto,TEXTO,BORDE,SALTO DE LINEA,ORIENTACION,COLOR);*/
$pdf->Cell(50, 6, 'C. I. del Cliente:', 0, 0, 'L');
$pdf->Cell(50, 6, '1111111', 0, 1, 'L');
$pdf->Cell(50, 6, 'Usuario:', 0, 0, 'L');
$pdf->Cell(50, 6, 'Administrador', 0, 1, 'L');
$pdf->Cell(50, 6, 'Fecha del Reporte:', 0, 0, 'L');
$pdf->Cell(50, 6, $fecha, 0, 1, 'L');
$pdf->Cell(100, 6, '', 0, 1, 'C'); // Salto de Linea
//$pdf->Cell(100,6,'',0,1,'C');
// LA cabezera de la Tabla
$pdf->Ln(5);

$pdf->SetWidths(array(25, 30, 60, 60, 20, 50));
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetFillColor(85, 107, 47);
$pdf->SetTextColor(255);

$re2 = array();
$e = 0;
for ($i = 0; $i < 1; $i++) {
    if (isset($_POST['fecha'])) {
        $re2[$e] = 'FECHA';
        $e++;
    }

    if (isset($_POST['email'])) {
        $re2[$e] = 'EMAIL';
        $e++;
    }
    if (isset($_POST['descripcion'])) {
        $re2[$e] = 'DESCRIPCION';
        $e++;
    }
    if (isset($_POST['respuesta'])) {
        $re2[$e] = 'RESPUESTA';
        $e++;
    }
    if (isset($_POST['ci'])) {
        $re2[$e] = 'CI';
        $e++;
    }
    if (isset($_POST['nombre_completo'])) {
        $re2[$e] = 'NOMBRE COMPLETO';
        $e++;
    }
    //$pdf->Cell(10,10,'cantidad de columna validad'.count($re2),0,1);
    $pdf->Row($re2);

}

$con = new Conexion;
$db = $con->con();

$strConsulta = "SELECT soporte.fecha,soporte.email,soporte.descripcion,soporte.respuesta,cliente.ci, concat(cliente.nombre, cliente.apellido)as nombre_completo FROM soporte Inner Join cliente ON soporte.ci_cliente = cliente.ci WHERE soporte.ci_cliente = cliente.ci order by soporte.fecha asc";

$req = $db->query($strConsulta);
$numfilas = $req->num_rows;

$re2 = array();
for ($i = 0; $i < $numfilas; $i++) {
    $ee = 0;
    $fila = $req->fetch_array();
    $pdf->SetFont('Arial', '', 12);

    if ($i % 2 == 1) {
        if (isset($_POST['fecha'])) {
            $re2[$ee] = $fila['fecha'];
            $ee++;
        }
        if (isset($_POST['email'])) {
            $re2[$ee] = $fila['email'];
            $ee++;
        }
        if (isset($_POST['descripcion'])) {
            $re2[$ee] = $fila['descripcion'];
            $ee++;
        }
        if (isset($_POST['respuesta'])) {
            $re2[$ee] = $fila['respuesta'];
            $ee++;
        }
        if (isset($_POST['ci'])) {
            $re2[$ee] = $fila['ci'];
            $ee++;
        }
        if (isset($_POST['nombre_completo'])) {
            $re2[$ee] = $fila['nombre_completo'];
            $ee++;
        }


        $pdf->SetFillColor(60, 60, 60);
        $pdf->SetTextColor(255);
        $pdf->Row($re2);

    } else {
        $ee = 0;
        if (isset($_POST['fecha'])) {
            $re2[$ee] = $fila['fecha'];
            $ee++;
        }
        if (isset($_POST['email'])) {
            $re2[$ee] = $fila['email'];
            $ee++;
        }
        if (isset($_POST['descripcion'])) {
            $re2[$ee] = $fila['descripcion'];
            $ee++;
        }
        if (isset($_POST['respuesta'])) {
            $re2[$ee] = $fila['respuesta'];
            $ee++;
        }
        if (isset($_POST['ci'])) {
            $re2[$ee] = $fila['ci'];
            $ee++;
        }
        if (isset($_POST['nombre_completo'])) {
            $re2[$ee] = $fila['nombre_completo'];
            $ee++;
        }


        $pdf->SetFillColor(60, 60, 60);
        $pdf->SetTextColor(255);
        $pdf->Row($re2);
    }
}
//$pdf->Cell(0,6,'cantidad de columna '.count($re2),0,1);


$pdf->Output();
?>
