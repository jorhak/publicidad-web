<?php

require('fpdf/fpdf.php');
require('conexion.php');

class PDF extends FPDF
{
    var $widths;
    var $aligns;

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border

            $this->Rect($x, $y, $w, $h);

            $this->MultiCell($w, 5, $data[$i], 0, $a, 'true');
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    function Header()
    {

        $this->SetFont('Arial', '', 20);
        $this->Text(20, 14, '                       REPORTE DE ANUNCIOS QUE ACEPTAN TRUEQUES', 0, 0, 'C', 0);
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(100, 10, 'Anuncios', 0, 0, 'L');

    }

}

$pdf = new PDF('L', 'mm', 'Legal');
//$pdf=new FPDF('L','mm','A4');
//$pdf=new PDF('L','mm','Letter'); //orientacion de la hoja
$pdf->Open();
$pdf->AddPage();
$pdf->SetMargins(20, 20, 20);
$pdf->Ln(10);
//                                      izquierda
$pdf->Image('../Vistas/img/Logo6.png', 15, 20, 100, 35, 'PNG');
$pdf->SetFont('Arial', '', 12);
$fecha = date('Y-m-d');

$pdf->Ln(10);
// Cabecera    NO SE TOCA

/*$pdf->Cell(Largo,Alto,TEXTO,BORDE,SALTO DE LINEA,ORIENTACION,COLOR);*/
$pdf->Cell(50, 6, 'C. I. del Cliente:', 0, 0, 'L');
$pdf->Cell(50, 6, '1111111', 0, 1, 'L');
$pdf->Cell(50, 6, 'Usuario:', 0, 0, 'L');
$pdf->Cell(50, 6, 'Administrador', 0, 1, 'L');
$pdf->Cell(50, 6, 'Fecha del Reporte:', 0, 0, 'L');
$pdf->Cell(50, 6, $fecha, 0, 1, 'L');
$pdf->Cell(100, 6, '', 0, 1, 'C'); // Salto de Linea
//$pdf->Cell(100,6,'',0,1,'C');
// LA cabezera de la Tabla
$pdf->Ln(5);

$pdf->SetWidths(array(60, 30, 20, 17, 15, 30, 40, 50));
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetFillColor(85, 107, 47);
$pdf->SetTextColor(255);

$re2 = array();
$e = 0;
for ($i = 0; $i < 1; $i++) {

    if (isset($_POST['titulo'])) {
        $re2[$e] = 'TITULO';
        $e++;
    }
    if (isset($_POST['categoria'])) {
        $re2[$e] = 'CATEGORIA';
        $e++;
    }
    if (isset($_POST['precio'])) {
        $re2[$e] = 'PRECIO';
        $e++;
    }
    if (isset($_POST['estado'])) {
        $re2[$e] = 'ESTADO';
        $e++;
    }
    if (isset($_POST['zona'])) {
        $re2[$e] = 'ZONA';
        $e++;
    }
    if (isset($_POST['dpto'])) {
        $re2[$e] = 'DPTO';
        $e++;
    }
    if (isset($_POST['direccion'])) {
        $re2[$e] = 'DIRECCION';
        $e++;
    }
    if (isset($_POST['permuta'])) {
        $re2[$e] = 'PERMUTA';
        $e++;
    }

    //$pdf->Cell(10,10,'cantidad de columna validad'.count($re2),0,1);
    $pdf->Row($re2);

}

$con = new Conexion;
$db = $con->con();

$strConsulta = "SELECT anuncio.id, anuncio.titulo, categoria.descripcion as categoria, producto_inmueble.precio, producto_inmueble.estado,ubicacion.zona,ubicacion.dpto,ubicacion.direccion, trueque.permuta 
        FROM anuncio 
        INNER JOIN categoria ON categoria.id = anuncio.id_categoria 
        INNER JOIN producto_inmueble ON producto_inmueble.id = anuncio.id_prod_inmu 
        INNER JOIN ubicacion on ubicacion.id = producto_inmueble.id_ubicacion 
        INNER JOIN trueque on trueque.id_prod_inm = producto_inmueble.id 
        WHERE anuncio.estado='activo' ORDER BY anuncio.id ASC";

$req = $db->query($strConsulta);
$numfilas = $req->num_rows;

$re2 = array();
for ($i = 0; $i < $numfilas; $i++) {
    $ee = 0;
    $fila = $req->fetch_array();
    $pdf->SetFont('Arial', '', 12);

    if ($i % 2 == 1) {

        if (isset($_POST['titulo'])) {
            $re2[$ee] = $fila['titulo'];
            $ee++;
        }
        if (isset($_POST['categoria'])) {
            $re2[$ee] = $fila['categoria'];
            $ee++;
        }
        if (isset($_POST['precio'])) {
            $re2[$ee] = $fila['precio'];
            $ee++;
        }
        if (isset($_POST['estado'])) {
            $re2[$ee] = $fila['estado'];
            $ee++;
        }
        if (isset($_POST['zona'])) {
            $re2[$ee] = $fila['zona'];
            $ee++;
        }
        if (isset($_POST['dpto'])) {
            $re2[$ee] = $fila['dpto'];
            $ee++;
        }
        if (isset($_POST['direccion'])) {
            $re2[$ee] = $fila['direccion'];
            $ee++;
        }
        if (isset($_POST['permuta'])) {
            $re2[$ee] = $fila['permuta'];
            $ee++;
        }

        $pdf->SetFillColor(60, 60, 60);
        $pdf->SetTextColor(255);
        $pdf->Row($re2);

    } else {
        $ee = 0;
        if (isset($_POST['#'])) {
            $re2[$ee] = $fila['id'];
            $ee++;
        }
        if (isset($_POST['titulo'])) {
            $re2[$ee] = $fila['titulo'];
            $ee++;
        }
        if (isset($_POST['categoria'])) {
            $re2[$ee] = $fila['categoria'];
            $ee++;
        }
        if (isset($_POST['precio'])) {
            $re2[$ee] = $fila['precio'];
            $ee++;
        }
        if (isset($_POST['estado'])) {
            $re2[$ee] = $fila['estado'];
            $ee++;
        }
        if (isset($_POST['zona'])) {
            $re2[$ee] = $fila['zona'];
            $ee++;
        }
        if (isset($_POST['dpto'])) {
            $re2[$ee] = $fila['dpto'];
            $ee++;
        }
        if (isset($_POST['direccion'])) {
            $re2[$ee] = $fila['direccion'];
            $ee++;
        }
        if (isset($_POST['permuta'])) {
            $re2[$ee] = $fila['permuta'];
            $ee++;
        }


        $pdf->SetFillColor(60, 60, 60);
        $pdf->SetTextColor(255);
        $pdf->Row($re2);
    }
}
//$pdf->Cell(0,6,'cantidad de columna '.count($re2),0,1);


$pdf->Output();
?>
