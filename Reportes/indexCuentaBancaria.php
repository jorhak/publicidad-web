<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Pacientes</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">

    <h1>Cuentas Bancarias</h1>

    <hr/>

    <?php
    include_once("conexion.php");

    $con = new Conexion;
    $conectar = $con->con();
    $strConsulta = "SELECT * from cuenta_bancaria";
    $pacientes = $conectar->query($strConsulta);
    $numlista = 0;

    echo '<table cellpadding="0" cellspacing="0" width="100%">';
    echo '<thead><tr><td>Nro. CUENTA</td><td>MONTO</td><td>ESTADO</td><td>CI</td></tr></thead>';
    foreach ($pacientes as $fila) {
        $numlista++;
        echo '<tr><td>' . $fila['nro_cuenta'] . '</td>';
        echo '<td>' . $fila['monto'] . '</td>';
        echo '<td>' . $fila['estado'] . '</td>';
        echo '<td>' . $fila['ci_cliente'] . '</td>';
        echo '</tr>';
    }
    echo "</table>";
    ?>

    <div class="col-md-12">
        <form action="reporteCuentaBancaria.php">
            <input type="submit" name="create_pdf" class="btn btn-danger pull-right"
                   value="Cuentas validas y a quien les pertenece">
        </form>
    </div>

</div>
</body>
</html>
