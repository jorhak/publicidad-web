<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Anuncios</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">

    <h1>Anuncios</h1>

    <div class="col-md-12">
        <form action="reporteAnuncio.php">
            <input type="submit" name="create_pdf" class="btn btn-danger pull-right" value="Ver Anuncios">
        </form>
    </div>

    <hr/>

    <?php
    include_once("conexion.php");

    $con = new Conexion;
    $conectar = $con->con();
    $strConsulta = "SELECT anuncio.titulo,anuncio.fecha_ini as fecha_publicacion,anuncio.estado, categoria.descripcion as categoria, producto_inmueble.precio, concat(cliente.nombre,' ',cliente.apellido) as nombre_completo FROM anuncio INNER JOIN categoria ON anuncio.id_categoria = categoria.id INNER JOIN producto_inmueble ON anuncio.id_prod_inmu=producto_inmueble.id INNER JOIN cliente ON anuncio.ci_cliente=cliente.ci WHERE cliente.ci=anuncio.ci_cliente order by anuncio.id asc";
    $pacientes = $conectar->query($strConsulta);
    $numlista = 0;

    echo '<table cellpadding="0" cellspacing="0" width="100%">';
    echo '<thead><tr><td>Titulo</td><td>Fecha de Publicacion</td><td>Estado</td><td>Categoria</td><td>Precio</td><td>Nobre Comple</tr></thead>';
    foreach ($pacientes as $fila) {
        $numlista++;
        echo '<tr><td>' . $fila['titulo'] . '</td>';
        echo '<td>' . $fila['fecha_publicacion'] . '</td>';
        echo '<td>' . $fila['estado'] . '</td>';
        echo '<td>' . $fila['categoria'] . '</td>';
        echo '<td>' . $fila['precio'] . '</td>';
        //echo '<td>'.$fila['id_deta_inmu'].'</td>';
        echo '<td>' . $fila['nombre_completo'] . '</td>';

    }
    echo "</table>";
    ?>


</div>
</body>
</html>
