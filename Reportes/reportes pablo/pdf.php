<?php
require('rotation.php');

class PDF extends PDF_Rotate
{
	/*function Header()
	{
	    //Put the watermark
	    $this->SetFont('Arial','B',50);
	    $this->SetTextColor(255,192,203);
	    $this->Text(35,190,'W a t e r m a r k   d e m o');
	}*/

	function RotatedImage($file,$x,$y,$w,$h,$angle)
	{
    //Image rotated around its upper-left corner
    $this->Rotate($angle,$x,$y);
    $this->Image($file,$x,$y,$w,$h);
    $this->Rotate(0);
	}

}
?>