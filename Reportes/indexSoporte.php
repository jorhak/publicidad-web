<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Pacientes</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">

    <h1>Soporte</h1>

    <hr/>

    <?php
    include_once("conexion.php");

    $con = new Conexion;
    $conectar = $con->con();
    $strConsulta = "SELECT * from soporte";
    $pacientes = $conectar->query($strConsulta);
    $numlista = 0;

    echo '<table cellpadding="0" cellspacing="0" width="100%">';
    echo '<thead><tr><td>No.</td><td>FECHA</td><td>EMAIL</td><td>DESCRIPCION</td><td>CI</td></tr></thead>';
    foreach ($pacientes as $fila) {
        $numlista++;
        echo '<tr><td>' . $fila['id'] . '</td>';
        echo '<td>' . $fila['fecha'] . '</td>';
        echo '<td>' . $fila['email'] . '</td>';
        echo '<td>' . $fila['descripcion'] . '</td>';
        echo '<td>' . $fila['ci_cliente'] . '</td>';
        echo '</tr>';
    }
    echo "</table>";
    ?>
    <div class="col-md-12">
        <form action="reporteSoporte.php">
            <input type="submit" name="create_pdf" class="btn btn-danger pull-right"
                   value="Contestados por el administrador">
        </form>
    </div>

</div>
</body>
</html>
