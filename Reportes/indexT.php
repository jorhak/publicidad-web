<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Anuncio</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">
    <div class="navbar-identity">
        <a href="" class="navbar-brand logo logo-title">
<span class="logo-icon">
  <CENTER><img src="../Vistas/img/Logo6.png" width="400" height="70"></CENTER>  
</span>
        </a>
    </div>
    <br>
    <h1>
        <CENTER> REPORTES PERSONALIZADO</CENTER>
    </h1>
    <form action="reporteT.php" method="POST">
        <br>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="categoria">CATEGORIA
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="titulo">TITULO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="descripcion">DESCRIPCION
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="precio">PRECIO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="clase">CLASE
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="superficie">SUPERFICIE
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="cant_pisos">CANT PISOS
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="cant_habitaciones">CANT HABITACIONES
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="cant_banos">CANT BAÑOS
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" checked="" name="garaje">GARAJE
            </label>
        </div>
        <br>
        <button type="submit" class="btn btn-primary" name="enviar">GENERAR REPORTE</button>
    </form>

    <hr/>
