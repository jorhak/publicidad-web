<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Anuncio</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">
    <div class="navbar-identity">
        <a href="" class="navbar-brand logo logo-title">
<span class="logo-icon">
  <CENTER><img src="../Vistas/img/Logo6.png" width="400" height="70"></CENTER>  
</span>
        </a>
    </div>
    <br>
    <h1>
        <CENTER> REPORTES DE CLIENTES QUE SOLICITARON SOPORTE</CENTER>
    </h1>
    <form action="reporteSoporte1.php" method="POST">
        <br>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="fecha">FECHA
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="email">EMAIL
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="descripcion">DESCRIPCION
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="respuesta">RESPUESTA
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="ci">CI
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="nombre_completo">NOMBRE COMPLETO
            </label>
        </div>
        <br>
        <button type="submit" class="btn btn-primary" name="enviar">IR</button>
    </form>

    <hr/>

    <?php
    include_once("conexion.php");

    $con = new Conexion;
    $conectar = $con->con();
    //arreglar el where
    $strConsulta = "SELECT soporte.fecha,soporte.email,soporte.descripcion,soporte.respuesta,cliente.ci, concat(cliente.nombre, cliente.apellido)as nombre_completo FROM soporte Inner Join cliente ON soporte.ci_cliente = cliente.ci WHERE soporte.ci_cliente = cliente.ci order by soporte.fecha asc";
    $pacientes = $conectar->query($strConsulta);
    $numlista = 0;
    ?>

    <?php if (!isset($_GET['enviar'])) { ?>
        <?php
        echo '<table cellpadding="0" cellspacing="0" width="100%">';
        echo '<thead><tr><td>FECHA</td><td>EMAIL</td><td>DESCRIPCION</td><td>RESPUESTA</td><td>CI</td><td>NOMBRE COMPLETO</td></tr></thead>';
        foreach ($pacientes as $fila) {
            //$numlista++;
            //echo '<tr><td>'.$numlista.'</td>';
            echo '<td>' . $fila['fecha'] . '</td>';
            echo '<td>' . $fila['email'] . '</td>';
            echo '<td>' . $fila['descripcion'] . '</td>';
            echo '<td>' . $fila['respuesta'] . '</td>';
            echo '<td>' . $fila['ci'] . '</td>';
            echo '<td>' . $fila['nombre_completo'] . '</td>';
            echo '</tr>';
        }
        echo "</table>";
        ?>
    <?php } ?>


    <?php if (isset($_GET['enviar'])) { ?>
        <!-- <?php
        // echo '<table cellpadding="0" cellspacing="0" width="100%">';
        // echo '<thead>
        // 		<tr>
        // 			<td>No.</td>';
        ?> -->
        <!--<?php $var = ""; ?> -->

        <?php if (isset($_GET['fecha'])) {
            $var = "?" . $var . $_GET['fecha']; ?>
            <td>FECHA</td>
        <?php } ?>

        <?php if (isset($_GET['email'])) {
            $email = $_GET['email']; ?>
            <td>EMAIL</td>
        <?php } ?>

        <?php if (isset($_GET['descripcion'])) {
            $descripcion = $_GET['descripcion']; ?>
            <td>DESCRIPCION</td>
        <?php } ?>
        <?php if (isset($_GET['respuesta'])) {
            $respuesta = $_GET['respuesta']; ?>
            <td>RESPUESTA</td>
        <?php } ?>
        <?php if (isset($_GET['ci'])) {
            $ci = $_GET['ci']; ?>
            <td>CI</td>
        <?php } ?>

        <?php if (isset($_GET['nombre_completo'])) {
            $var = "?" . $var . $_GET['nombre_completo']; ?>
            <td>NOMBRE COMPLETO</td>
        <?php } ?>
        <?php
        echo '	</tr>
				</thead>';
        ?>


        <?php foreach ($pacientes as $fila) {
            $numlista++; ?>
            <tr class="gradeX">
                <td>
                    <?php echo $numlista; ?>
                </td>
                <?php if (isset($_GET['fecha'])) { ?>
                    <td>
                        <?php echo $fila['FECHA']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['email'])) { ?>
                    <td>
                        <?php echo $fila['EMAIL']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['descripcion'])) { ?>
                    <td>
                        <?php echo $fila['DESCRIPCION']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['respuesta'])) { ?>
                    <td>
                        <?php echo $fila['RESPUESTA']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['ci'])) { ?>
                    <td>
                        <?php echo $fila['CI']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['nombre_completo'])) { ?>
                    <td>
                        <?php echo $fila['NOMBRE COMPLETO']; ?>
                    </td>
                <?php } ?>
            </tr>

        <?php } ?>
        <?php
        echo "</table>";
        ?>
        <div class="col-md-12">
            <form action="reporteSoporte.php?">
                <input type="submit" name="create_pdf" class="btn btn-danger pull-right" value="reporte por dpto">
            </form>
        </div>
    <?php } ?>


</div>
</body>
</html>
