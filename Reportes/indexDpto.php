<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Anuncio</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">
    <div class="navbar-identity">
        <a href="" class="navbar-brand logo logo-title">
<span class="logo-icon">
  <CENTER><img src="../Vistas/img/Logo6.png" width="400" height="70"></CENTER>  
</span>
        </a>
    </div>
    <br>
    <h1>
        <CENTER> REPORTES DE ANUNCIOS</CENTER>
    </h1>
    <form action="reporteDpto.php" method="POST">
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="titulo">TITULO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="descripcion">DESCRIPCION
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="nombre">NOMBRE COMPLETO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="precio">PRECIO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="direccion">DIRECCION
            </label>
        </div>
        <br>
        <button type="submit" class="btn btn-primary" name="enviar">Filtrar</button>
    </form>

    <hr/>

    <?php
    include_once("conexion.php");

    $con = new Conexion;
    $conectar = $con->con();
    $strConsulta = "SELECT anuncio.titulo, anuncio.descripcion, concat(cliente.nombre, cliente.apellido)as nombre_completo, producto_inmueble.precio, ubicacion.direccion 
	FROM anuncio 
	Inner Join cliente ON anuncio.ci_cliente= cliente.ci
	Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
    Inner Join ubicacion ON ubicacion.id=producto_inmueble.id_ubicacion
	WHERE anuncio.estado='activo' order by anuncio.id asc;";
    $pacientes = $conectar->query($strConsulta);
    $numlista = 0;
    ?>

    <?php if (!isset($_GET['enviar'])) { ?>
        <?php
        echo '<table cellpadding="0" cellspacing="0" width="100%">';
        echo '<thead><tr><td>No.</td><td>TITULO</td><td>DESCRIPCION</td><td>NOMBRE COMPLETO</td><td>PRECIO</td><td>DIRECCION</td></tr></thead>';
        foreach ($pacientes as $fila) {
            $numlista++;
            echo '<tr><td>' . $numlista . '</td>';
            echo '<td>' . $fila['titulo'] . '</td>';
            echo '<td>' . $fila['descripcion'] . '</td>';
            echo '<td>' . $fila['nombre_completo'] . '</td>';
            echo '<td>' . $fila['precio'] . '</td>';
            echo '<td>' . $fila['direccion'] . '</td>';
            echo '</tr>';
        }
        echo "</table>";
        ?>
    <?php } ?>


    <?php if (isset($_GET['enviar'])) { ?>
        <?php
        echo '<table cellpadding="0" cellspacing="0" width="100%">';
        echo '<thead>
				<tr>
					<td>No.</td>';
        ?>
        <?php $var = ""; ?>

        <?php if (isset($_GET['titulo'])) {
            $var = "?" . $var . $_GET['titulo']; ?>
            <td>TITULO</td>
        <?php } ?>

        <?php if (isset($_GET['descripcion'])) {
            $descripcion = $_GET['descripcion']; ?>
            <td>DESCRIPCION</td>
        <?php } ?>

        <?php if (isset($_GET['nombre'])) {
            $nombre = $_GET['nombre']; ?>
            <td>NOMBRE COMPLETO</td>
        <?php } ?>
        <?php if (isset($_GET['precio'])) {
            $precio = $_GET['precio']; ?>
            <td>PRECIO</td>
        <?php } ?>
        <?php if (isset($_GET['direccion'])) {
            $direccion = $_GET['direccion']; ?>
            <td>DIRECCION</td>
        <?php } ?>

        <?php
        echo '	</tr>
				</thead>';
        ?>


        <?php foreach ($pacientes as $fila) {
            $numlista++; ?>
            <tr class="gradeX">
                <td>
                    <?php echo $numlista; ?>
                </td>
                <?php if (isset($_GET['titulo'])) { ?>
                    <td>
                        <?php echo $fila['titulo']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['descripcion'])) { ?>
                    <td>
                        <?php echo $fila['descripcion']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['nombre'])) { ?>
                    <td>
                        <?php echo $fila['nombre_completo']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['precio'])) { ?>
                    <td>
                        <?php echo $fila['precio']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['direccion'])) { ?>
                    <td>
                        <?php echo $fila['direccion']; ?>
                    </td>
                <?php } ?>
            </tr>

        <?php } ?>
        <?php
        echo "</table>";
        ?>
        <div class="col-md-12">
            <form action="reporteSoporte.php?">
                <input type="submit" name="create_pdf" class="btn btn-danger pull-right" value="reporte por dpto">
            </form>
        </div>
    <?php } ?>


</div>
</body>
</html>
