<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Anuncio</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">
    <div class="navbar-identity">
        <a href="" class="navbar-brand logo logo-title">
<span class="logo-icon">
  <CENTER><img src="../Vistas/img/Logo6.png" width="400" height="70"></CENTER>  
</span>
        </a>
    </div>
    <br>
    <h1>
        <CENTER> REPORTE DE ANUNCIOS QUE ACEPTAN TRUEQUES</CENTER>
    </h1>
    <form action="reporteT1.php" method="POST">
        <br>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="titulo">TITULO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="categoria">CATEGORIA
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="precio">PRECIO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="estado">ESTADO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="zona">ZONA
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="dpto">DPTO
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="direccion">DIRECCION
            </label>
        </div>
        <div class="form-check disabled">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="permuta">PERMUTA
            </label>
        </div>
        <br>
        <button type="submit" class="btn btn-primary" name="enviar">GENERAR REPORTE</button>
    </form>

    <hr/>
