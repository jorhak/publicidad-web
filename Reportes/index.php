<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Pacientes</title>

    <link rel="stylesheet" href="style.css"/>

</head>

<body>
<div id="content">

    <h1>USUARIOS</h1>

    <hr/>

    <?php
    include_once("conexion.php");

    $con = new Conexion;
    $conectar = $con->con();
    $strConsulta = "SELECT * from usuario";
    $pacientes = $conectar->query($strConsulta);
    $numlista = 0;

    echo '<table cellpadding="0" cellspacing="0" width="100%">';
    echo '<thead><tr><td>No.</td><td>USUARIO</td><td>PASSWORD</td><td>CI</td><td>ROL</td></tr></thead>';
    foreach ($pacientes as $fila) {
        $numlista++;
        echo '<tr><td>' . $numlista . '</td>';
        echo '<td>' . $fila['user'] . '</td>';
        echo '<td>' . $fila['password'] . '</td>';
        echo '<td>' . $fila['ci'] . '</td>';
        echo '<td>' . $fila['id_rol'] . '</td>';
        echo '<td><a href="reporteAnunPublicado.php?ci=' . $fila['ci'] . '">ver</a></td></tr>';
    }
    echo "</table>";
    ?>

</div>
</body>
</html>
