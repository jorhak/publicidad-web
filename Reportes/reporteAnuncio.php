<?php

require('fpdf/fpdf.php');
require('conexion.php');

class PDF extends FPDF
{
    var $widths;
    var $aligns;

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border

            $this->Rect($x, $y, $w, $h);

            $this->MultiCell($w, 5, $data[$i], 0, $a, 'true');
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    function Header()
    {

        $this->SetFont('Arial', '', 20);
        $this->Text(20, 14, 'Clientes que han publicado anuncios', 0, 'C', 0);
        $this->Ln(30);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(100, 10, 'Anuncio', 0, 0, 'L');

    }

}


$con = new Conexion;
$conectar = $con->con();


$pdf = new PDF('L', 'mm', 'Letter');
$pdf->Open();
$pdf->AddPage();
$pdf->SetMargins(20, 20, 20);
$pdf->Ln(10);
//										izquierda
$pdf->Image('../Vistas/img/Logo6.png', 15, 20, 100, 35, 'PNG');
$pdf->SetFont('Arial', '', 12);
$fecha = date('Y-m-d');

$pdf->Ln(10);
// Cabecera    NO SE TOCA

/*$pdf->Cell(Largo,Alto,TEXTO,BORDE,SALTO DE LINEA,ORIENTACION,COLOR);*/
$pdf->Cell(50, 6, 'C. I. del Administrador:', 0, 0, 'L');
$pdf->Cell(50, 6, '7720112', 0, 1, 'L');
$pdf->Cell(50, 6, 'Usuario:', 0, 0, 'L');
$pdf->Cell(50, 6, 'Administrador', 0, 1, 'L');
$pdf->Cell(50, 6, 'Fecha del Reporte:', 0, 0, 'L');
$pdf->Cell(50, 6, $fecha, 0, 1, 'L');
$pdf->Cell(100, 6, '', 0, 1, 'C'); // Salto de Linea
//$pdf->Cell(100,6,'',0,1,'C');
// LA cabezera de la Tabla
$pdf->Ln(5);


$pdf->SetWidths(array(50, 55, 30, 30, 30, 50));
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetFillColor(85, 107, 47);
$pdf->SetTextColor(255);

for ($i = 0; $i < 1; $i++) {
    $pdf->Row(array('TITULO', 'FECHA DE PUBLICACION', 'ESTADO', 'CATEGORIA', 'PRECIO', 'CLIENTE'));
}

$strConsulta = "SELECT anuncio.titulo,anuncio.fecha_ini as fecha_publicacion,anuncio.estado, categoria.descripcion as categoria, producto_inmueble.precio, concat(cliente.nombre,' ',cliente.apellido) as nombre_completo 
		FROM anuncio 
		INNER JOIN categoria ON anuncio.id_categoria = categoria.id 
		INNER JOIN producto_inmueble ON anuncio.id_prod_inmu=producto_inmueble.id 
		INNER JOIN cliente ON anuncio.ci_cliente=cliente.ci 
		WHERE cliente.ci=anuncio.ci_cliente order by anuncio.id asc";

$historial = $conectar->query($strConsulta);
$numfilas = $historial->num_rows;

for ($i = 0; $i < $numfilas; $i++) {
    $fila = $historial->fetch_array();
    $pdf->SetFont('Arial', '', 10);

    if ($i % 2 == 1) {
        $pdf->SetFillColor(153, 255, 153);
        $pdf->SetTextColor(0);
        $pdf->Row(array($fila['titulo'], $fila['fecha_publicacion'], $fila['estado'], $fila['categoria'], $fila['precio'], $fila['nombre_completo']));
    } else {
        $pdf->SetFillColor(102, 204, 51);
        $pdf->SetTextColor(0);
        $pdf->Row(array($fila['titulo'], $fila['fecha_publicacion'], $fila['estado'], $fila['categoria'], $fila['precio'], $fila['nombre_completo']));
    }
}

$pdf->Output();
?>
