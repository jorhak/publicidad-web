<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Anuncio</title>

    <link rel="stylesheet" href="style.css"/>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="../Vistas/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="../Vistas/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="../Vistas/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../Vistas/assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../Vistas/assets/ico/favicon.png">
    <title>Publicidad Web</title>
    <!-- Bootstrap core CSS -->
    <link href="../Vistas/assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="../Vistas/assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="../Vistas/assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="../Vistas/assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="../Vistas/assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="../Vistas/assets/js/pace.min.js"></script>
    <script src="../Vistas/assets/plugins/modernizr/modernizr-custom.js"></script>


</head>

<body>
<div id="content">
    <div class="navbar-identity">
        <a href="" class="navbar-brand logo logo-title">
<span class="logo-icon">
  <CENTER><img src="../Vistas/img/Logo6.png" width="400" height="70"></CENTER>  
</span>
        </a>
    </div>
    <br>
    <h1>
        <CENTER> REPORTES DE CATEGORIA POR ZONA</CENTER>
    </h1>
    <form action="reporteT2.php" method="POST">
        <div class="search-row-wrapper" style="background-image: url(images/bg.jpg)">
            <div class="inner">
                <div class="container ">
                    <form action="vParametroZonaDpto.php" method="POST">
                        <div class="row">


                            <div class="col-md-4">
                                <select class="form-control selecter" name="descripcion" id="search-category">
                                    <option selected="selected" value="">Buscar por Categoria</option>
                                    <option value="Bienes de Hogar">Bienes de Hogar</option>
                                    <option value="Ropa">Ropa</option>
                                    <option value="Celulares">Celulares</option>
                                    <option value="Electrodomesticos">Electrodomesticos</option>
                                    <option value="Vivienda">Vivienda</option>
                                    <option value="Computadoras">Computadoras</option>
                                    <option value="Instrumentos Musicales">Instrumentos Musicales</option>
                                    <option value="Joyeria">Joyeria</option>
                                    <option value="Accesorios">Accesorios</option>
                                    <option value="Vehiculos">Vehiculos</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <select class="form-control selecter" name="dpto" id="search-category">
                                    <option selected="selected" value="">Buscar por Departamento</option>
                                    <option value="Santa Cruz">Santa Cruz</option>
                                    <option value="La Paz">La Paz</option>
                                    <option value="Cochabamba">Cochabamba</option>
                                    <option value="Pando">Pando</option>
                                    <option value="Beni">Beni</option>
                                    <option value="Oruro">Oruro</option>
                                    <option value="Potosi">Potosi</option>
                                    <option value="Tarija">Tarija</option>
                                    <option value="Chuquisaca">Chuquisaca</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <button class="btn btn-block btn-primary btn-gradient" name="enviar"> Buscar <i
                                        class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </form>

    <hr/>

    <?php
    include_once("conexion.php");

    $con = new Conexion;
    $conectar = $con->con();
    $strConsulta = "SELECT anuncio.titulo, anuncio.descripcion, concat(cliente.nombre, cliente.apellido)as nombre_completo, producto_inmueble.precio, ubicacion.direccion 
    FROM anuncio 
    Inner Join cliente ON anuncio.ci_cliente= cliente.ci
    Inner Join producto_inmueble ON anuncio.id_prod_inmu = producto_inmueble.id
    Inner Join ubicacion ON ubicacion.id=producto_inmueble.id_ubicacion
    WHERE anuncio.estado='activo' order by anuncio.id asc;";
    $pacientes = $conectar->query($strConsulta);
    $numlista = 0;
    ?>

    <?php if (!isset($_GET['enviar'])) { ?>
        <?php
        echo '<table cellpadding="0" cellspacing="0" width="100%">';
        echo '<thead><tr><td>No.</td><td>TITULO</td><td>DESCRIPCION</td><td>NOMBRE COMPLETO</td><td>PRECIO</td><td>DIRECCION</td></tr></thead>';
        foreach ($pacientes as $fila) {
            $numlista++;
            echo '<tr><td>' . $numlista . '</td>';
            echo '<td>' . $fila['titulo'] . '</td>';
            echo '<td>' . $fila['descripcion'] . '</td>';
            echo '<td>' . $fila['nombre_completo'] . '</td>';
            echo '<td>' . $fila['precio'] . '</td>';
            echo '<td>' . $fila['direccion'] . '</td>';
            echo '</tr>';
        }
        echo "</table>";
        ?>
    <?php } ?>


    <?php if (isset($_GET['enviar'])) { ?>
        <?php
        echo '<table cellpadding="0" cellspacing="0" width="100%">';
        echo '<thead>
                <tr>
                    <td>No.</td>';
        ?>
        <?php $var = ""; ?>

        <?php if (isset($_GET['titulo'])) {
            $var = "?" . $var . $_GET['titulo']; ?>
            <td>TITULO</td>
        <?php } ?>

        <?php if (isset($_GET['descripcion'])) {
            $descripcion = $_GET['descripcion']; ?>
            <td>DESCRIPCION</td>
        <?php } ?>

        <?php if (isset($_GET['nombre'])) {
            $nombre = $_GET['nombre']; ?>
            <td>NOMBRE COMPLETO</td>
        <?php } ?>
        <?php if (isset($_GET['precio'])) {
            $precio = $_GET['precio']; ?>
            <td>PRECIO</td>
        <?php } ?>
        <?php if (isset($_GET['direccion'])) {
            $direccion = $_GET['direccion']; ?>
            <td>DIRECCION</td>
        <?php } ?>

        <?php
        echo '   </tr>
                </thead>';
        ?>


        <?php foreach ($pacientes as $fila) {
            $numlista++; ?>
            <tr class="gradeX">
                <td>
                    <?php echo $numlista; ?>
                </td>
                <?php if (isset($_GET['titulo'])) { ?>
                    <td>
                        <?php echo $fila['titulo']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['descripcion'])) { ?>
                    <td>
                        <?php echo $fila['descripcion']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['nombre'])) { ?>
                    <td>
                        <?php echo $fila['nombre_completo']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['precio'])) { ?>
                    <td>
                        <?php echo $fila['precio']; ?>
                    </td>
                <?php } ?>
                <?php if (isset($_GET['direccion'])) { ?>
                    <td>
                        <?php echo $fila['direccion']; ?>
                    </td>
                <?php } ?>
            </tr>

        <?php } ?>
        <?php
        echo "</table>";
        ?>
        <div class="col-md-12">
            <form action="reporteSoporte.php?">
                <input type="submit" name="create_pdf" class="btn btn-danger pull-right" value="reporte por dpto">
            </form>
        </div>
    <?php } ?>


</div>
</body>
</html>
