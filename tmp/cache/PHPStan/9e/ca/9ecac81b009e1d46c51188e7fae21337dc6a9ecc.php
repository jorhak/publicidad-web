<?php declare(strict_types = 1);

// odsl-/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo
return \PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => 'v1',
   'data' => 
  array (
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MModulo.php' => 
    array (
      0 => '20cbba77fa98849a4f126b59fc937ce471ce37a8',
      1 => 
      array (
        0 => 'mmodulo',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getmodulo',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MCasoUso.php' => 
    array (
      0 => '606dc32bb7bdc12bb6b15bea04d838817b382309',
      1 => 
      array (
        0 => 'mcasouso',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getcasouso',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MAddCuentaBancaria.php' => 
    array (
      0 => 'd9ca5b4130031aac7025af7158d9e31f6867d3ba',
      1 => 
      array (
        0 => 'maddcuentabancaria',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'deletedata',
        2 => 'montocuentabancaria',
        3 => 'addcuentabancaria',
        4 => 'connectdatabase',
        5 => 'getnrocuenta',
        6 => 'setnrocuenta',
        7 => 'getmonto',
        8 => 'setmonto',
        9 => 'getid',
        10 => 'activarcuentasbancarias',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MUbicacion.php' => 
    array (
      0 => '4818489b6a3891f9499a52d0c921739619a0ef03',
      1 => 
      array (
        0 => 'mubicacion',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'updateaddres',
        2 => 'updatezone',
        3 => 'updateestate',
        4 => 'addaddress',
        5 => 'getdireccion',
        6 => 'setdireccion',
        7 => 'getzona',
        8 => 'setzona',
        9 => 'getdepartamento',
        10 => 'setdepartamento',
        11 => 'getid',
        12 => 'obtenerubicacion',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MTrueque.php' => 
    array (
      0 => 'cbc7fe3f15d3b70b185f710d21db18e1c115fb49',
      1 => 
      array (
        0 => 'mtrueque',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getid',
        2 => 'addtrueque',
        3 => 'addtruequeno',
        4 => 'obtenertrueque',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MSoporte.php' => 
    array (
      0 => '7774f0874bf10a0b072d817ea7ed585d3c88af2f',
      1 => 
      array (
        0 => 'msoporte',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getsoporte',
        2 => 'addsoporte',
        3 => 'responder',
        4 => 'getsoporteci',
        5 => 'getid',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MProducto.php' => 
    array (
      0 => '06bbd5258a7d60b88e05a0f003b898360aa4a416',
      1 => 
      array (
        0 => 'mproducto',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getproductos',
        2 => 'getidu',
        3 => 'addproducto',
        4 => 'obtenerproducto',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MPrivilegio.php' => 
    array (
      0 => '48d5b2dae420bec4b80562e2ccfe7560dfb166d0',
      1 => 
      array (
        0 => 'mprivilegio',
      ),
      2 => 
      array (
        0 => 'insertprivilegio',
        1 => 'updateprivilegio',
        2 => 'obtenerid',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MInmueble.php' => 
    array (
      0 => 'da58177b59feff4d65e820055ad804ef59ef7405',
      1 => 
      array (
        0 => 'minmueble',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getinmueble',
        2 => 'getid',
        3 => 'getidu',
        4 => 'addinmueble',
        5 => 'obtenerdatosinmueble',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MEmpleado.php' => 
    array (
      0 => 'd6e3f12ab96719369af1faf49ed27796f9bdaa8a',
      1 => 
      array (
        0 => 'mempleado',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'gettabla',
        2 => 'getempleado',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MGaleria.php' => 
    array (
      0 => '590ad6fda69de55250b5371d894439c6d7db2174',
      1 => 
      array (
        0 => 'mgaleria',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getiddi',
        2 => 'insertgaleria',
        3 => 'updategaleria',
        4 => 'deletegaleria',
        5 => 'gettabla',
        6 => 'getgaleria',
        7 => 'getid',
        8 => 'obtenergaleria',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MDetalleAnuncio.php' => 
    array (
      0 => 'd8f207d66fb5518f54626877bc9e2a071c3027bc',
      1 => 
      array (
        0 => 'mdetalleanuncio',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getdetalleanuncio',
        2 => 'getdetalleanuncioparaaddanuncio',
        3 => 'adddetalleanuncio',
        4 => 'getid',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MCliente.php' => 
    array (
      0 => '97108ee7c17e2f063b48fd60615e9306f1b30ad4',
      1 => 
      array (
        0 => 'mcliente',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getcliente',
        2 => 'addcliente',
        3 => 'adduser',
        4 => 'edituser',
        5 => 'addempleado',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MBitacora.php' => 
    array (
      0 => 'd5c29e1a1722bed73ca84bf0eaaa53871fe562b0',
      1 => 
      array (
        0 => 'mbitacora',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'insertbitacora',
        2 => 'updatebitacora',
        3 => 'deletebitacora',
        4 => 'gettabla',
        5 => 'getbitacora',
        6 => 'getid',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MAnuncio.php' => 
    array (
      0 => 'cf1a23c41b0de29bc4ef2682532ee5bd89978828',
      1 => 
      array (
        0 => 'manuncio',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getiddi',
        2 => 'getidcat',
        3 => 'getiddetaanuncio',
        4 => 'addanuncio',
        5 => 'addanuncio2',
        6 => 'getanuncios',
        7 => 'getcategoria',
        8 => 'obtenerdatosanuncios',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MLogin.php' => 
    array (
      0 => 'd345fbbac97200d78eedf81d65845845ad02a314',
      1 => 
      array (
        0 => 'musuario',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getuser',
        2 => 'getrol',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MCuentasBancarias.php' => 
    array (
      0 => 'f29d4c2b31e82c466bae65260797ab09780e2973',
      1 => 
      array (
        0 => 'mcuentasbancarias',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getcuentasbancarias',
        2 => 'deletecuentasbancarias',
      ),
      3 => 
      array (
      ),
    ),
    '/home/juanvladimir13/Diplomado/modulo05/proyecto/publicidad-web/Modelo/MCliente1.php' => 
    array (
      0 => '2953a6afddf2fcc4d52a86d88e57f240f4ae51c1',
      1 => 
      array (
        0 => 'mcliente1',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'gettabla',
        2 => 'getcliente',
        3 => 'updatecliente',
        4 => 'updateusuario',
      ),
      3 => 
      array (
      ),
    ),
  ),
));