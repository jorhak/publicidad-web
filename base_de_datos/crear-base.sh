#!/bin/bash

# Espera un momento para asegurarse de que el servidor MySQL esté arriba y funcionando
echo "Esperando a que MySQL esté disponible..."
while ! mysqladmin ping -h"localhost" -u"root" -p"123" --silent; do
    sleep 1
done

# Ejecutamos el comando para crear la base de datos
mysql -u root -p123 -e "CREATE DATABASE IF NOT EXISTS base_olx;"

# Importamos datos desde el archivo SQL si es necesario
if [ -f /docker-entrypoint-initdb.d/base_olx.sql ]; then
    echo "Importando datos desde base_olx.sql..."
    mysql -u root -p123 base_olx < /docker-entrypoint-initdb.d/base_olx.sql
fi

echo "Base de datos base_olx creada y configurada."
