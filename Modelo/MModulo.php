<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MModulo
{
    public int $moduloId;
    public string $modulo;
    public string $icono;

    public function __construct(int $moduloId, string $modulo, string $icono)
    {
        $this->moduloId = $moduloId;
        $this->modulo = $modulo;
        $this->icono = $icono;
    }

    public static function getModulo(int $rolId): array
    {
        $list = [];
        $db = Conectar::conexion();
        $req = $db->query("SELECT DISTINCT m.idModulo as id, m.nombre as modulo, m.icono as icono FROM PRIVILEGIO p, CU cu, roles r, MODULO m WHERE p.estado=1 and cu.idModulo=m.idModulo and p.idCU=cu.idCU and p.idRol=r.id and r.id='$rolId' ORDER BY m.idModulo");

        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MModulo($row['id'], $row['modulo'], $row['icono']);
        }
        return $list;
    }
}
