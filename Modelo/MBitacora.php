<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MBitacora
{
    public int $id_bit;
    public string $fecha_hora;
    public string $descripcion;
    public string $ci;

    public function __construct($id_bit, $fecha_hora, $descripcion, $ci)
    {
        $this->id_bit = $id_bit;
        $this->fecha_hora = $fecha_hora;
        $this->descripcion = $descripcion;
        $this->ci = $ci;
    }

    /**
     * @return bool|mysqli_result
     */
    public static function insertBitacora(string $descripcion, string $ci)
    {
        $db = Conectar::conexion();
        return $db->query("INSERT INTO bitacora(fecha_hora,descripcion,ci) VALUES (now(),'$descripcion','$ci')");
    }

    /**
     * @return bool|mysqli_result
     */
    public static function updateBitacora(string $descripcion, int $id)
    {
        $db = Conectar::conexion();
        return $db->query("UPDATE bitacora SET descripcion='$descripcion' WHERE id_bit='$id'");
    }

    /**
     * @return bool|mysqli_result
     */
    public static function deleteBitacora(int $id)
    {
        $db = Conectar::conexion();
        $sql = $db->query("DELETE FROM bitacora WHERE id_bit='$id'");

        return $sql;
    }

    public static function getTabla(): array
    {
        $list = [];
        $db = Conectar::conexion();
        $req = $db->query('SELECT * FROM bitacora');
        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MBitacora($row['id_bit'], $row['fecha_hora'], $row['descripcion'], $row['ci']);
        }
        return $list;
    }

    public static function getBitacora(int $id): array
    {
        $list = [];
        $db = Conectar::conexion();
        $sql = 'SELECT * FROM bitacora WHERE id=' . $id;
        $req = $db->query($sql);
        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MBitacora($row['id_bit'], $row['fecha_hora'], $row['descripcion'], $row['ci']);
        }
        return $list;
    }

    public static function getID(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM bitacora;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

}
