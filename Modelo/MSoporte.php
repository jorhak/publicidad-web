<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MSoporte
{
    /** @var mysqli */
    private $db;
    private array $soportes;
 
    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->soportes = [];
    }

    /**
     * @return bool|mysqli_result
     */
    public function getSoporte()
    {
        return $this->db->query("SELECT * from soporte where estado= 'En Espera';");
    }

    public function addSoporte(string $email, string $descripcion, string $carnet): int
    {
        $sql = $this->db->query("INSERT INTO soporte VALUES (null,now(),'$email','$descripcion','En Espera',null,'$carnet');");
        return $sql ? 1 : 0;
    }

    public function responder(int $id, string $respuesta): void
    {
        $consulta = $this->db->query("UPDATE soporte set respuesta='$respuesta', estado='Solucionado' where id='$id';");
    }

    /**
     * @return bool|mysqli_result
     */
    public function getSoporteCi(string $ci)
    {
        return $this->db->query("SELECT * from soporte where ci_cliente= '$ci';");
    }

    public static function getId(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM soporte;");
        $row = $sql->fetch_assoc();
        return array_key_exists('id', $row) ? (int)$row['id'] : 0;
    }
}
