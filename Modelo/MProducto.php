<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MProducto
{
    /** @var mysqli */
    private $db;
    /*private $descripcion_prod;
    private $marca_prod;*/
    private array $productos;

    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->productos = [];
    }

    public function getProductos(): array
    { // Muestra al cliente solicitado

        $consulta = $this->db->query("SELECT * from detalle_inmueble");
        $this->productos = mysqli_fetch_array($consulta);
        return $this->productos;
    }

    public static function getIdU(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM ubicacion;");
        $row = $sql->fetch_assoc();
        return array_key_exists('id', $row) ? (int)$row['id'] : 0;
    }

    public function addProducto(int $precio, string $estado, string $descripcion, string $marca): int
    {
        $id_u = self::getIdU();
        $sql2 = $this->db->query("INSERT into producto_inmueble values(null,'$precio','$estado','$id_u');");

        $sql = $this->db->query("INSERT into detalle_producto values(null,'$descripcion','$marca','$id_u');");
        return $sql ? 1 : 0;
    }

    public static function obtenerProducto(int $id_producto):void
    {
        $id_pru_anuncio = 0;
        $db = Conectar::conexion();
        $sql2 = $db->query("SELECT a.titulo,a.descripcion,pi.precio,pi.estado,dp.descripcion,dp.marca
                        from anuncio as a, producto_inmueble as pi, detalle_producto as dp
                        where a.id_prod_inmu=pi.id and pi.id=dp.id_prod_inmu and a.id= '$id_pru_anuncio';");
    }
}
