<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MCuentasBancarias
{
    /** @var mysqli */
    private $db;
    /*private string $nro;
    private int $monto;
    private string $estado;*/
    private array $cuentaBancarias;

    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->cuentaBancarias = [];
    }

    /**
     * @return bool|mysqli_result
     */
    public function getCuentasBancarias(string $clienteCI)
    { // Muestra al cliente solicitado
        $consulta = $this->db->query("SELECT cb.nro_cuenta,cb.monto,cb.ci_cliente from cliente as c , cuenta_bancaria as cb
                                            where cb.ci_cliente=c.ci and ci='$clienteCI' and cb.estado = 'activa';");
        //$this->cbancarias=  mysqli_fetch_array($consulta);
        return $consulta;
    }

    /**
     * @return bool|mysqli_result
     */
    public function deleteCuentasBancarias(string $nroCuenta)
    { // Muestra al cliente solicitado
        $consulta = $this->db->query("UPDATE cuenta_bancaria set estado= 'inactiva' where nro_cuenta = '$nroCuenta';");
        return $consulta;
    }
}
