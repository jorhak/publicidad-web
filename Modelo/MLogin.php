<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MUsuario
{
    /** @var mysqli */
    private $db;
    private array $users;

    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->users = [];
    }

    public function getUser(string $user_busq, string $pass_busq):string
    {
        $consulta = $this->db->query("SELECT u.ci from usuario as u 
                                            where u.user='$user_busq'and u.password='$pass_busq';");
        $rws = mysqli_fetch_array($consulta);
        $cod = $rws[0];
        return $cod > 0 ? $cod : '0';
    }

    public function getRol(string $user, string $pass): int
    {
        $sql = $this->db->query("SELECT u.id_rol FROM usuario as u WHERE u.user='$user' and u.password='$pass';");
        $row1 = mysqli_fetch_array($sql);
        return array_key_exists('id_rol',$row1) ? (int)$row1['id_rol'] : 0;
    }
}
