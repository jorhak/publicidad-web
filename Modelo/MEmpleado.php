<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MEmpleado
{
    public string $ci;
    public string $nombre;
    public string $user;
    public int $idRol;

    public function __construct(string $ci, string $nombre, string $user, int $idRol)
    {
        $this->ci = $ci;
        $this->nombre = $nombre;
        $this->user = $user;
        $this->idRol = $idRol;
    }


    public static function getTabla(): array
    {
        $list = [];
        $db = Conectar::conexion();
        $sql = "SELECT cliente.ci,concat (cliente.nombre,' ',cliente.apellido),usuario.user,usuario.id_rol
                FROM cliente
                Inner Join usuario on usuario.ci=cliente.ci 
                WHERE usuario.id_rol=1 ;";
        $req = $db->query($sql);

        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MEmpleado($row['ci'], $row['nombre'], $row['user'], $row['id_rol']);
        }
        return $list;
    }

    public static function getEmpleado(): array
    {
        $list = [];
        $db = Conectar::conexion();
        $sql = "SELECT cliente.ci, concat(cliente.nombre,' ', cliente.apellido) as nombre, usuario.user as usuario, roles.id as rol 
            FROM cliente 
            INNER JOIN usuario on usuario.ci = cliente.ci 
            INNER JOIN roles on roles.id = usuario.id_rol 
            WHERE usuario.id_rol=1";
        $req = $db->query($sql);

        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MEmpleado($row['ci'], $row['nombre'], $row['usuario'], $row['rol']);
        }
        return $list;
    }

}
