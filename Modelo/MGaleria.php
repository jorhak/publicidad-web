<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MGaleria
{
    public int $id;
    public string $url;
    public int $id_deta_inmu;
    public string $imagen;
    public string $ruta;

    //private $db;

    public function __construct(int $id, string $url, int $id_deta_inmu, string $imagen, string $ruta)
    {
        $this->id = $id;
        $this->url = $url;
        $this->id_deta_inmu = $id_deta_inmu;
        $this->imagen = $imagen;
        $this->ruta = $ruta;
    }

    public static function getIdDi(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM ubicacion;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    /**
     * @return bool|mysqli_result
     */
    public static function insertGaleria(string $url, string $imagen, string $ruta)
    {
        $db = Conectar::conexion();
        $id_deta_inmu = self::getIdDi();
        $sql = $db->query("INSERT INTO galeria(url,id_prod_inmu,imagen,ruta) VALUES ('$url','$id_deta_inmu','$imagen','$ruta')");

        return $sql;
    }

    /**
     * @return bool|mysqli_result
     */
    public static function updateGaleria(string $url, string $imagen, int $id)
    {
        $db = Conectar::conexion();
        $sql = $db->query("UPDATE galeria SET url='$url', imagen='$imagen' WHERE id='$id'");

        return $sql;
    }

    /**
     * @return bool|mysqli_result
     */
    public static function deleteGaleria(int $id)
    {
        $db = Conectar::conexion();
        $sql = $db->query("DELETE FROM galeria WHERE id='$id'");

        return $sql;
    }

    public static function getTabla(): array
    {
        $list = [];
        $db = Conectar::conexion();
        $req = $db->query('SELECT * FROM galeria');
        foreach ($req->fetch_all(MYSQLI_ASSOC) as $tabla) {
            $list[] = new MGaleria($tabla['id'], $tabla['url'], $tabla['id_deta_inmu'], $tabla['imagen'], $tabla['ruta']);
        }
        return $list;
    }

    public static function getGaleria(int $id): array
    {
        $list = [];
        $db = Conectar::conexion();
        $sql = 'SELECT * FROM galeria WHERE id=' . $id;
        $req = $db->query($sql);
        foreach ($req->fetch_all(MYSQLI_ASSOC) as $tabla) {
            $list[] = new MGaleria($tabla['id'], $tabla['url'], $tabla['id_deta_inmu'], $tabla['imagen'], $tabla['ruta']);
        }
        return $list;
    }

    public static function getId(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM galeria;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    public static function obtenerGaleria(): void
    {
        $id_pru_anuncio = '';
        $db = Conectar::conexion();
        $sql3 = $db->query("SELECT g.imagen from anuncio as a , producto_inmueble as pi, galeria as g
                        where a.id_prod_inmu=pi.id and g.id_prod_inmu=pi.id and a.id= '$id_pru_anuncio';");
    }
}
