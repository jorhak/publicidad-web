<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MCasoUso
{
    public int $idCU;
    public int $idModulo;
    public string $carpeta;
    public string $nombre;
    public int $estado;

    public function __construct(int $idCU, int $idModulo, string $carpeta, string $nombre, $estado)
    {
        $this->idCU = $idCU;
        $this->idModulo = $idModulo;
        $this->carpeta = $carpeta;
        $this->nombre = $nombre;
        $this->estado = $estado;
    }

    public static function getCasoUso(string $ci, string $user): array
    {
        $list = [];
        $db = Conectar::conexion();
        $req = $db->query("SELECT cu.idCU,cu.idModulo,cu.carpeta,cu.nombre,cu.estado
                FROM usuario
                Inner Join roles on usuario.id_rol=roles.id
                Inner Join privilegio on roles.id=privilegio.idRol
                Inner Join cu on privilegio.idCU=cu.idCU
                Inner Join modulo on cu.idModulo=modulo.idModulo
                WHERE privilegio.ci='$ci' AND usuario.user='$user' and privilegio.estado=1 GROUP by cu.idCU;");

        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MCasoUso($row['idCU'], $row['idModulo'], $row['carpeta'], $row['nombre'], $row['estado']);
        }
        return $list;
    }
}
