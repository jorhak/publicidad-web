<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MUbicacion
{

    private string $direccion = '';
    private string $zona = '';
    private string $departamento = '';

    public function __construct(string $direccion = '', string $zona = '', string $departamento = '')
    {
        $this->direccion = $direccion;
        $this->zona = $zona;
        $this->departamento = $departamento;
    }

    public function updateAddres(int $id): bool
    {
        $conexion = Conectar::conexion();
        //EL ID DE LA UBICACION SE RECIBIRA POR PARAMETRO DESDE EL CONTROLADOR
        $updateQuery = "update ubicacion set direccion = '$this->direccion' where id = '$id'";
        $resultado = mysqli_query($conexion, $updateQuery);
        mysqli_close($conexion);

        return $resultado ? true : false;
    }

    public function updateZone(int $id): bool
    {
        $conexion = Conectar::conexion();
        //EL ID DE LA UBICACION SE RECIBIRA POR PARAMETRO DESDE EL CONTROLADOR
        $updateQuery = "update ubicacion set zona = '$this->zona' where id = '$id'";
        $resultado = mysqli_query($conexion, $updateQuery);
        mysqli_close($conexion);
        return $resultado ? true : false;
    }

    public function updateEstate(int $id): bool
    {
        $conexion = Conectar::conexion();
        //EL ID DE LA UBICACION SE RECIBIRA POR PARAMETRO DESDE EL CONTROLADOR
        $updateQuery = "update ubicacion set dpto = '$this->departamento' where id = '$id'";
        $resultado = mysqli_query($conexion, $updateQuery);
        mysqli_close($conexion);
        return $resultado ? true : false;
    }

    public function addAddress():int
    {
        $conexion = Conectar::conexion();
        $QueryInsert = "INSERT into ubicacion (direccion,zona,dpto) values ('$this->direccion','$this->zona','$this->departamento')";
        $resultado = mysqli_query($conexion, $QueryInsert);
        return !$resultado ? 0 : 1;
    }

    public function getDireccion(): string
    {
        return $this->direccion ?? '';
    }

    public function setDireccion(string $direccion): void
    {
        $this->direccion = $direccion;
    }

    public function getZona(): string
    {
        return $this->zona ?? '';
    }

    public function setZona(string $zona): void
    {
        $this->zona = $zona;
    }

    public function getDepartamento(): string
    {
        return $this->departamento ?? '';
    }

    public function setDepartamento(string $departamento):void
    {
        $this->departamento = $departamento;
    }


    public static function getId(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM ubicacion;");
        $row = $sql->fetch_assoc();
        return array_key_exists('id', $row) ? (int)$row['id'] : 0;
    }

    /**
     * @return bool|mysqli_result
     */
    public static function obtenerUbicacion(int $id_pru_anuncio)
    {
        $db = Conectar::conexion();
        $sql5 = $db->query("SELECT u.direccion,u.zona,u.dpto from ubicacion as u, producto_inmueble as pi, anuncio as a
                     where a.id_prod_inmu = pi.id and pi.id_ubicacion=u.id and a.id= '$id_pru_anuncio';");
        return $sql5;
    }
}
