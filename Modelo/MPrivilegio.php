<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MPrivilegio
{
    public int $privilegioId;
    public int $casousoId;
    public int $rolId;
    public int $estado;

    /**
     * @return bool|mysqli_result
     */
    public static function insertPrivilegio(int $casousoId, int $rolId)
    {
        $db = Conectar::conexion();
        $sql = "INSERT INTO privilegio(idCU,idRol,estado) VALUES('$casousoId','$rolId',1)";
        return $db->query($sql);
    }

    /**
     * @return bool|mysqli_result
     */
    public static function updatePrivilegio(int $casousoId, int $estado, string $ci)
    {
        $db = Conectar::conexion();
        $sql = "UPDATE privilegio set estado='$estado' WHERE idCU='$casousoId' and ci='$ci'";
        $req = $db->query($sql);
        return $req;
    }

    public static function obtenerId(): int
    {
        $db = Conectar::conexion();
        $sql = "SELECT MAX(idPrivilegio) as id FROM privilegio";
        $req = $db->query($sql);
        $row = $req->fetch_assoc();
        return array_key_exists('id', $row) ? (int)$row['id'] : 0;
    }
}
