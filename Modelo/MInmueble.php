<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MInmueble
{
    /** @var mysqli */
    private $db;
    /*private $nro_casa_inmu;
    private $categoria;
    private $tipo_inmu;
    private $superficie_casa;
    private $cant_pisos;
    private $cant_banos;
    private $garaje_casa;
    private $alcantarillado_Casa;
    private $detalles_varios;*/
    private array $inmueble;

    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->inmueble = [];
    }

    public function getInmueble(): array
    {
        $consulta = $this->db->query("SELECT * from detalle_inmueble");
        $this->inmueble = mysqli_fetch_array($consulta);
        return $this->inmueble;
    }

    public static function getId(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM detalle_inmueble;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    public static function getIdU(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM ubicacion;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    public function addInmueble(int    $nro_casa, int $precio, string $estado, string $categoria, string $tipo,
                                float  $superficie, int $cantPisos, int $cantHabitaciones, int $cantBanios,
                                string $garaje, string $alcantarillado, string $detalles): int
    {
        $id_u = self::getIdU();
        $sql2 = $this->db->query("INSERT into producto_inmueble values(null,'$precio','$estado','$id_u');");

        $sql = $this->db->query("INSERT into detalle_inmueble values(null,'$nro_casa','$categoria','$tipo','$superficie','$cantPisos','$cantHabitaciones','$cantBanios','$garaje','$alcantarillado','$detalles','$id_u');");
        return $sql ? 1 : 0;
    }

    public static function obtenerDatosInmueble(int $id_producto): void
    {
        $id_pru_anuncio = 0;
        $db = Conectar::conexion();
        $sql2 = $db->query("SELECT a.titulo,a.descripcion,di.nro_casa,pi.precio,pi.estado,di.categoria,di.tipo,di.superficie,di.cant_pisos,di.cant_habitaciones,di.cant_banos,di.garaje,di.alcantarillado,di.detalles 
        from anuncio as a, detalle_inmueble as di, producto_inmueble as pi
        where a.id_prod_inmu=pi.id and pi.id=di.id_prod_inmu and a.id= '$id_pru_anuncio';");
    }
}
