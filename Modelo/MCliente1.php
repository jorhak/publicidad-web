<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MCliente1
{
    public string $ci;
    public string $nombre;
    public string $apellido;
    public string $email;
    public string $telefono;

    public function __construct(string $ci, string $nombre, string $apellido, string $email, string $telefono)
    {
        $this->ci = $ci;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->email = $email;
        $this->telefono = $telefono;
    }


    public static function getTabla(): array
    {
        $list = [];
        $db = Conectar::conexion();
        $sql = "SELECT cliente.ci,concat (cliente.nombre,' ',cliente.apellido),usuario.user,usuario.id_rol
                FROM cliente
                Inner Join usuario on usuario.ci=cliente.ci 
                WHERE usuario.id_rol=1 ;";
        $req = $db->query($sql);

        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MEmpleado($row['ci'], $row['nombre'], $row['user'], $row['id_rol']);
        }
        return $list;
    }

    public static function getCliente(): array
    {
        $list = [];
        $db = Conectar::conexion();
        $sql = "SELECT * FROM cliente";
        $req = $db->query($sql);

        while ($row = mysqli_fetch_array($req)) {
            $list[] = new MCliente1($row['ci'], $row['nombre'], $row['apellido'], $row['email'], $row['telefono']);
        }
        return $list;
    }

    /**
     * @return bool|mysqli_result
     */
    public static function updateCliente(string $ci, string $nombre, string $apellido, string $email, string $telefono)
    {
        $db = Conectar::conexion();
        $sql = "UPDATE cliente SET nombre='$nombre', apellido='$apellido', email='$email', telefono='$telefono' WHERE ci='$ci'";
        return $db->query($sql);
    }

    /**
     * @return bool|mysqli_result
     */
    public static function updateUsuario(string $password, string $ci)
    {
        $db = Conectar::conexion();
        $sql = "UPDATE usuario SET password='$password' WHERE ci='$ci'";
        return $db->query($sql);
    }

}
