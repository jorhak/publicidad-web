<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MCliente
{
    /** @var mysqli */
    private $db;
    private array $clientes;
    /*private string $nombre_clien;
    private string $apellido_clien;
    private string $email_clien;
    private string $tel_cel;*/

    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->clientes = [];
    }

    /**
     * @param string $ci_cliente
     * @return array | false | null
     */
    public function getCliente(string $ci_cliente)
    {
        $consulta = $this->db->query("SELECT * from cliente where ci='$ci_cliente';");
        $this->clientes = mysqli_fetch_array($consulta);
        return $this->clientes;
    }

    public function addCliente(string $ci_c, string $name, string $ap_name, string $email, string $cel): int
    {
        $sql = $this->db->query("INSERT into cliente values('$ci_c','$name','$ap_name','$email','$cel');");
        return $sql ? 1 : 0;
    }

    public function addUser(int $id, string $pas, string $ci_c): int
    {
        $sql = $this->db->query("INSERT into usuario values('$id','$pas','$ci_c',2);");
        return $sql ? 1 : 0;
    }

    public function editUser(string $ci, string $email, string $tel): int
    {
        $sql = $this->db->query("UPDATE cliente set email='$email', telefono='$tel' where ci='$ci'");
        return $sql ? 1 : 0;
    }

    public function addEmpleado(int $id, string $pas, string $ci_c): int
    {
        $sql = $this->db->query("INSERT into usuario values('$id','$pas','$ci_c',1);");
        $sql1 = $this->db->query("CALL ADDPRIVILEGIO('$ci_c',1)");
        return $sql ? 1 : 0;
    }
}
