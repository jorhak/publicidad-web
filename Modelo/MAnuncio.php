<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MAnuncio
{
    /** @var mysqli */
    private $db;
    /*private string $estado;
    private string $fecha_inicio;
    private string $titulo;
    private string $descripcion;*/
    private array $anuncios;

    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->anuncios = [];
    }

    public function getIdDi(): int
    {
        $sql = $this->db->query("SELECT MAX(id) AS id FROM ubicacion;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    public function getIdCat(string $categoriaDescripcion): int
    {
        $sql = $this->db->query("SELECT id from categoria as c where c.descripcion = '$categoriaDescripcion';");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    public function getIdDetaAnuncio(string $detalleTipo): int
    {
        $sql2 = $this->db->query("SELECT id from detalle_anuncio where tipo='$detalleTipo';");
        $row2 = mysqli_fetch_array($sql2);
        $r = $row2['id'] ?? 0;
        return $r;
    }

    public function addAnuncio(string $titulo, string $descripcion, int $detalleAnuncioId, string $clienteCI): int
    {
        $di = self::getIdDi();
        $sql = $this->db->query("INSERT into anuncio values (null,'activo',curdate(),'$titulo','$descripcion','$di','$clienteCI','$detalleAnuncioId',5);");
        return $sql ? 1 : 0;
    }

    public function addAnuncio2(string $titulo, string $descripcion, int $detalleAnuncioId, string $clienteCI, string $categoriaDescripcion): int
    {
        $di = self::getIdDi();
        $cat_A = self::getIdCat($categoriaDescripcion);

        $sql = $this->db->query("INSERT into anuncio 
                    values (null,'activo',curdate(),'$titulo','$descripcion','$di','$clienteCI','$detalleAnuncioId','$cat_A');");
        return $sql ? 1 : 0;
    }

    /**
     * @return bool|mysqli_result
     */
    public function getAnuncios(string $clienteCI)
    {
        $consulta = $this->db->query("SELECT a.id , a.titulo , pi.precio , cat.descripcion , a.estado from anuncio as a, producto_inmueble as pi,categoria as cat, cliente as c
                    where c.ci=a.ci_cliente and a.id_categoria=cat.id and a.id_prod_inmu= pi.id and c.ci='$clienteCI' 
                    order by a.id asc;");
        return $consulta;
    }

    /**
     * @return bool|mysqli_result
     */
    public function getCategoria()
    {
        return $this->db->query("SELECT descripcion from categoria as c where c.id <> 5;");
    }

    /**
     * @return bool|mysqli_result
     */
    public static function obtenerDatosAnuncios(string $clienteCI)
    {
        $db = Conectar::conexion();
        return $db->query("SELECT a.id , a.titulo , pi.precio , cat.descripcion , a.estado from anuncio as a, producto_inmueble as pi,categoria as cat, cliente as c
                    where c.ci=a.ci_cliente and a.id_categoria=cat.id and a.id_prod_inmu= pi.id and c.ci='$clienteCI' 
                    order by a.id asc;");
    }

}
