<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";
require_once "$root/Reportes/conexion.php";

class MAddCuentaBancaria
{
    protected string $nroCuenta = '';
    protected int $monto = 0;
    /** @var mysqli */
    private $db;

    public function __construct(string $nro_cuenta = null, int $monto = null)
    {
        $this->monto = $monto;
        $this->nroCuenta = $nro_cuenta;
        $this->db = Conectar::conexion();
    }

    /**
     * @return bool|mysqli_result
     */
    public function deleteData(string $nro_cuenta, $conexion)
    {
        $resultado = mysqli_query($conexion, "delete from cuenta_bancaria where nro_cuenta = '$nro_cuenta';");
        return $resultado;
    }

    /**
     * @return mixed|void
     */
    public function montoCuentaBancaria(string $cuenta)
    {
        $sql = $this->db->query("SELECT cr.monto_cr from cuentas_registradas as cr where cr.nro_cr='$cuenta';");
        if ($sql) {
            $rws = mysqli_fetch_array($sql);
            $monto_reg = $rws[0];
            return $monto_reg;
        }
    }

    public function addCuentaBancaria(string $cuenta, int $monto, string $ci_cliente): int
    {
        $sql = $this->db->query("INSERT into cuenta_bancaria values ('$cuenta','$monto','activa','$ci_cliente')");
        return $sql ? 1 : 0;
    }

    /**
     * @return mixed
     */
    public function connectDatabase()
    {
        $resultado = Conexion::con();
        return $resultado;
    }

    public function getNroCuenta(): string
    {
        return $this->nroCuenta ?? '';
    }

    public function setNroCuenta(string $nroCuenta): void
    {
        $this->nroCuenta = $nroCuenta;
    }

    public function getMonto(): int
    {
        return $this->monto ?? 0;
    }

    public function setMonto(int $monto): void
    {
        $this->monto = $monto;
    }

    public static function getId(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM galeria;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    /**
     * @return bool|mysqli_result
     */
    public function activarCuentasBancarias($nro_cuenta)
    { // Muestra al cliente solicitado

        return $this->db->query("UPDATE cuenta_bancaria set estado= 'activa' where nro_cuenta = '$nro_cuenta';");
    }

}
