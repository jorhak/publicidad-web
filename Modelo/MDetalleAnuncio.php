<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MDetalleAnuncio
{
    /** @var mysqli */
    private $db;
    private array $det_anun;

    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->det_anun = array();
    }

    /**
     * Muestra al cliente solicitado
     * @return bool|mysqli_result
     */
    public function getDetalleAnuncio()
    {
        $consulta = $this->db->query("SELECT * from detalle_anuncio");
        return $consulta;
    }

    /**
     * Muestra al cliente solicitado
     * @return bool|mysqli_result
     */
    public function getDetalleAnuncioParaAddAnuncio()
    {
        $consulta = $this->db->query("SELECT id,tipo from detalle_anuncio");
        return $consulta;
    }

    public function addDetalleAnuncio($tipo, string $duracion, int $precio): int
    {
        $sql = $this->db->query("INSERT into detalle_anuncio values(null,'$tipo','$duracion','$precio');");
        return $sql ? 1 : 0;
    }

    public static function getId(): int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM detalle_anuncio;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }
}
