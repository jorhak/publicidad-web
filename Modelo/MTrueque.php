<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/Controlador/conexion.php";

class MTrueque
{
    /** @var mysqli */
    private $db;
    /*private $estado_trueq;
    private $permuta_trueq;
    private $diferencia_trueq;*/
    private array $trueques;
 
    public function __construct()
    {
        $this->db = Conectar::conexion();
        $this->trueques = [];
    }

    public static function getId():int
    {
        $db = Conectar::conexion();
        $sql = $db->query("SELECT MAX(id) AS id FROM ubicacion;");
        $row = $sql->fetch_assoc();
        return $row['id'] ?? 0;
    }

    public function addTrueque(string $detalle, int $diferencia): int
    {
        $id_di = self::getId();
        $sql = $this->db->query("INSERT INTO trueque VALUES(null,'ACEPTA', '$detalle','$diferencia','$id_di');");
        return $sql ? 1 : 0;
    }

    public function addTruequeNO():int
    {
        $id_di = self::getId();
        $sql = $this->db->query("INSERT INTO trueque VALUES(null,'NO ACEPTA', 'No solicita Trueque',0, '$id_di');");
        return $sql ? 1 : 0;
    }

    public static function obtenerTrueque(int $id_pru_anuncio): void
    {
        $db = Conectar::conexion();
        $sql6 = $db->query("SELECT t.permuta,t.diferencia from trueque as t, anuncio as a, producto_inmueble as pi
                        where a.id_prod_inmu=pi.id and pi.id=t.id_prod_inm and a.id = '$id_pru_anuncio';");
    }
}
